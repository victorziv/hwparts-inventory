HW Parts Inventory application deployment
=========================================

This is how to deploy a new version of the application to staging and production environment.

The deployment is done via automatic Ansible procedure. Here is an example on how to run it:

.. code-block:: bash
   
   $ cd /opt/infinidat/hwparts-inventory-deployment
   $ ./deploy-staging.sh [ version ]

*Version* is the optional parameter; if not provided the latest **develop** branch code will be deployed. 
If you need to check out a specific version / branch use do provide the *version* parameter

.. code-block:: bash

   $ ./deploy-staging.sh some-valuable-topic-branch

or if some version tags were set and you'd rather deploy one of them:

.. code-block:: bash

   $ ./deploy-staging.sh 1.2.0


Here is the list of tasks the deploy playbook  executes:

- Clones the project repository or pulls it if it's already cloned
- Copies ivtsrv service script under /etc/init.d directory.
- Creates run directory ( */opt/infinidat/ivt-dashboard/run* ) if it does not already exists 
- Likewise creates a log directory if it's not already there ( */opt/infinidat/ivt-dashboard/log* )
- Creates virtualenv
- Installs the Python packages listed in requirements.txt inside virtualenv
- Triggers Django migrate
- Triggers Django collectstatic
- Changes ownership the application root dir and all subdirectories to IVT user
- Restarts *ivtsrv* service to re-load changes in code.
- Makes sure nginx server is running
