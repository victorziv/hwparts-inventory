HW Parts Inventory
==================

.. |Infinidat_TM| unicode:: Infinidat U+2122

.. |automation_infrastructure| raw:: html

    <a href="http://infraweb/docs/tutorial/" target="_blank">automation infrastructure</a>

This is documentation for |Infinidat_TM| HW parts inventory application.

Table Of Contents
-----------------

.. toctree::
    :maxdepth: 1
    
   deployment


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
