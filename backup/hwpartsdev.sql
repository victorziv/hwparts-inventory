--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.2
-- Dumped by pg_dump version 9.6.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: changelog; Type: TABLE; Schema: public; Owner: ivtapp
--

CREATE TABLE changelog (
    id integer NOT NULL,
    version character varying(16),
    name character varying(100),
    applied timestamp without time zone
);


ALTER TABLE changelog OWNER TO ivtapp;

--
-- Name: changelog_id_seq; Type: SEQUENCE; Schema: public; Owner: ivtapp
--

CREATE SEQUENCE changelog_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE changelog_id_seq OWNER TO ivtapp;

--
-- Name: changelog_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ivtapp
--

ALTER SEQUENCE changelog_id_seq OWNED BY changelog.id;


--
-- Name: contact; Type: TABLE; Schema: public; Owner: ivtapp
--

CREATE TABLE contact (
    id integer NOT NULL,
    name character varying(128),
    email character varying(128) NOT NULL,
    phone_desk character varying(32),
    phone_mobile character varying(32),
    notes text
);


ALTER TABLE contact OWNER TO ivtapp;

--
-- Name: contact_id_seq; Type: SEQUENCE; Schema: public; Owner: ivtapp
--

CREATE SEQUENCE contact_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE contact_id_seq OWNER TO ivtapp;

--
-- Name: contact_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ivtapp
--

ALTER SEQUENCE contact_id_seq OWNED BY contact.id;


--
-- Name: drive; Type: TABLE; Schema: public; Owner: ivtapp
--

CREATE TABLE drive (
    id integer NOT NULL,
    model character varying(32),
    revision character varying(32),
    vendorid integer,
    is_ssd boolean,
    protocol character varying(16)
);


ALTER TABLE drive OWNER TO ivtapp;

--
-- Name: drive_hdd_rejection_criteria; Type: TABLE; Schema: public; Owner: ivtapp
--

CREATE TABLE drive_hdd_rejection_criteria (
    modelid integer NOT NULL,
    logpage integer NOT NULL,
    counter_byte_offset integer NOT NULL,
    counter_length integer,
    comparison character varying(16),
    value integer
);


ALTER TABLE drive_hdd_rejection_criteria OWNER TO ivtapp;

--
-- Name: drive_id_seq; Type: SEQUENCE; Schema: public; Owner: ivtapp
--

CREATE SEQUENCE drive_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE drive_id_seq OWNER TO ivtapp;

--
-- Name: drive_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ivtapp
--

ALTER SEQUENCE drive_id_seq OWNED BY drive.id;


--
-- Name: drive_inquiry; Type: TABLE; Schema: public; Owner: ivtapp
--

CREATE TABLE drive_inquiry (
    modelid integer NOT NULL,
    page integer NOT NULL,
    data character varying(128)
);


ALTER TABLE drive_inquiry OWNER TO ivtapp;

--
-- Name: drive_mixing; Type: TABLE; Schema: public; Owner: ivtapp
--

CREATE TABLE drive_mixing (
    modelid integer NOT NULL,
    alternative_modeleid integer NOT NULL
);


ALTER TABLE drive_mixing OWNER TO ivtapp;

--
-- Name: drive_mode_pages; Type: TABLE; Schema: public; Owner: ivtapp
--

CREATE TABLE drive_mode_pages (
    modelid integer NOT NULL,
    page integer NOT NULL,
    data character varying(128)
);


ALTER TABLE drive_mode_pages OWNER TO ivtapp;

--
-- Name: drive_ssd_rejection_criteria; Type: TABLE; Schema: public; Owner: ivtapp
--

CREATE TABLE drive_ssd_rejection_criteria (
    modelid integer NOT NULL,
    attr integer NOT NULL,
    use_raw boolean,
    comparison character varying(16),
    value integer
);


ALTER TABLE drive_ssd_rejection_criteria OWNER TO ivtapp;

--
-- Name: enclosure; Type: TABLE; Schema: public; Owner: ivtapp
--

CREATE TABLE enclosure (
    id integer NOT NULL,
    model character varying(32),
    revision character varying(32),
    vendorid integer
);


ALTER TABLE enclosure OWNER TO ivtapp;

--
-- Name: enclosure_id_seq; Type: SEQUENCE; Schema: public; Owner: ivtapp
--

CREATE SEQUENCE enclosure_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE enclosure_id_seq OWNER TO ivtapp;

--
-- Name: enclosure_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ivtapp
--

ALTER SEQUENCE enclosure_id_seq OWNED BY enclosure.id;


--
-- Name: enclosure_iom; Type: TABLE; Schema: public; Owner: ivtapp
--

CREATE TABLE enclosure_iom (
    id integer NOT NULL,
    model character varying(32),
    revision character varying(32),
    vendorid integer,
    enclosure_modelid integer
);


ALTER TABLE enclosure_iom OWNER TO ivtapp;

--
-- Name: enclosure_iom_id_seq; Type: SEQUENCE; Schema: public; Owner: ivtapp
--

CREATE SEQUENCE enclosure_iom_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE enclosure_iom_id_seq OWNER TO ivtapp;

--
-- Name: enclosure_iom_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ivtapp
--

ALTER SEQUENCE enclosure_iom_id_seq OWNED BY enclosure_iom.id;


--
-- Name: enclosure_psu; Type: TABLE; Schema: public; Owner: ivtapp
--

CREATE TABLE enclosure_psu (
    id integer NOT NULL,
    model character varying(32),
    revision character varying(32),
    vendorid integer,
    enclosure_modelid integer
);


ALTER TABLE enclosure_psu OWNER TO ivtapp;

--
-- Name: enclosure_psu_id_seq; Type: SEQUENCE; Schema: public; Owner: ivtapp
--

CREATE SEQUENCE enclosure_psu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE enclosure_psu_id_seq OWNER TO ivtapp;

--
-- Name: enclosure_psu_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ivtapp
--

ALTER SEQUENCE enclosure_psu_id_seq OWNED BY enclosure_psu.id;


--
-- Name: ethernet_card; Type: TABLE; Schema: public; Owner: ivtapp
--

CREATE TABLE ethernet_card (
    id integer NOT NULL,
    model character varying(32),
    revision character varying(32),
    vendorid integer
);


ALTER TABLE ethernet_card OWNER TO ivtapp;

--
-- Name: ethernet_card_id_seq; Type: SEQUENCE; Schema: public; Owner: ivtapp
--

CREATE SEQUENCE ethernet_card_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ethernet_card_id_seq OWNER TO ivtapp;

--
-- Name: ethernet_card_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ivtapp
--

ALTER SEQUENCE ethernet_card_id_seq OWNED BY ethernet_card.id;


--
-- Name: fcadapter; Type: TABLE; Schema: public; Owner: ivtapp
--

CREATE TABLE fcadapter (
    id integer NOT NULL,
    model character varying(32),
    revision character varying(32),
    vendorid integer,
    port_speed character varying(16)
);


ALTER TABLE fcadapter OWNER TO ivtapp;

--
-- Name: fcadapter_gbic; Type: TABLE; Schema: public; Owner: ivtapp
--

CREATE TABLE fcadapter_gbic (
    id integer NOT NULL,
    model character varying(32),
    revision character varying(32),
    port_speed character varying(16),
    vendorid integer
);


ALTER TABLE fcadapter_gbic OWNER TO ivtapp;

--
-- Name: fcadapter_gbic_id_seq; Type: SEQUENCE; Schema: public; Owner: ivtapp
--

CREATE SEQUENCE fcadapter_gbic_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE fcadapter_gbic_id_seq OWNER TO ivtapp;

--
-- Name: fcadapter_gbic_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ivtapp
--

ALTER SEQUENCE fcadapter_gbic_id_seq OWNED BY fcadapter_gbic.id;


--
-- Name: fcadapter_id_seq; Type: SEQUENCE; Schema: public; Owner: ivtapp
--

CREATE SEQUENCE fcadapter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE fcadapter_id_seq OWNER TO ivtapp;

--
-- Name: fcadapter_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ivtapp
--

ALTER SEQUENCE fcadapter_id_seq OWNED BY fcadapter.id;


--
-- Name: ibcard; Type: TABLE; Schema: public; Owner: ivtapp
--

CREATE TABLE ibcard (
    id integer NOT NULL,
    model character varying(32),
    revision character varying(32),
    vendorid integer
);


ALTER TABLE ibcard OWNER TO ivtapp;

--
-- Name: ibcard_id_seq; Type: SEQUENCE; Schema: public; Owner: ivtapp
--

CREATE SEQUENCE ibcard_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ibcard_id_seq OWNER TO ivtapp;

--
-- Name: ibcard_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ivtapp
--

ALTER SEQUENCE ibcard_id_seq OWNED BY ibcard.id;


--
-- Name: ibox_sw_release; Type: TABLE; Schema: public; Owner: ivtapp
--

CREATE TABLE ibox_sw_release (
    id integer NOT NULL,
    release character varying(32),
    nodemodeld integer
);


ALTER TABLE ibox_sw_release OWNER TO ivtapp;

--
-- Name: ibox_sw_release_id_seq; Type: SEQUENCE; Schema: public; Owner: ivtapp
--

CREATE SEQUENCE ibox_sw_release_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ibox_sw_release_id_seq OWNER TO ivtapp;

--
-- Name: ibox_sw_release_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ivtapp
--

ALTER SEQUENCE ibox_sw_release_id_seq OWNED BY ibox_sw_release.id;


--
-- Name: node_bios; Type: TABLE; Schema: public; Owner: ivtapp
--

CREATE TABLE node_bios (
    id integer NOT NULL,
    release character varying(16),
    version character varying(32),
    firmware_bin character varying(256),
    firmware_exe character varying(256),
    hwstatus_check boolean,
    nodemodelid integer,
    notes text
);


ALTER TABLE node_bios OWNER TO ivtapp;

--
-- Name: node_bios_id_seq; Type: SEQUENCE; Schema: public; Owner: ivtapp
--

CREATE SEQUENCE node_bios_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE node_bios_id_seq OWNER TO ivtapp;

--
-- Name: node_bios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ivtapp
--

ALTER SEQUENCE node_bios_id_seq OWNED BY node_bios.id;


--
-- Name: node_front_backplane; Type: TABLE; Schema: public; Owner: ivtapp
--

CREATE TABLE node_front_backplane (
    id integer NOT NULL,
    model character varying(32),
    pn character varying(32),
    revision character varying(16),
    version character varying(32),
    firmware_bin character varying(256),
    firmware_exe character varying(256),
    hwstatus_check boolean,
    nodemodelid integer NOT NULL,
    notes text
);


ALTER TABLE node_front_backplane OWNER TO ivtapp;

--
-- Name: node_front_backplane_id_seq; Type: SEQUENCE; Schema: public; Owner: ivtapp
--

CREATE SEQUENCE node_front_backplane_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE node_front_backplane_id_seq OWNER TO ivtapp;

--
-- Name: node_front_backplane_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ivtapp
--

ALTER SEQUENCE node_front_backplane_id_seq OWNED BY node_front_backplane.id;


--
-- Name: node_idrac; Type: TABLE; Schema: public; Owner: ivtapp
--

CREATE TABLE node_idrac (
    id integer NOT NULL,
    model character varying(32),
    version character varying(32),
    firmware_bin character varying(256),
    firmware_exe character varying(256),
    nodemodelid integer,
    hwstatus_check boolean,
    notes text
);


ALTER TABLE node_idrac OWNER TO ivtapp;

--
-- Name: node_idrac_id_seq; Type: SEQUENCE; Schema: public; Owner: ivtapp
--

CREATE SEQUENCE node_idrac_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE node_idrac_id_seq OWNER TO ivtapp;

--
-- Name: node_idrac_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ivtapp
--

ALTER SEQUENCE node_idrac_id_seq OWNED BY node_idrac.id;


--
-- Name: node_lifecycle_controller; Type: TABLE; Schema: public; Owner: ivtapp
--

CREATE TABLE node_lifecycle_controller (
    id integer NOT NULL,
    model character varying(32),
    version character varying(32),
    firmware_bin character varying(256),
    firmware_exe character varying(256),
    nodemodelid integer NOT NULL,
    hwstatus_check boolean,
    notes text
);


ALTER TABLE node_lifecycle_controller OWNER TO ivtapp;

--
-- Name: node_lifecycle_controller_id_seq; Type: SEQUENCE; Schema: public; Owner: ivtapp
--

CREATE SEQUENCE node_lifecycle_controller_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE node_lifecycle_controller_id_seq OWNER TO ivtapp;

--
-- Name: node_lifecycle_controller_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ivtapp
--

ALTER SEQUENCE node_lifecycle_controller_id_seq OWNED BY node_lifecycle_controller.id;


--
-- Name: node_model; Type: TABLE; Schema: public; Owner: ivtapp
--

CREATE TABLE node_model (
    id integer NOT NULL,
    model character varying(32),
    vendorid integer,
    notes text
);


ALTER TABLE node_model OWNER TO ivtapp;

--
-- Name: node_model_id_seq; Type: SEQUENCE; Schema: public; Owner: ivtapp
--

CREATE SEQUENCE node_model_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE node_model_id_seq OWNER TO ivtapp;

--
-- Name: node_model_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ivtapp
--

ALTER SEQUENCE node_model_id_seq OWNED BY node_model.id;


--
-- Name: node_perc; Type: TABLE; Schema: public; Owner: ivtapp
--

CREATE TABLE node_perc (
    id integer NOT NULL,
    model character varying(32),
    version character varying(32),
    firmware_bin character varying(256),
    firmware_exe character varying(256),
    nodemodelid integer NOT NULL,
    hwstatus_check boolean,
    notes text
);


ALTER TABLE node_perc OWNER TO ivtapp;

--
-- Name: node_perc_id_seq; Type: SEQUENCE; Schema: public; Owner: ivtapp
--

CREATE SEQUENCE node_perc_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE node_perc_id_seq OWNER TO ivtapp;

--
-- Name: node_perc_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ivtapp
--

ALTER SEQUENCE node_perc_id_seq OWNED BY node_perc.id;


--
-- Name: node_psu; Type: TABLE; Schema: public; Owner: ivtapp
--

CREATE TABLE node_psu (
    id integer NOT NULL,
    model character varying(32),
    revision character varying(32),
    vendorid integer,
    nodemodelid integer,
    notes text
);


ALTER TABLE node_psu OWNER TO ivtapp;

--
-- Name: node_psu_id_seq; Type: SEQUENCE; Schema: public; Owner: ivtapp
--

CREATE SEQUENCE node_psu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE node_psu_id_seq OWNER TO ivtapp;

--
-- Name: node_psu_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ivtapp
--

ALTER SEQUENCE node_psu_id_seq OWNED BY node_psu.id;


--
-- Name: node_rear_backplane; Type: TABLE; Schema: public; Owner: ivtapp
--

CREATE TABLE node_rear_backplane (
    id integer NOT NULL,
    release character varying(16),
    model character varying(32),
    pn character varying(32),
    revision character varying(16),
    version character varying(32),
    firmware_bin character varying(256),
    firmware_exe character varying(256),
    hwstatus_check boolean,
    nodemodelid integer NOT NULL,
    notes text
);


ALTER TABLE node_rear_backplane OWNER TO ivtapp;

--
-- Name: node_rear_backplane_id_seq; Type: SEQUENCE; Schema: public; Owner: ivtapp
--

CREATE SEQUENCE node_rear_backplane_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE node_rear_backplane_id_seq OWNER TO ivtapp;

--
-- Name: node_rear_backplane_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ivtapp
--

ALTER SEQUENCE node_rear_backplane_id_seq OWNED BY node_rear_backplane.id;


--
-- Name: roles; Type: TABLE; Schema: public; Owner: ivtapp
--

CREATE TABLE roles (
    id integer NOT NULL,
    name character varying(64),
    isdefault boolean DEFAULT false,
    permissions integer
);


ALTER TABLE roles OWNER TO ivtapp;

--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: ivtapp
--

CREATE SEQUENCE roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE roles_id_seq OWNER TO ivtapp;

--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ivtapp
--

ALTER SEQUENCE roles_id_seq OWNED BY roles.id;


--
-- Name: sascard; Type: TABLE; Schema: public; Owner: ivtapp
--

CREATE TABLE sascard (
    id integer NOT NULL,
    model character varying(32),
    revision character varying(32),
    vendorid integer
);


ALTER TABLE sascard OWNER TO ivtapp;

--
-- Name: sascard_id_seq; Type: SEQUENCE; Schema: public; Owner: ivtapp
--

CREATE SEQUENCE sascard_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sascard_id_seq OWNER TO ivtapp;

--
-- Name: sascard_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ivtapp
--

ALTER SEQUENCE sascard_id_seq OWNED BY sascard.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: ivtapp
--

CREATE TABLE users (
    id integer NOT NULL,
    social_id character varying(64),
    email character varying(64),
    username character varying(32),
    name character varying(32),
    given_name character varying(32),
    family_name character varying(32),
    picture character varying(128),
    role_id integer
);


ALTER TABLE users OWNER TO ivtapp;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: ivtapp
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO ivtapp;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ivtapp
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: vendor; Type: TABLE; Schema: public; Owner: ivtapp
--

CREATE TABLE vendor (
    id integer NOT NULL,
    name character varying(128),
    display_name character varying(128),
    hq character varying(128),
    notes text
);


ALTER TABLE vendor OWNER TO ivtapp;

--
-- Name: vendor_contact_map; Type: TABLE; Schema: public; Owner: ivtapp
--

CREATE TABLE vendor_contact_map (
    id integer NOT NULL,
    vendorid integer NOT NULL,
    contactid integer
);


ALTER TABLE vendor_contact_map OWNER TO ivtapp;

--
-- Name: vendor_contact_map_id_seq; Type: SEQUENCE; Schema: public; Owner: ivtapp
--

CREATE SEQUENCE vendor_contact_map_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE vendor_contact_map_id_seq OWNER TO ivtapp;

--
-- Name: vendor_contact_map_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ivtapp
--

ALTER SEQUENCE vendor_contact_map_id_seq OWNED BY vendor_contact_map.id;


--
-- Name: vendor_id_seq; Type: SEQUENCE; Schema: public; Owner: ivtapp
--

CREATE SEQUENCE vendor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE vendor_id_seq OWNER TO ivtapp;

--
-- Name: vendor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ivtapp
--

ALTER SEQUENCE vendor_id_seq OWNED BY vendor.id;


--
-- Name: vendor_ware_map; Type: TABLE; Schema: public; Owner: ivtapp
--

CREATE TABLE vendor_ware_map (
    id integer NOT NULL,
    vendorid integer NOT NULL,
    wareid integer NOT NULL
);


ALTER TABLE vendor_ware_map OWNER TO ivtapp;

--
-- Name: vendor_ware_map_id_seq; Type: SEQUENCE; Schema: public; Owner: ivtapp
--

CREATE SEQUENCE vendor_ware_map_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE vendor_ware_map_id_seq OWNER TO ivtapp;

--
-- Name: vendor_ware_map_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ivtapp
--

ALTER SEQUENCE vendor_ware_map_id_seq OWNED BY vendor_ware_map.id;


--
-- Name: ware; Type: TABLE; Schema: public; Owner: ivtapp
--

CREATE TABLE ware (
    id integer NOT NULL,
    name character varying(128),
    display_name character varying(128)
);


ALTER TABLE ware OWNER TO ivtapp;

--
-- Name: ware_id_seq; Type: SEQUENCE; Schema: public; Owner: ivtapp
--

CREATE SEQUENCE ware_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ware_id_seq OWNER TO ivtapp;

--
-- Name: ware_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ivtapp
--

ALTER SEQUENCE ware_id_seq OWNED BY ware.id;


--
-- Name: changelog id; Type: DEFAULT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY changelog ALTER COLUMN id SET DEFAULT nextval('changelog_id_seq'::regclass);


--
-- Name: contact id; Type: DEFAULT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY contact ALTER COLUMN id SET DEFAULT nextval('contact_id_seq'::regclass);


--
-- Name: drive id; Type: DEFAULT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY drive ALTER COLUMN id SET DEFAULT nextval('drive_id_seq'::regclass);


--
-- Name: enclosure id; Type: DEFAULT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY enclosure ALTER COLUMN id SET DEFAULT nextval('enclosure_id_seq'::regclass);


--
-- Name: enclosure_iom id; Type: DEFAULT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY enclosure_iom ALTER COLUMN id SET DEFAULT nextval('enclosure_iom_id_seq'::regclass);


--
-- Name: enclosure_psu id; Type: DEFAULT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY enclosure_psu ALTER COLUMN id SET DEFAULT nextval('enclosure_psu_id_seq'::regclass);


--
-- Name: ethernet_card id; Type: DEFAULT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY ethernet_card ALTER COLUMN id SET DEFAULT nextval('ethernet_card_id_seq'::regclass);


--
-- Name: fcadapter id; Type: DEFAULT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY fcadapter ALTER COLUMN id SET DEFAULT nextval('fcadapter_id_seq'::regclass);


--
-- Name: fcadapter_gbic id; Type: DEFAULT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY fcadapter_gbic ALTER COLUMN id SET DEFAULT nextval('fcadapter_gbic_id_seq'::regclass);


--
-- Name: ibcard id; Type: DEFAULT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY ibcard ALTER COLUMN id SET DEFAULT nextval('ibcard_id_seq'::regclass);


--
-- Name: ibox_sw_release id; Type: DEFAULT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY ibox_sw_release ALTER COLUMN id SET DEFAULT nextval('ibox_sw_release_id_seq'::regclass);


--
-- Name: node_bios id; Type: DEFAULT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_bios ALTER COLUMN id SET DEFAULT nextval('node_bios_id_seq'::regclass);


--
-- Name: node_front_backplane id; Type: DEFAULT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_front_backplane ALTER COLUMN id SET DEFAULT nextval('node_front_backplane_id_seq'::regclass);


--
-- Name: node_idrac id; Type: DEFAULT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_idrac ALTER COLUMN id SET DEFAULT nextval('node_idrac_id_seq'::regclass);


--
-- Name: node_lifecycle_controller id; Type: DEFAULT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_lifecycle_controller ALTER COLUMN id SET DEFAULT nextval('node_lifecycle_controller_id_seq'::regclass);


--
-- Name: node_model id; Type: DEFAULT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_model ALTER COLUMN id SET DEFAULT nextval('node_model_id_seq'::regclass);


--
-- Name: node_perc id; Type: DEFAULT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_perc ALTER COLUMN id SET DEFAULT nextval('node_perc_id_seq'::regclass);


--
-- Name: node_psu id; Type: DEFAULT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_psu ALTER COLUMN id SET DEFAULT nextval('node_psu_id_seq'::regclass);


--
-- Name: node_rear_backplane id; Type: DEFAULT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_rear_backplane ALTER COLUMN id SET DEFAULT nextval('node_rear_backplane_id_seq'::regclass);


--
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY roles ALTER COLUMN id SET DEFAULT nextval('roles_id_seq'::regclass);


--
-- Name: sascard id; Type: DEFAULT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY sascard ALTER COLUMN id SET DEFAULT nextval('sascard_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: vendor id; Type: DEFAULT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY vendor ALTER COLUMN id SET DEFAULT nextval('vendor_id_seq'::regclass);


--
-- Name: vendor_contact_map id; Type: DEFAULT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY vendor_contact_map ALTER COLUMN id SET DEFAULT nextval('vendor_contact_map_id_seq'::regclass);


--
-- Name: vendor_ware_map id; Type: DEFAULT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY vendor_ware_map ALTER COLUMN id SET DEFAULT nextval('vendor_ware_map_id_seq'::regclass);


--
-- Name: ware id; Type: DEFAULT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY ware ALTER COLUMN id SET DEFAULT nextval('ware_id_seq'::regclass);


--
-- Data for Name: changelog; Type: TABLE DATA; Schema: public; Owner: ivtapp
--

COPY changelog (id, version, name, applied) FROM stdin;
1	000000	baseline	2017-10-25 10:22:48.136119
2	000002	vendor_contact	2017-10-25 10:22:48.192025
3	000003	vendor_ware	2017-10-25 10:22:48.226109
4	000004	create_node_model_table	2017-10-25 10:22:48.242798
5	000005	add_notes_field_node_model	2017-10-25 10:22:48.253993
6	000010	create_ibox_sw_release_table	2017-10-25 10:22:48.282886
7	000020	create_psu_table	2017-10-25 10:22:48.31348
8	000030	create_bios_table	2017-10-25 10:22:48.333039
9	000040	create_idrac_table	2017-10-25 10:22:48.351601
10	000050	create_lifecycle_controller_table	2017-10-25 10:22:48.381349
11	000060	create_perc_table	2017-10-25 10:22:48.4164
12	000070	create_front_backplane_table	2017-10-25 10:22:48.442515
13	000080	create_rear_backplane_table	2017-10-25 10:22:48.483793
14	000090	create_ethcard_table	2017-10-25 10:22:48.501918
15	000100	create_ibcard_table	2017-10-25 10:22:48.521129
16	000110	create_fcadapter_table	2017-10-25 10:22:48.538113
17	000120	create_fcadapter_gbic_table	2017-10-25 10:22:48.56382
18	000130	create_sas_card_table	2017-10-25 10:22:48.590137
19	000140	create_drive_table	2017-10-25 10:22:48.614146
20	000150	drive_ssd_rejection_criteria	2017-10-25 10:22:48.630358
21	000160	drive_hdd_rejection_criteria	2017-10-25 10:22:48.640066
22	000170	drive_inquiry	2017-10-25 10:22:48.648213
23	000180	drive_mode_pages	2017-10-25 10:22:48.65694
24	000190	drive_mixing	2017-10-25 10:22:48.6716
25	000200	create_enclosure_table	2017-10-25 10:22:48.692822
26	000210	create_enclosure_psu_table	2017-10-25 10:22:48.718568
27	000220	create_enclosure_iom_table	2017-10-25 10:22:48.738231
28	000225	alter_node_bios_table	2017-10-30 13:58:39.52152
\.


--
-- Name: changelog_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ivtapp
--

SELECT pg_catalog.setval('changelog_id_seq', 28, true);


--
-- Data for Name: contact; Type: TABLE DATA; Schema: public; Owner: ivtapp
--

COPY contact (id, name, email, phone_desk, phone_mobile, notes) FROM stdin;
1	John Doe	johnd@dell.com			\N
2	Gerald Lange	gerald.lange@sanmina.com			\N
3	Bobo Mintz	bobom@ibm.com			\N
4	Rob Jones	Rob.Jones@cavium.com	949.542.1451	949.340.4513	\N
\.


--
-- Name: contact_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ivtapp
--

SELECT pg_catalog.setval('contact_id_seq', 4, true);


--
-- Data for Name: drive; Type: TABLE DATA; Schema: public; Owner: ivtapp
--

COPY drive (id, model, revision, vendorid, is_ssd, protocol) FROM stdin;
\.


--
-- Data for Name: drive_hdd_rejection_criteria; Type: TABLE DATA; Schema: public; Owner: ivtapp
--

COPY drive_hdd_rejection_criteria (modelid, logpage, counter_byte_offset, counter_length, comparison, value) FROM stdin;
\.


--
-- Name: drive_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ivtapp
--

SELECT pg_catalog.setval('drive_id_seq', 1, false);


--
-- Data for Name: drive_inquiry; Type: TABLE DATA; Schema: public; Owner: ivtapp
--

COPY drive_inquiry (modelid, page, data) FROM stdin;
\.


--
-- Data for Name: drive_mixing; Type: TABLE DATA; Schema: public; Owner: ivtapp
--

COPY drive_mixing (modelid, alternative_modeleid) FROM stdin;
\.


--
-- Data for Name: drive_mode_pages; Type: TABLE DATA; Schema: public; Owner: ivtapp
--

COPY drive_mode_pages (modelid, page, data) FROM stdin;
\.


--
-- Data for Name: drive_ssd_rejection_criteria; Type: TABLE DATA; Schema: public; Owner: ivtapp
--

COPY drive_ssd_rejection_criteria (modelid, attr, use_raw, comparison, value) FROM stdin;
\.


--
-- Data for Name: enclosure; Type: TABLE DATA; Schema: public; Owner: ivtapp
--

COPY enclosure (id, model, revision, vendorid) FROM stdin;
\.


--
-- Name: enclosure_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ivtapp
--

SELECT pg_catalog.setval('enclosure_id_seq', 1, false);


--
-- Data for Name: enclosure_iom; Type: TABLE DATA; Schema: public; Owner: ivtapp
--

COPY enclosure_iom (id, model, revision, vendorid, enclosure_modelid) FROM stdin;
\.


--
-- Name: enclosure_iom_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ivtapp
--

SELECT pg_catalog.setval('enclosure_iom_id_seq', 1, false);


--
-- Data for Name: enclosure_psu; Type: TABLE DATA; Schema: public; Owner: ivtapp
--

COPY enclosure_psu (id, model, revision, vendorid, enclosure_modelid) FROM stdin;
\.


--
-- Name: enclosure_psu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ivtapp
--

SELECT pg_catalog.setval('enclosure_psu_id_seq', 1, false);


--
-- Data for Name: ethernet_card; Type: TABLE DATA; Schema: public; Owner: ivtapp
--

COPY ethernet_card (id, model, revision, vendorid) FROM stdin;
\.


--
-- Name: ethernet_card_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ivtapp
--

SELECT pg_catalog.setval('ethernet_card_id_seq', 1, false);


--
-- Data for Name: fcadapter; Type: TABLE DATA; Schema: public; Owner: ivtapp
--

COPY fcadapter (id, model, revision, vendorid, port_speed) FROM stdin;
\.


--
-- Data for Name: fcadapter_gbic; Type: TABLE DATA; Schema: public; Owner: ivtapp
--

COPY fcadapter_gbic (id, model, revision, port_speed, vendorid) FROM stdin;
\.


--
-- Name: fcadapter_gbic_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ivtapp
--

SELECT pg_catalog.setval('fcadapter_gbic_id_seq', 1, false);


--
-- Name: fcadapter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ivtapp
--

SELECT pg_catalog.setval('fcadapter_id_seq', 1, false);


--
-- Data for Name: ibcard; Type: TABLE DATA; Schema: public; Owner: ivtapp
--

COPY ibcard (id, model, revision, vendorid) FROM stdin;
\.


--
-- Name: ibcard_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ivtapp
--

SELECT pg_catalog.setval('ibcard_id_seq', 1, false);


--
-- Data for Name: ibox_sw_release; Type: TABLE DATA; Schema: public; Owner: ivtapp
--

COPY ibox_sw_release (id, release, nodemodeld) FROM stdin;
\.


--
-- Name: ibox_sw_release_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ivtapp
--

SELECT pg_catalog.setval('ibox_sw_release_id_seq', 1, false);


--
-- Data for Name: node_bios; Type: TABLE DATA; Schema: public; Owner: ivtapp
--

COPY node_bios (id, release, version, firmware_bin, firmware_exe, hwstatus_check, nodemodelid, notes) FROM stdin;
\.


--
-- Name: node_bios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ivtapp
--

SELECT pg_catalog.setval('node_bios_id_seq', 1, false);


--
-- Data for Name: node_front_backplane; Type: TABLE DATA; Schema: public; Owner: ivtapp
--

COPY node_front_backplane (id, model, pn, revision, version, firmware_bin, firmware_exe, hwstatus_check, nodemodelid, notes) FROM stdin;
\.


--
-- Name: node_front_backplane_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ivtapp
--

SELECT pg_catalog.setval('node_front_backplane_id_seq', 1, false);


--
-- Data for Name: node_idrac; Type: TABLE DATA; Schema: public; Owner: ivtapp
--

COPY node_idrac (id, model, version, firmware_bin, firmware_exe, nodemodelid, hwstatus_check, notes) FROM stdin;
\.


--
-- Name: node_idrac_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ivtapp
--

SELECT pg_catalog.setval('node_idrac_id_seq', 1, false);


--
-- Data for Name: node_lifecycle_controller; Type: TABLE DATA; Schema: public; Owner: ivtapp
--

COPY node_lifecycle_controller (id, model, version, firmware_bin, firmware_exe, nodemodelid, hwstatus_check, notes) FROM stdin;
\.


--
-- Name: node_lifecycle_controller_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ivtapp
--

SELECT pg_catalog.setval('node_lifecycle_controller_id_seq', 1, false);


--
-- Data for Name: node_model; Type: TABLE DATA; Schema: public; Owner: ivtapp
--

COPY node_model (id, model, vendorid, notes) FROM stdin;
1	R720	1	Deprecated
2	GX6000	3	Not really
3	R730XD	1	Current qualified model.                                        
4	R740	1	Not qualified yet.                                        
\.


--
-- Name: node_model_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ivtapp
--

SELECT pg_catalog.setval('node_model_id_seq', 4, true);


--
-- Data for Name: node_perc; Type: TABLE DATA; Schema: public; Owner: ivtapp
--

COPY node_perc (id, model, version, firmware_bin, firmware_exe, nodemodelid, hwstatus_check, notes) FROM stdin;
\.


--
-- Name: node_perc_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ivtapp
--

SELECT pg_catalog.setval('node_perc_id_seq', 1, false);


--
-- Data for Name: node_psu; Type: TABLE DATA; Schema: public; Owner: ivtapp
--

COPY node_psu (id, model, revision, vendorid, nodemodelid, notes) FROM stdin;
\.


--
-- Name: node_psu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ivtapp
--

SELECT pg_catalog.setval('node_psu_id_seq', 1, false);


--
-- Data for Name: node_rear_backplane; Type: TABLE DATA; Schema: public; Owner: ivtapp
--

COPY node_rear_backplane (id, release, model, pn, revision, version, firmware_bin, firmware_exe, hwstatus_check, nodemodelid, notes) FROM stdin;
\.


--
-- Name: node_rear_backplane_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ivtapp
--

SELECT pg_catalog.setval('node_rear_backplane_id_seq', 1, false);


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: ivtapp
--

COPY roles (id, name, isdefault, permissions) FROM stdin;
\.


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ivtapp
--

SELECT pg_catalog.setval('roles_id_seq', 1, false);


--
-- Data for Name: sascard; Type: TABLE DATA; Schema: public; Owner: ivtapp
--

COPY sascard (id, model, revision, vendorid) FROM stdin;
\.


--
-- Name: sascard_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ivtapp
--

SELECT pg_catalog.setval('sascard_id_seq', 1, false);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: ivtapp
--

COPY users (id, social_id, email, username, name, given_name, family_name, picture, role_id) FROM stdin;
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ivtapp
--

SELECT pg_catalog.setval('users_id_seq', 1, false);


--
-- Data for Name: vendor; Type: TABLE DATA; Schema: public; Owner: ivtapp
--

COPY vendor (id, name, display_name, hq, notes) FROM stdin;
1	dell	Dell Inc		\N
2	sanmina	Sanmina	San Jose, California, United States	\N
3	ibm	Ibm		\N
4	qlogic	QLogic		\N
\.


--
-- Data for Name: vendor_contact_map; Type: TABLE DATA; Schema: public; Owner: ivtapp
--

COPY vendor_contact_map (id, vendorid, contactid) FROM stdin;
1	1	1
2	2	2
3	3	3
4	4	4
\.


--
-- Name: vendor_contact_map_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ivtapp
--

SELECT pg_catalog.setval('vendor_contact_map_id_seq', 4, true);


--
-- Name: vendor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ivtapp
--

SELECT pg_catalog.setval('vendor_id_seq', 4, true);


--
-- Data for Name: vendor_ware_map; Type: TABLE DATA; Schema: public; Owner: ivtapp
--

COPY vendor_ware_map (id, vendorid, wareid) FROM stdin;
1	1	1
2	2	4
3	3	1
4	4	5
\.


--
-- Name: vendor_ware_map_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ivtapp
--

SELECT pg_catalog.setval('vendor_ware_map_id_seq', 4, true);


--
-- Data for Name: ware; Type: TABLE DATA; Schema: public; Owner: ivtapp
--

COPY ware (id, name, display_name) FROM stdin;
1	server	Server
2	ssd	SSD
3	hhd	HHD
4	enclosure	Enclosure
5	fc_card	FC Card
6	ib_card	IB Card
\.


--
-- Name: ware_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ivtapp
--

SELECT pg_catalog.setval('ware_id_seq', 10, true);


--
-- Name: changelog changelog_name_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY changelog
    ADD CONSTRAINT changelog_name_key UNIQUE (name);


--
-- Name: changelog changelog_pkey; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY changelog
    ADD CONSTRAINT changelog_pkey PRIMARY KEY (id);


--
-- Name: contact contact_email_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY contact
    ADD CONSTRAINT contact_email_key UNIQUE (email);


--
-- Name: contact contact_pkey; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY contact
    ADD CONSTRAINT contact_pkey PRIMARY KEY (id);


--
-- Name: drive_hdd_rejection_criteria drive_hdd_rejection_criteria_pkey; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY drive_hdd_rejection_criteria
    ADD CONSTRAINT drive_hdd_rejection_criteria_pkey PRIMARY KEY (modelid, logpage, counter_byte_offset);


--
-- Name: drive_inquiry drive_inquiry_pkey; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY drive_inquiry
    ADD CONSTRAINT drive_inquiry_pkey PRIMARY KEY (modelid, page);


--
-- Name: drive_mixing drive_mixing_pkey; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY drive_mixing
    ADD CONSTRAINT drive_mixing_pkey PRIMARY KEY (modelid, alternative_modeleid);


--
-- Name: drive_mode_pages drive_mode_pages_pkey; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY drive_mode_pages
    ADD CONSTRAINT drive_mode_pages_pkey PRIMARY KEY (modelid, page);


--
-- Name: drive drive_model_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY drive
    ADD CONSTRAINT drive_model_key UNIQUE (model);


--
-- Name: drive drive_pkey; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY drive
    ADD CONSTRAINT drive_pkey PRIMARY KEY (id);


--
-- Name: drive_ssd_rejection_criteria drive_ssd_rejection_criteria_pkey; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY drive_ssd_rejection_criteria
    ADD CONSTRAINT drive_ssd_rejection_criteria_pkey PRIMARY KEY (modelid, attr);


--
-- Name: enclosure_iom enclosure_iom_model_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY enclosure_iom
    ADD CONSTRAINT enclosure_iom_model_key UNIQUE (model);


--
-- Name: enclosure_iom enclosure_iom_pkey; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY enclosure_iom
    ADD CONSTRAINT enclosure_iom_pkey PRIMARY KEY (id);


--
-- Name: enclosure enclosure_model_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY enclosure
    ADD CONSTRAINT enclosure_model_key UNIQUE (model);


--
-- Name: enclosure enclosure_pkey; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY enclosure
    ADD CONSTRAINT enclosure_pkey PRIMARY KEY (id);


--
-- Name: enclosure_psu enclosure_psu_model_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY enclosure_psu
    ADD CONSTRAINT enclosure_psu_model_key UNIQUE (model);


--
-- Name: enclosure_psu enclosure_psu_pkey; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY enclosure_psu
    ADD CONSTRAINT enclosure_psu_pkey PRIMARY KEY (id);


--
-- Name: ethernet_card ethernet_card_model_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY ethernet_card
    ADD CONSTRAINT ethernet_card_model_key UNIQUE (model);


--
-- Name: ethernet_card ethernet_card_pkey; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY ethernet_card
    ADD CONSTRAINT ethernet_card_pkey PRIMARY KEY (id);


--
-- Name: fcadapter_gbic fcadapter_gbic_model_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY fcadapter_gbic
    ADD CONSTRAINT fcadapter_gbic_model_key UNIQUE (model);


--
-- Name: fcadapter_gbic fcadapter_gbic_pkey; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY fcadapter_gbic
    ADD CONSTRAINT fcadapter_gbic_pkey PRIMARY KEY (id);


--
-- Name: fcadapter fcadapter_model_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY fcadapter
    ADD CONSTRAINT fcadapter_model_key UNIQUE (model);


--
-- Name: fcadapter fcadapter_pkey; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY fcadapter
    ADD CONSTRAINT fcadapter_pkey PRIMARY KEY (id);


--
-- Name: ibcard ibcard_model_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY ibcard
    ADD CONSTRAINT ibcard_model_key UNIQUE (model);


--
-- Name: ibcard ibcard_pkey; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY ibcard
    ADD CONSTRAINT ibcard_pkey PRIMARY KEY (id);


--
-- Name: ibox_sw_release ibox_sw_release_pkey; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY ibox_sw_release
    ADD CONSTRAINT ibox_sw_release_pkey PRIMARY KEY (id);


--
-- Name: ibox_sw_release ibox_sw_release_release_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY ibox_sw_release
    ADD CONSTRAINT ibox_sw_release_release_key UNIQUE (release);


--
-- Name: node_bios node_bios_pkey; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_bios
    ADD CONSTRAINT node_bios_pkey PRIMARY KEY (id);


--
-- Name: node_bios node_bios_version_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_bios
    ADD CONSTRAINT node_bios_version_key UNIQUE (version);


--
-- Name: node_front_backplane node_front_backplane_model_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_front_backplane
    ADD CONSTRAINT node_front_backplane_model_key UNIQUE (model);


--
-- Name: node_front_backplane node_front_backplane_pkey; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_front_backplane
    ADD CONSTRAINT node_front_backplane_pkey PRIMARY KEY (id);


--
-- Name: node_front_backplane node_front_backplane_pn_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_front_backplane
    ADD CONSTRAINT node_front_backplane_pn_key UNIQUE (pn);


--
-- Name: node_front_backplane node_front_backplane_version_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_front_backplane
    ADD CONSTRAINT node_front_backplane_version_key UNIQUE (version);


--
-- Name: node_idrac node_idrac_model_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_idrac
    ADD CONSTRAINT node_idrac_model_key UNIQUE (model);


--
-- Name: node_idrac node_idrac_pkey; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_idrac
    ADD CONSTRAINT node_idrac_pkey PRIMARY KEY (id);


--
-- Name: node_lifecycle_controller node_lifecycle_controller_model_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_lifecycle_controller
    ADD CONSTRAINT node_lifecycle_controller_model_key UNIQUE (model);


--
-- Name: node_lifecycle_controller node_lifecycle_controller_pkey; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_lifecycle_controller
    ADD CONSTRAINT node_lifecycle_controller_pkey PRIMARY KEY (id);


--
-- Name: node_model node_model_model_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_model
    ADD CONSTRAINT node_model_model_key UNIQUE (model);


--
-- Name: node_model node_model_pkey; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_model
    ADD CONSTRAINT node_model_pkey PRIMARY KEY (id);


--
-- Name: node_perc node_perc_model_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_perc
    ADD CONSTRAINT node_perc_model_key UNIQUE (model);


--
-- Name: node_perc node_perc_pkey; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_perc
    ADD CONSTRAINT node_perc_pkey PRIMARY KEY (id);


--
-- Name: node_perc node_perc_version_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_perc
    ADD CONSTRAINT node_perc_version_key UNIQUE (version);


--
-- Name: node_psu node_psu_model_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_psu
    ADD CONSTRAINT node_psu_model_key UNIQUE (model);


--
-- Name: node_psu node_psu_pkey; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_psu
    ADD CONSTRAINT node_psu_pkey PRIMARY KEY (id);


--
-- Name: node_rear_backplane node_rear_backplane_model_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_rear_backplane
    ADD CONSTRAINT node_rear_backplane_model_key UNIQUE (model);


--
-- Name: node_rear_backplane node_rear_backplane_pkey; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_rear_backplane
    ADD CONSTRAINT node_rear_backplane_pkey PRIMARY KEY (id);


--
-- Name: node_rear_backplane node_rear_backplane_pn_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_rear_backplane
    ADD CONSTRAINT node_rear_backplane_pn_key UNIQUE (pn);


--
-- Name: node_rear_backplane node_rear_backplane_release_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_rear_backplane
    ADD CONSTRAINT node_rear_backplane_release_key UNIQUE (release);


--
-- Name: node_rear_backplane node_rear_backplane_version_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_rear_backplane
    ADD CONSTRAINT node_rear_backplane_version_key UNIQUE (version);


--
-- Name: roles roles_name_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_name_key UNIQUE (name);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: sascard sascard_model_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY sascard
    ADD CONSTRAINT sascard_model_key UNIQUE (model);


--
-- Name: sascard sascard_pkey; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY sascard
    ADD CONSTRAINT sascard_pkey PRIMARY KEY (id);


--
-- Name: users users_email_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_email_key UNIQUE (email);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users users_social_id_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_social_id_key UNIQUE (social_id);


--
-- Name: vendor_contact_map vendor_contact_map_pkey; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY vendor_contact_map
    ADD CONSTRAINT vendor_contact_map_pkey PRIMARY KEY (id);


--
-- Name: vendor_contact_map vendor_contact_map_vendorid_contactid_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY vendor_contact_map
    ADD CONSTRAINT vendor_contact_map_vendorid_contactid_key UNIQUE (vendorid, contactid);


--
-- Name: vendor vendor_name_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY vendor
    ADD CONSTRAINT vendor_name_key UNIQUE (name);


--
-- Name: vendor vendor_pkey; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY vendor
    ADD CONSTRAINT vendor_pkey PRIMARY KEY (id);


--
-- Name: vendor_ware_map vendor_ware_map_pkey; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY vendor_ware_map
    ADD CONSTRAINT vendor_ware_map_pkey PRIMARY KEY (id);


--
-- Name: vendor_ware_map vendor_ware_map_vendorid_wareid_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY vendor_ware_map
    ADD CONSTRAINT vendor_ware_map_vendorid_wareid_key UNIQUE (vendorid, wareid);


--
-- Name: ware ware_name_key; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY ware
    ADD CONSTRAINT ware_name_key UNIQUE (name);


--
-- Name: ware ware_pkey; Type: CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY ware
    ADD CONSTRAINT ware_pkey PRIMARY KEY (id);


--
-- Name: drive_hdd_rejection_criteria drive_hdd_rejection_criteria_modelid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY drive_hdd_rejection_criteria
    ADD CONSTRAINT drive_hdd_rejection_criteria_modelid_fkey FOREIGN KEY (modelid) REFERENCES drive(id);


--
-- Name: drive_inquiry drive_inquiry_modelid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY drive_inquiry
    ADD CONSTRAINT drive_inquiry_modelid_fkey FOREIGN KEY (modelid) REFERENCES drive(id);


--
-- Name: drive_mixing drive_mixing_alternative_modeleid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY drive_mixing
    ADD CONSTRAINT drive_mixing_alternative_modeleid_fkey FOREIGN KEY (alternative_modeleid) REFERENCES drive(id);


--
-- Name: drive_mixing drive_mixing_modelid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY drive_mixing
    ADD CONSTRAINT drive_mixing_modelid_fkey FOREIGN KEY (modelid) REFERENCES drive(id);


--
-- Name: drive_mode_pages drive_mode_pages_modelid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY drive_mode_pages
    ADD CONSTRAINT drive_mode_pages_modelid_fkey FOREIGN KEY (modelid) REFERENCES drive(id);


--
-- Name: drive_ssd_rejection_criteria drive_ssd_rejection_criteria_modelid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY drive_ssd_rejection_criteria
    ADD CONSTRAINT drive_ssd_rejection_criteria_modelid_fkey FOREIGN KEY (modelid) REFERENCES drive(id);


--
-- Name: drive drive_vendorid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY drive
    ADD CONSTRAINT drive_vendorid_fkey FOREIGN KEY (vendorid) REFERENCES vendor(id);


--
-- Name: enclosure_iom enclosure_iom_enclosure_modelid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY enclosure_iom
    ADD CONSTRAINT enclosure_iom_enclosure_modelid_fkey FOREIGN KEY (enclosure_modelid) REFERENCES enclosure(id);


--
-- Name: enclosure_iom enclosure_iom_vendorid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY enclosure_iom
    ADD CONSTRAINT enclosure_iom_vendorid_fkey FOREIGN KEY (vendorid) REFERENCES vendor(id);


--
-- Name: enclosure_psu enclosure_psu_enclosure_modelid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY enclosure_psu
    ADD CONSTRAINT enclosure_psu_enclosure_modelid_fkey FOREIGN KEY (enclosure_modelid) REFERENCES enclosure(id);


--
-- Name: enclosure_psu enclosure_psu_vendorid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY enclosure_psu
    ADD CONSTRAINT enclosure_psu_vendorid_fkey FOREIGN KEY (vendorid) REFERENCES vendor(id);


--
-- Name: enclosure enclosure_vendorid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY enclosure
    ADD CONSTRAINT enclosure_vendorid_fkey FOREIGN KEY (vendorid) REFERENCES vendor(id);


--
-- Name: ethernet_card ethernet_card_vendorid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY ethernet_card
    ADD CONSTRAINT ethernet_card_vendorid_fkey FOREIGN KEY (vendorid) REFERENCES vendor(id);


--
-- Name: fcadapter_gbic fcadapter_gbic_vendorid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY fcadapter_gbic
    ADD CONSTRAINT fcadapter_gbic_vendorid_fkey FOREIGN KEY (vendorid) REFERENCES vendor(id);


--
-- Name: fcadapter fcadapter_vendorid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY fcadapter
    ADD CONSTRAINT fcadapter_vendorid_fkey FOREIGN KEY (vendorid) REFERENCES vendor(id);


--
-- Name: ibcard ibcard_vendorid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY ibcard
    ADD CONSTRAINT ibcard_vendorid_fkey FOREIGN KEY (vendorid) REFERENCES vendor(id);


--
-- Name: ibox_sw_release ibox_sw_release_nodemodeld_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY ibox_sw_release
    ADD CONSTRAINT ibox_sw_release_nodemodeld_fkey FOREIGN KEY (nodemodeld) REFERENCES node_model(id);


--
-- Name: node_bios node_bios_nodemodelid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_bios
    ADD CONSTRAINT node_bios_nodemodelid_fkey FOREIGN KEY (nodemodelid) REFERENCES node_model(id);


--
-- Name: node_front_backplane node_front_backplane_nodemodelid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_front_backplane
    ADD CONSTRAINT node_front_backplane_nodemodelid_fkey FOREIGN KEY (nodemodelid) REFERENCES node_model(id);


--
-- Name: node_idrac node_idrac_nodemodelid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_idrac
    ADD CONSTRAINT node_idrac_nodemodelid_fkey FOREIGN KEY (nodemodelid) REFERENCES node_model(id);


--
-- Name: node_lifecycle_controller node_lifecycle_controller_nodemodelid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_lifecycle_controller
    ADD CONSTRAINT node_lifecycle_controller_nodemodelid_fkey FOREIGN KEY (nodemodelid) REFERENCES node_model(id);


--
-- Name: node_model node_model_vendorid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_model
    ADD CONSTRAINT node_model_vendorid_fkey FOREIGN KEY (vendorid) REFERENCES vendor(id);


--
-- Name: node_perc node_perc_nodemodelid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_perc
    ADD CONSTRAINT node_perc_nodemodelid_fkey FOREIGN KEY (nodemodelid) REFERENCES node_model(id);


--
-- Name: node_psu node_psu_nodemodelid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_psu
    ADD CONSTRAINT node_psu_nodemodelid_fkey FOREIGN KEY (nodemodelid) REFERENCES node_model(id);


--
-- Name: node_psu node_psu_vendorid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_psu
    ADD CONSTRAINT node_psu_vendorid_fkey FOREIGN KEY (vendorid) REFERENCES vendor(id);


--
-- Name: node_rear_backplane node_rear_backplane_nodemodelid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY node_rear_backplane
    ADD CONSTRAINT node_rear_backplane_nodemodelid_fkey FOREIGN KEY (nodemodelid) REFERENCES node_model(id);


--
-- Name: sascard sascard_vendorid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY sascard
    ADD CONSTRAINT sascard_vendorid_fkey FOREIGN KEY (vendorid) REFERENCES vendor(id);


--
-- Name: users users_role_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_role_id_fkey FOREIGN KEY (role_id) REFERENCES roles(id);


--
-- Name: vendor_contact_map vendor_contact_map_contactid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY vendor_contact_map
    ADD CONSTRAINT vendor_contact_map_contactid_fkey FOREIGN KEY (contactid) REFERENCES contact(id);


--
-- Name: vendor_contact_map vendor_contact_map_vendorid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY vendor_contact_map
    ADD CONSTRAINT vendor_contact_map_vendorid_fkey FOREIGN KEY (vendorid) REFERENCES vendor(id);


--
-- Name: vendor_ware_map vendor_ware_map_vendorid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY vendor_ware_map
    ADD CONSTRAINT vendor_ware_map_vendorid_fkey FOREIGN KEY (vendorid) REFERENCES vendor(id);


--
-- Name: vendor_ware_map vendor_ware_map_wareid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivtapp
--

ALTER TABLE ONLY vendor_ware_map
    ADD CONSTRAINT vendor_ware_map_wareid_fkey FOREIGN KEY (wareid) REFERENCES ware(id);


--
-- PostgreSQL database dump complete
--

