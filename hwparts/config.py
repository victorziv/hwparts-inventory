import os
import sys
import logging

logger = None
BASEDIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# ==============================================


class Permission:
    FOLLOW = 0x01               # 0b00000001
    COMMENT = 0x02              # 0b00000010
    WRITE_ARTICLES = 0x04       # 0b00000100
    MODERATE_COMMENTS = 0x08    # 0b00001000
    ADMINISTER = 0x80           # 0b10000000
# =========================================


class Config:
    PROJECT = 'hwparts'
    PROJECT_USER = 'ivtapp'
    BASEDIR = os.path.abspath(os.path.dirname(__file__))
    LOGPATH = os.path.join(os.path.dirname(BASEDIR), 'logs')
    API_URL_PREFIX = '/hwparts/api/v1.0'
    API_2_URL_PREFIX = '/hwparts/api/v2.0'
    SECRET_KEY = os.environ.get('SECRET_KEY') \
        or "est-ce que c'est la voiture numero huit?"
    ADMIN_USER = os.environ.get('ADMIN_USER') \
        or '%s@infinidat.com' % PROJECT_USER

    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 465
    MAIL_USE_TLS = True
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')

    MAIL_SUBJECT_PREFIX = '[%s]' % PROJECT.capitalize()
    MAIL_SENDER = 'Admin %s' % ADMIN_USER
    TEST_OWNER_DEFAULT = '%s@nowhere.com' % PROJECT_USER

    DBHOST = 'localhost'
    DBPORT = 5432
    DBUSER = PROJECT_USER
    DBPASSWORD = PROJECT_USER

    DB_CONNECTION_PARAMS = dict(
        dbhost=DBHOST,
        dbport=DBPORT,
        dbuser=DBUSER,
        dbpassword=DBPASSWORD
    )

    DB_URI_FORMAT = 'postgresql://{dbuser}:{dbpassword}@{dbhost}:{dbport}/{dbname}'
    DBNAME_ADMIN = 'postgres'
    DB_CONN_URI_ADMIN = DB_URI_FORMAT.format(
        dbname=DBNAME_ADMIN,
        **DB_CONNECTION_PARAMS
    )

    AUTH_TOKEN_EXPIRATION = 3600
    OAUTH_CREDENTIALS = {

        "google": {
            "id": "261888576370-pn751o1qrqbcu662nmr3a24nr9cf3eku.apps.googleusercontent.com",
            "secret": "TDwCUl-WzhOjGw6x_P5bYxtH"
        }
    }

    MIGRATIONS_DIR = "migrations"
    MIGRATIONS_MODULE = "migrations"

    JIRA_USER = 'ivt'
    JIRA_PASS = 'f30QWDKq'
    JIRA_OPTIONS = {
        'server': 'https://jira.infinidat.com',
        'verify': False
    }

    # ___________________________________________

    @staticmethod
    def create_directory(path):
        if not os.path.exists(path):
            try:
                os.makedirs(path)
            except OSError as e:
                if e.errno == 17:
                    pass
    # _________________________________

    @classmethod
    def set_logging_level(cls, logging_level):
        for h in cls.logger.handlers:
            h.setLevel(getattr(logging, logging_level.upper()))
# _________________________________

    @classmethod
    def set_logging(cls, name, session='', loglevel='DEBUG', console_logging=False, console_loglevel='INFO'):

        global logger
        logger = logging.getLogger(name)
        logger.setLevel(getattr(logging, loglevel.upper()))
        logger.propogate = False
        logfile = os.path.join(Config.LOGPATH, session, '%s.log' % name)
        logger.addHandler(Config.log_to_file(name, session, logfile, loglevel))

        if console_logging:
            logger.addHandler(Config.log_to_console(console_loglevel))
            logger.info("Log path: %s", logfile)

    # ________________________________________

    @staticmethod
    def log_to_console(logging_level, out_to='stderr'):
        logformat = '%(asctime)s - %(levelname)-10s %(message)s'
        handler = logging.StreamHandler(getattr(sys, out_to))
        handler.setLevel(getattr(logging, logging_level.upper()))
        handler.setFormatter(logging.Formatter(logformat))
        return handler
    # ___________________________________________

    @staticmethod
    def log_to_file(name, session, logfile, logging_level):
        logformat = '%(asctime)s %(process)d %(levelname)-10s %(module)s %(funcName)-4s %(message)s'
        Config.create_directory(os.path.dirname(logfile))

        handler = logging.FileHandler(logfile)

        handler.setLevel(getattr(logging, logging_level.upper()))
        handler.setFormatter(logging.Formatter(logformat))

        return handler
    # ___________________________________________

    @classmethod
    def init_app(cls, app, logging_name):
        pass
# ===================================


class DevelopmentConfig(Config):
    DEBUG = True
    DBNAME = "%sdev" % Config.PROJECT
    DB_CONN_URI = Config.DB_URI_FORMAT.format(
        dbname=DBNAME,
        **Config.DB_CONNECTION_PARAMS
    )

    # ________________________________

    @classmethod
    def init_app(cls, app, logging_name):
        pass
# ===================================


class TestingConfig(Config):
    APPTYPE = 'testing'
    TESTING = True
    SERVER_NAME = 'localhost'
    DBNAME = "%stest" % Config.PROJECT
    DB_CONN_URI = Config.DB_URI_FORMAT.format(
        dbname=DBNAME,
        **Config.DB_CONNECTION_PARAMS
    )

    @classmethod
    def init_app(cls, app, logging_name):
        pass

# ===================================


class ProductionConfig(Config):

    DBNAME = Config.PROJECT
    DB_CONN_URI = Config.DB_URI_FORMAT.format(
        dbname=DBNAME,
        **Config.DB_CONNECTION_PARAMS
    )
    # _____________________________

    @classmethod
    def init_app(cls, app):

        # email errors to the administrator
        from logging.handlers import SMTPHandler
        credentials = None
        secure = None
        if getattr(cls, 'MAIL_USERNAME', None) is not None:
            credentials = (cls.MAIL_USERNAME, cls.MAIL_PASSWORD)

        if getattr(cls, 'MAIL_USE_TLS', None):
            secure = ()

        mail_handler = SMTPHandler(
            mailhost=(cls.MAIL_SERVER, cls.MAIL_PORT),
            fromaddr=cls.IVT_MAIL_SENDER,
            toaddrs=[cls.IVT_ADMIN],
            subject='%s Application Error' % cls.IVT_MAIL_SUBJECT_PREFIX,
            credentials=credentials, secure=secure
        )

        mail_handler.setLevel(logging.ERROR)
        app.logger.addHandler(mail_handler)

# ===================================


config = {
    'develop': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig
}
# ___________________________________
