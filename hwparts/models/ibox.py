from config import logger
from . import db
from . import Base
# =============================


class Ibox(Base):

    def __init__(self):
        pass
    # ____________________________

    def fetch_releases(self):

        query = """
            SELECT
               release
            FROM ibox_sw_release
            ORDER BY release DESC
        """
        params = ()

        db.cursor.execute(query, params)
        fetch = db.cursor.fetchall()
        logger.debug("Releases fetch: %r", fetch)
        return [f['release'] for f in fetch]
    # ____________________________

    def fetch_node_model_by_release(self, release):

        query = """
            SELECT
               m.model
            FROM ibox_sw_release r, node_model m
            WHERE r.release = %s
            AND r.nodemodelid = m.id
        """
        params = (release,)

        db.cursor.execute(query, params)
        fetch = db.cursor.fetchone()
        logger.debug("Model fetch: %r", fetch)
        if fetch is None:
            return
        return fetch['model']
    # ____________________________
