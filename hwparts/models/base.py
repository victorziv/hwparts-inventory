from psycopg2.extensions import AsIs
from config import logger
from . import db
# =========================================


class Base:
    # ____________________________

    def clear_table(self):
        query = """
            DELETE FROM %s
        """
        params = (AsIs(self.table),)
        db.cursor.execute(query, params)
        db.conn.commit()
    # __________________________________

    def delete_by_id(self, rowid):
        query = """
            DELETE FROM %s
            WHERE id = %s
        """
        params = (AsIs(self.table), rowid)
        logger.debug("QUERY: {}".format(db.cursor.mogrify(query, params)))
        db.cursor.execute(query, params)
        db.conn.commit()
    # ____________________________

    def fetch_id_by_name(self, name):
        query = """
            SELECT id
            FROM %s
            WHERE name = %s
        """
        params = (AsIs(self.table), name)

        logger.debug("QUERY: {}".format(db.cursor.mogrify(query, params)))
        db.cursor.execute(query, params)
        fetch = db.cursor.fetchone()
        return fetch['id']
    # ____________________________

    def fetch_id_by_version(self, version):
        query = """
            SELECT id
            FROM %s
            WHERE version = %s
        """
        params = (AsIs(self.table), version)

        logger.debug("QUERY: {}".format(db.cursor.mogrify(query, params)))
        db.cursor.execute(query, params)
        fetch = db.cursor.fetchone()
        return fetch['id']
    # ____________________________

    def fetch_total(self, **kwargs):

        query = """
            SELECT COUNT(*) AS total FROM %s
        """
        params = (AsIs(self.table),)

        db.cursor.execute(query, params)
        fetch = db.cursor.fetchone()
        return fetch['total']
    # ____________________________

# ===========================
