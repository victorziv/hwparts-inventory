import psycopg2
from psycopg2.extensions import AsIs
from config import logger
from lib import utils
from . import db
from . import Base
# =============================


class Ware(Base):

    def __init__(self):
        self.table = 'ware'
    # ____________________________

    def create(self, name):

        query = """
            INSERT INTO %s
            (name, display_name)
            VALUES (%s, %s)
            RETURNING id
        """

        params = (
            AsIs(self.table),
            utils.normalize_name(name),
            utils.normalize_display_name(name)
        )

        logger.debug("Mogrify: {}".format(db.cursor.mogrify(query, params)))

        try:
            db.cursor.execute(query, params)
            db.conn.commit()
            fetch = db.cursor.fetchone()
            logger.debug("FETCH: {}".format(fetch))
            return fetch['id']
        except psycopg2.IntegrityError as e:
            logger.debug("!!!!ERROR: %s", e)
            db.conn.rollback()
            return self.fetch_id_by_name(utils.normalize_name(name))
        except Exception:
            db.conn.rollback()
            raise
    # ____________________________

    def fetch_names(self):

        query = """
            SELECT
               name,
               display_name
            FROM %s
            ORDER BY name
        """
        params = (AsIs(self.table),)

        db.cursor.execute(query, params)
        fetch = db.cursor.fetchall()
        return [dict(f) for f in fetch]
    # ____________________________
