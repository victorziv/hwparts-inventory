import psycopg2
from psycopg2.extensions import AsIs
from config import logger
from . import db, Ware, Contact
waremod = Ware()
contactmod = Contact()
# =============================


class Vendor:

    def __init__(self):
        self.table = 'vendor'
    # ____________________________

    def add(self, vendor):
        vendor_name = vendor.pop('vendor_name')
        vendor['display_name'] = vendor_name
        vendor['name'] = vendor_name.split()[0].strip().lower()
        wareid = self.add_vendor_ware(vendor)
#         contactid = self.add_vendor_contacts(vendor)
        logger.debug("VENDOR to ADD: %r", vendor)
        vendorid = self.create(vendor)
        self.map_vendor_ware(vendorid, wareid)
#         self.map_vendor_contact(vendorid, contactid)
    # ____________________________

    def add_vendor_contacts(self, vendor):
        """
        For now - a single contact is added.
        Multiple contacts will be able to be added in the future.

        """
        contact = {}
        contact['name'] = vendor.pop('contact_name')
        contact['email'] = vendor.pop('contact_email')
        contact['phone_desk'] = vendor.pop('contact_phone_desk')
        contact['phone_mobile'] = vendor.pop('contact_phone_mobile')
        contactid = contactmod.create(contact)
        return contactid
    # ____________________________

    def add_vendor_ware(self, vendor):
        ware = vendor.pop('ware_select')
        wareid = waremod.create(ware)
        logger.debug("WARE ID: %r", wareid)
        return wareid
    # ____________________________

    def create(self, attrs):

        query_template = """
            INSERT INTO %s
            ({})
            VALUES ({})
            RETURNING id
        """ % self.table

        fields = ', '.join(attrs.keys())
        logger.debug("Fields: {}".format(fields))
        values_placeholders = ', '.join(['%s' for v in attrs.values()])

        query = query_template.format(fields, values_placeholders)
        params = tuple(attrs.values())

        logger.debug("Mogrify: {}".format(db.cursor.mogrify(query, params)))

        try:
            db.cursor.execute(query, params)
            db.conn.commit()
            fetch = db.cursor.fetchone()
            logger.debug("FETCH: {}".format(fetch))
            return fetch['id']
        except psycopg2.IntegrityError as e:
            logger.debug("!!!!ERROR: %s", e)
            db.conn.rollback()
            return self.fetch_id_by_name(attrs['name'])
        except psycopg2.ProgrammingError:
            logger.exception("!ERROR")
            db.conn.rollback()
        except Exception:
            db.conn.rollback()
            raise
    # ____________________________

    def map_vendor_ware(self, vendorid, wareid):

        query = """
            INSERT INTO vendor_ware_map
            (vendorid, wareid)
            VALUES (%s, %s)
            RETURNING id
        """

        params = (vendorid, wareid)
        logger.debug("Mogrify: {}".format(db.cursor.mogrify(query, params)))

        try:
            db.cursor.execute(query, params)
            db.conn.commit()
            fetch = db.cursor.fetchone()
            logger.debug("FETCH: {}".format(fetch))
            return fetch['id']
        except psycopg2.IntegrityError as e:
            logger.debug("!!!!ERROR: %s", e)
            db.conn.rollback()
        except Exception:
            db.conn.rollback()
            raise
    # ____________________________

#     def map_vendor_contact(self, vendorid, contactid):

#         query = """
#             INSERT INTO vendor_contact_map
#             (vendorid, contactid)
#             VALUES (%s, %s)
#             RETURNING id
#         """

#         params = (vendorid, contactid)
#         logger.debug("Mogrify: {}".format(db.cursor.mogrify(query, params)))

#         try:
#             db.cursor.execute(query, params)
#             db.conn.commit()
#             fetch = db.cursor.fetchone()
#             logger.debug("FETCH: {}".format(fetch))
#             return fetch['id']
#         except psycopg2.IntegrityError as e:
#             logger.debug("!!!!ERROR: %s", e)
#             db.conn.rollback()
#         except Exception:
#             db.conn.rollback()
#             raise
    # ____________________________

    def fetch_by_id(self, vendorid):
        query = """
            SELECT id
            FROM %s
            WHERE id = %s
        """
        params = (AsIs(self.table), vendorid)

        logger.debug("QUERY: {}".format(db.cursor.mogrify(query, params)))
        db.cursor.execute(query, params)
        fetch = db.cursor.fetchone()
        return fetch['id']
    # ____________________________

    def fetch_by_name(self, name):
        query = """
            SELECT id
            FROM %s
            WHERE name = %s
        """
        params = (AsIs(self.table), name)

        logger.debug("QUERY: {}".format(db.cursor.mogrify(query, params)))
        db.cursor.execute(query, params)
        fetch = db.cursor.fetchone()
        return fetch['id']
    # ____________________________

    def fetch_id_by_name(self, name):
        query = """
            SELECT id
            FROM %s
            WHERE name = %s
        """
        params = (AsIs(self.table), name)

        logger.debug("QUERY: {}".format(db.cursor.mogrify(query, params)))
        db.cursor.execute(query, params)
        fetch = db.cursor.fetchone()
        return fetch['id']
    # ____________________________

    def fetch_total(self, **kwargs):

        query = """
            SELECT COUNT(*) AS total FROM %s
        """
        params = (AsIs(self.table),)

        db.cursor.execute(query, params)
        fetch = db.cursor.fetchone()
        return fetch['total']
    # ____________________________

    def fetch_names(self, ware):

        query = """
            SELECT
               v.name,
               v.display_name
            FROM
                vendor v
                INNER JOIN vendor_ware_map vwm ON vwm.vendorid = v.id
                INNER JOIN ware w ON w.id = vwm.wareid
            WHERE vwm.wareid = ( SELECT id FROM ware WHERE name = %s )
            ORDER BY v.name
        """
        params = (ware,)

        db.cursor.execute(query, params)
        fetch = db.cursor.fetchall()
        return [dict(f) for f in fetch]
    # ____________________________

    def fetch_page(self, params):

        sort_field = params.get('sort_field', 'name')
        sort_order = params.get('sort_order', 'asc')
        limit = params.get('limit', None)
        offset = params.get('offset', 0)

        query = """
            SELECT
               v.id,
               v.name,
               v.display_name,
               w.display_name AS ware,
               v.hq,
               v.notes
            FROM
                vendor v,
                ware w,
                vendor_ware_map vwm
            WHERE vwm.vendorid = v.id
            AND vwm.wareid = w.id
            ORDER BY v.%s %s
            LIMIT %s
            OFFSET %s
        """
        params = (AsIs(sort_field), AsIs(sort_order), AsIs(limit), AsIs(offset))

        db.cursor.execute(query, params)
        fetch = db.cursor.fetchall()
        logger.debug("Fetch page type: {}".format(type(fetch)))
        return fetch
    # ____________________________
