import psycopg2
from psycopg2.extensions import AsIs
from config import logger
from lib import utils
from . import db
from . import Base
# =============================


class Contact(Base):

    def __init__(self):
        self.table = 'contact'
    # ____________________________

    def create(self, contact):

        query = """
            INSERT INTO %s
            (name, email, phone_desk, phone_mobile)
            VALUES (%s, %s, %s, %s)
            RETURNING id
        """

        params = (
            AsIs(self.table),
            utils.normalize_display_name(contact['name']),
            contact['email'],
            contact['phone_desk'],
            contact['phone_mobile']
        )

        logger.debug("Mogrify: {}".format(db.cursor.mogrify(query, params)))

        try:
            db.cursor.execute(query, params)
            db.conn.commit()
            fetch = db.cursor.fetchone()
            logger.debug("FETCH: {}".format(fetch))
            return fetch['id']
        except psycopg2.IntegrityError as e:
            logger.debug("!!!!ERROR: %s", e)
            db.conn.rollback()
            return self.fetch_id_by_email(contact['email'])
        except Exception:
            db.conn.rollback()
            raise
    # ____________________________

    def fetch_id_by_email(self, email):
        query = """
            SELECT id
            FROM %s
            WHERE email = %s
        """
        params = (AsIs(self.table), email)

        logger.debug("QUERY: {}".format(db.cursor.mogrify(query, params)))
        db.cursor.execute(query, params)
        fetch = db.cursor.fetchone()
        return fetch['id']
    # ____________________________

    def fetch_names(self):

        query = """
            SELECT
               name,
               display_name
            FROM %s
            ORDER BY name
        """
        params = (AsIs(self.table),)

        db.cursor.execute(query, params)
        fetch = db.cursor.fetchall()
        return [dict(f) for f in fetch]
    # ____________________________
