from psycopg2 import IntegrityError
from psycopg2.extensions import AsIs
import hashlib
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import url_for, request
from flask_login import UserMixin
from flask import current_app as cup
from config import logger
from . import db, login_manager
from .role import Role
from . import Base
from config import Permission
rolemod = Role()
# ===========================


class User(UserMixin, Base):

    """
    __tablename__ = 'users'

    """

    # __________________________________

    def __init__(self, social_id=None, username=None, email=None):
        self.table = 'users'
        self.social_id = social_id
        self.username = username
        self.email = email
    # __________________________________

    def fetch_by_social_id(self, social_id):
        return self.fetch_by_field(field_name='social_id', field_value=social_id)
    # __________________________________

    def fetch_by_field(self, field_name, field_value):
        query = """
            SELECT
                u.id,
                u.social_id,
                u.email,
                u.username,
                u.name,
                u.given_name,
                u.family_name,
                u.picture
            FROM users AS u
            WHERE %s = %s
        """

        params = (AsIs(field_name), field_value)

        db.cursor.execute(query, params)
        fetch = db.cursor.fetchone()
        if fetch is None:
            return {}

        return fetch
    # __________________________________

    def get_by_social_id(self, social_id):
        userd = self.fetch_by_social_id(social_id)
        if not userd:
            return None
        self.__dict__.update(userd)
        return self
    # __________________________________

    def get_all(self):
        """
        Fetches all existent users from the DB.

        :Returns:
            A list of User() objects - an object per fetched user.

        """
        user_dicts = self.fetch_users()
        return [
            self.set_user_attributes(user_dict)
            for user_dict in user_dicts
        ]
    # ____________________________

    def gravatar(self, size=100, default='identicon', rating='g'):

        if request.is_secure:
            url = 'https://secure.gravatar.com/avatar'
        else:
            url = 'http://www.gravatar.com/avatar'

        logger.debug("Avatar hash: {}".format(self.avatar_hash))
        if self.avatar_hash is None:
            self.avatar_hash = hashlib.md5(self.email.encode('utf-8')).hexdigest()
            User.update_user(params={'email': self.email, 'avatar_hash': self.avatar_hash})

        return '{url}/{checksum}?s={size}&d={default}&r={rating}'.format(
            url=url, checksum=self.avatar_hash, size=size, default=default, rating=rating)
    # ____________________________

    def to_json(self):

        json_user = {
            'url': url_for('api.get_user', id=self.id, _external=True),
            'username': self.username,
            'email': self.email,
            'role': self.role
        }

        return json_user

    # __________________________________

    def can(self, permissions):
        """
        Figures out whether the current role can do anything
        given a set of permissions to check against.

        :Returns:
            bool : True or False

        """
        logger.info("User {} role: {}".format(self.email, self.role))
        return self.role is not None and (
            self.permissions & permissions) == permissions
    # __________________________________

    def is_administrator(self):
        return self.can(Permission.ADMINISTER)
    # __________________________________

    def generate_auth_token(self, expiration):
        s = Serializer(cup.config['SECRET_KEY'], expires_in=expiration)
        return s.dumps({'id': self.id})
    # __________________________________

    def create(self, profile):

        query = """
            INSERT INTO users
            (
                social_id,
                username,
                email,
                name,
                given_name,
                family_name,
                picture
            )
            VALUES (
                %(social_id)s,
                %(username)s,
                %(email)s,
                %(name)s,
                %(given_name)s,
                %(family_name)s,
                %(picture)s
            )
            RETURNING id;
        """

        logger.debug("Query: {}".format(db.cursor.mogrify(query, profile)))

        try:
            db.cursor.execute(query, profile)
            db.conn.commit()
            fetch = db.cursor.fetchone()
            return fetch['id']
        except IntegrityError:
            db.conn.rollback()
            return self.fetch_by_social_id(profile['social_id'])
        except Exception:
            raise
    # ____________________________

    def save(self, profile, role='user'):

        # Set user role
        if role.lower() == 'admin':
            # user is an administrator
            role = rolemod.fetch_by_name('admin')
        else:
            role = rolemod.fetch_by_name(role.lower())

        logger.debug(("Role: {}".format(role)))

        self.id = self.create(profile)

        logger.info("New user ID: %r", self.id)
        if self.id:
            self.__dict__.update(profile)

        return self
    # ____________________________

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(cup.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return None

        return User.get_by_field(name='id', value=data['id'])

# ===========================


@login_manager.user_loader
def load_user(user_id):
    u = User()
    userd = u.fetch_by_field(field_name='id', field_value=user_id)
    u.__dict__.update(userd)
    return u
