import psycopg2
from psycopg2.extensions import AsIs
from config import logger
from models import db, Base
# =============================


class Rearbp(Base):

    def __init__(self):
        self.table = 'node_rearbp'
    # ____________________________

    def add(self, rearbp):
        try:
            query = """
                INSERT INTO %s
                (
                    releaseid,
                    pn,
                    mixpn,
                    revision,
                    version,
                    firmware_bin,
                    firmware_exe,
                    hwstatus_check,
                    notes
                )
                VALUES (
                    (SELECT id FROM ibox_sw_release WHERE release = %s),
                    %s, %s, %s, %s, %s, %s, %s, %s
                )
                RETURNING id
            """

            params = (
                AsIs(self.table),
                rearbp['ibox_release'],
                rearbp['rearbp_pn'],
                rearbp['rearbp_mixpn'],
                rearbp['rearbp_revision'],
                rearbp['rearbp_version'],
                rearbp['firmware_bin'],
                rearbp['firmware_exe'],
                rearbp['hwstatus_check'],
                rearbp['notes'])
            db.cursor.execute(query, params)
            db.conn.commit()
            fetch = db.cursor.fetchone()
            logger.debug("FETCH: {}".format(fetch))
            return fetch['id']
        except psycopg2.IntegrityError as e:
            logger.debug("!!!!ERROR: %s", e)
            db.conn.rollback()
            return self.fetch_id_by_version(rearbp['rearbp_version'])
        except Exception:
            db.conn.rollback()
            raise
    # ____________________________

    def fetch_by_id(self, rearbpid):
        query = """
            SELECT
               rb.id,
               rb.pn,
               rb.revision,
               rb.mixpn,
               ib.release,
               rb.version,
               nm.model as node_model,
               rb.firmware_bin,
               rb.firmware_exe,
               rb.hwstatus_check,
               rb.notes
            FROM
                node_rearbp rb,
                node_model nm,
                ibox_sw_release ib
            WHERE rb.releaseid = ib.id
            AND ib.nodemodelid = nm.id
            AND rb.id = %s

        """
        params = (rearbpid,)

        db.cursor.execute(query, params)
        fetch = db.cursor.fetchone()
        if not fetch:
            return {}
        return dict(fetch)

    # ____________________________

    def fetch_page(self, params):

        sort_field = params['sort_field']
        sort_order = params['sort_order']
        limit = params.get('limit', None)
        offset = params.get('offset', 0)

        query = """
            SELECT
               rb.id,
               rb.pn,
               rb.revision,
               rb.mixpn,
               ib.release,
               rb.version,
               nm.model as node_model,
               rb.firmware_bin,
               rb.firmware_exe,
               rb.hwstatus_check,
               rb.notes
            FROM
                node_rearbp rb,
                node_model nm,
                ibox_sw_release ib
            WHERE rb.releaseid = ib.id
            AND ib.nodemodelid = nm.id

            ORDER BY %s %s
            LIMIT %s
            OFFSET %s
        """
        params = (AsIs(sort_field), AsIs(sort_order), AsIs(limit), AsIs(offset))

        db.cursor.execute(query, params)
        fetch = db.cursor.fetchall()
        logger.debug("Fetch page type: {}".format(type(fetch)))
        return fetch
    # ____________________________

    def update(self, rearbpid, rearbp):

        try:
            query = """
                UPDATE %s
                SET
                    pn = %s,
                    mixpn = %s,
                    version = %s,
                    revision = %s,
                    firmware_bin = %s,
                    firmware_exe = %s,
                    hwstatus_check = %s,
                    notes = %s
                WHERE id = %s
            """
            params = (
                AsIs(self.table),
                rearbp['rearbp_pn'],
                rearbp['rearbp_mixpn'],
                rearbp['rearbp_version'],
                rearbp['rearbp_revision'],
                rearbp['firmware_bin'],
                rearbp['firmware_exe'],
                rearbp['hwstatus_check'],
                rearbp['notes'],
                rearbpid
            )

            db.cursor.execute(query, params)
            db.conn.commit()
        except Exception:
            db.conn.rollback()
            raise
    # ____________________________
