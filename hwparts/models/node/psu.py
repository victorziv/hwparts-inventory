import psycopg2
from psycopg2.extensions import AsIs
from config import logger
from models import db, Base
# =============================


class PSU(Base):

    def __init__(self):
        self.table = 'node_psu'
    # ____________________________

    def add(self, psu):
        try:
            query = """
                INSERT INTO %s
                (
                    releaseid,
                    power,
                    pn,
                    version,
                    firmware_bin,
                    firmware_exe,
                    hwstatus_check,
                    notes
                )
                VALUES (
                    (SELECT id FROM ibox_sw_release WHERE release = %s),
                    %s, %s, %s, %s, %s, %s, %s
                )
                RETURNING id
            """

            params = (
                AsIs(self.table),
                psu['ibox_release'],
                psu['psu_power'],
                psu['psu_pn'],
                psu['psu_version'],
                psu['firmware_bin'],
                psu['firmware_exe'],
                psu['hwstatus_check'],
                psu['notes'])
            db.cursor.execute(query, params)
            db.conn.commit()
            fetch = db.cursor.fetchone()
            logger.debug("FETCH: {}".format(fetch))
            return fetch['id']
        except psycopg2.IntegrityError as e:
            logger.debug("!!!!ERROR: %s", e)
            db.conn.rollback()
            return self.fetch_id_by_version(psu['psu_version'])
        except Exception:
            db.conn.rollback()
            raise
    # ____________________________

    def fetch_id_by_version(self, version):
        query = """
            SELECT id
            FROM %s
            WHERE version = %s
        """
        params = (AsIs(self.table), version)

        logger.debug("QUERY: {}".format(db.cursor.mogrify(query, params)))
        db.cursor.execute(query, params)
        fetch = db.cursor.fetchone()
        return fetch['id']
    # ____________________________

    def fetch_by_id(self, psuid):

        query = """
            SELECT
               p.id,
               p.power,
               p.pn,
               ib.release,
               p.version,
               nm.model as node_model,
               p.firmware_bin,
               p.firmware_exe,
               p.hwstatus_check,
               p.notes
            FROM
                node_psu p,
                node_model nm,
                ibox_sw_release ib
            WHERE p.releaseid = ib.id
            AND ib.nodemodelid = nm.id
            AND p.id = %s

        """
        params = (psuid,)

        db.cursor.execute(query, params)
        fetch = db.cursor.fetchone()
        if not fetch:
            return {}
        return dict(fetch)
    # ____________________________

    def fetch_page(self, params):

        sort_field = params['sort_field']
        sort_order = params['sort_order']
        limit = params.get('limit', None)
        offset = params.get('offset', 0)

        query = """
            SELECT
               p.id,
               p.power,
               p.pn,
               ib.release,
               p.version,
               nm.model as node_model,
               p.firmware_bin,
               p.firmware_exe,
               p.hwstatus_check,
               p.notes
            FROM
                node_psu p,
                node_model nm,
                ibox_sw_release ib
            WHERE p.releaseid = ib.id
            AND ib.nodemodelid = nm.id

            ORDER BY %s %s
            LIMIT %s
            OFFSET %s
        """
        params = (AsIs(sort_field), AsIs(sort_order), AsIs(limit), AsIs(offset))

        db.cursor.execute(query, params)
        fetch = db.cursor.fetchall()
        logger.debug("Fetch page type: {}".format(type(fetch)))
        return fetch
    # ____________________________

    def update(self, psuid, psu):

        try:
            query = """
                UPDATE %s
                SET
                   power = %s,
                   pn = %s,
                   version = %s,
                   firmware_bin = %s,
                   firmware_exe = %s,
                   hwstatus_check = %s,
                   notes = %s
                WHERE id = %s
            """
            params = (
                AsIs(self.table),
                psu['psu_power'],
                psu['psu_pn'],
                psu['psu_version'],
                psu['firmware_bin'],
                psu['firmware_exe'],
                psu['hwstatus_check'],
                psu['notes'],
                psuid
            )

            db.cursor.execute(query, params)
            db.conn.commit()
        except Exception:
            db.conn.rollback()
            raise
    # ____________________________
