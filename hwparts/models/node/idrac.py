import psycopg2
from psycopg2.extensions import AsIs
from config import logger
from models import db, Base
# =============================


class Idrac(Base):

    def __init__(self):
        self.table = 'node_idrac'
    # ____________________________

    def add(self, idrac):
        try:
            query = """
                INSERT INTO %s
                (
                    releaseid,
                    version,
                    build,
                    firmware_bin,
                    firmware_exe,
                    hwstatus_check,
                    notes
                )
                VALUES (
                    (SELECT id FROM ibox_sw_release WHERE release = %s),
                    %s, %s, %s, %s, %s, %s
                )
                RETURNING id
            """

            params = (
                AsIs(self.table),
                idrac['ibox_release'],
                idrac['idrac_version'],
                idrac['idrac_build'],
                idrac['firmware_bin'],
                idrac['firmware_exe'],
                idrac['hwstatus_check'],
                idrac['notes'])
            db.cursor.execute(query, params)
            db.conn.commit()
            fetch = db.cursor.fetchone()
            logger.debug("FETCH: {}".format(fetch))
            return fetch['id']
        except psycopg2.IntegrityError as e:
            logger.debug("!!!!ERROR: %s", e)
            db.conn.rollback()
            return self.fetch_id_by_version(idrac['idrac_version'])
        except Exception:
            db.conn.rollback()
            raise
    # ____________________________

    def fetch_by_id(self, idracid):
        query = """
            SELECT
                idr.id,
                ib.release,
                nm.model as node_model,
                idr.version,
                idr.build,
                idr.firmware_bin,
                idr.firmware_exe,
                idr.hwstatus_check,
                idr.notes
            FROM node_idrac idr, ibox_sw_release ib, node_model nm
            WHERE idr.id = %s
            AND idr.releaseid = ib.id
            AND ib.nodemodelid = nm.id
        """
        params = (idracid,)

        logger.debug("QUERY: {}".format(db.cursor.mogrify(query, params)))
        db.cursor.execute(query, params)
        fetch = db.cursor.fetchone()
        if not fetch:
            return {}
        return dict(fetch)
    # ____________________________

    def fetch_page(self, params):

        sort_field = params.get('sort_field', 'release')
        sort_order = params.get('sort_order', 'desc')
        limit = params.get('limit', None)
        offset = params.get('offset', 0)

        query = """
            SELECT
               i.id,
               ib.release,
               i.version,
               i.build,
               nm.model as node_model,
               i.firmware_bin,
               i.firmware_exe,
               i.hwstatus_check,
               i.notes
            FROM
                node_idrac i,
                node_model nm,
                ibox_sw_release ib
            WHERE i.releaseid = ib.id
            AND ib.nodemodelid = nm.id

            ORDER BY %s %s
            LIMIT %s
            OFFSET %s
        """
        params = (AsIs(sort_field), AsIs(sort_order), AsIs(limit), AsIs(offset))

        db.cursor.execute(query, params)
        fetch = db.cursor.fetchall()
        logger.debug("Fetch page type: {}".format(type(fetch)))
        return fetch
    # ____________________________

    def update(self, idracid, idrac):

        try:
            query = """
                UPDATE %s
                SET
                    version = %s,
                    build = %s,
                    firmware_bin = %s,
                    firmware_exe = %s,
                    hwstatus_check = %s,
                    notes = %s
                WHERE id = %s
            """

            params = (
                AsIs(self.table),
                idrac['idrac_version'],
                idrac['idrac_build'],
                idrac['firmware_bin'],
                idrac['firmware_exe'],
                idrac['hwstatus_check'],
                idrac['notes'],
                idracid
            )

            db.cursor.execute(query, params)
            db.conn.commit()
        except Exception:
            db.conn.rollback()
            raise
    # ____________________________
