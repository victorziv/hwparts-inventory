import psycopg2
from psycopg2.extensions import AsIs
from config import logger
from models import db, Base
# =============================


class Bios(Base):

    def __init__(self):
        self.table = 'node_bios'
    # ____________________________

    def add(self, bios):
        try:
            query = """
                INSERT INTO %s
                (
                    releaseid,
                    version,
                    firmware_bin,
                    firmware_exe,
                    hwstatus_check,
                    cpu_microarch,
                    notes
                )
                VALUES (
                    (SELECT id FROM ibox_sw_release WHERE release = %s),
                    %s, %s, %s, %s, %s, %s
                )
                RETURNING id
            """

            params = (
                AsIs(self.table),
                bios['ibox_release'],
                bios['bios_version'],
                bios['firmware_bin'],
                bios['firmware_exe'],
                bios['hwstatus_check'],
                bios['cpu_microarch'],
                bios['notes'])
            db.cursor.execute(query, params)
            db.conn.commit()
            fetch = db.cursor.fetchone()
            logger.debug("FETCH: {}".format(fetch))
            return fetch['id']
        except psycopg2.IntegrityError as e:
            logger.debug("!!!!ERROR: %s", e)
            db.conn.rollback()
            return self.fetch_id_by_version(bios['bios_version'])
        except Exception:
            db.conn.rollback()
            raise
    # ____________________________

    def fetch_by_id(self, rowid):
        query = """
            SELECT
                nb.id,
                ib.release,
                nm.model as node_model,
                nb.version,
                nb.cpu_microarch,
                nb.firmware_bin,
                nb.firmware_exe,
                nb.hwstatus_check,
                nb.notes
            FROM node_bios nb, ibox_sw_release ib, node_model nm
            WHERE nb.id = %s
            AND nb.releaseid = ib.id
            AND ib.nodemodelid = nm.id
        """
        params = (rowid,)

        logger.debug("QUERY: {}".format(db.cursor.mogrify(query, params)))
        db.cursor.execute(query, params)
        fetch = db.cursor.fetchone()
        if not fetch:
            return {}
        return dict(fetch)
    # ____________________________

    def fetch_page(self, params):

        sort_field = params.get('sort_field', 'release')
        sort_order = params.get('sort_order', 'asc')
        limit = params.get('limit', None)
        offset = params.get('offset', 0)

        query = """
            SELECT
               b.id,
               ib.release,
               b.version,
               nm.model as node_model,
               b.cpu_microarch,
               b.firmware_bin,
               b.firmware_exe,
               b.hwstatus_check,
               b.notes
            FROM
                node_bios b,
                node_model nm,
                ibox_sw_release ib
            WHERE b.releaseid = ib.id
            AND ib.nodemodelid = nm.id

            ORDER BY %s %s
            LIMIT %s
            OFFSET %s
        """
        params = (AsIs(sort_field), AsIs(sort_order), AsIs(limit), AsIs(offset))

        db.cursor.execute(query, params)
        fetch = db.cursor.fetchall()
        logger.debug("Fetch page type: {}".format(type(fetch)))
        return fetch
    # ____________________________

    def update(self, biosid, bios):

        try:
            query = """
                UPDATE %s
                SET
                    version = %s,
                    cpu_microarch = %s,
                    firmware_bin = %s,
                    firmware_exe = %s,
                    hwstatus_check = %s,
                    notes = %s
                WHERE id = %s
            """

            params = (
                AsIs(self.table),
                bios['bios_version'],
                bios['cpu_microarch'],
                bios['firmware_bin'],
                bios['firmware_exe'],
                bios['hwstatus_check'],
                bios['notes'],
                biosid
            )

            db.cursor.execute(query, params)
            db.conn.commit()
        except Exception:
            db.conn.rollback()
            raise
    # ____________________________
