import psycopg2
from psycopg2.extensions import AsIs
from config import logger
from models import db, Base
# =============================


class LifecycleController(Base):

    def __init__(self):
        self.table = 'node_lifecycle_controller'
    # ____________________________

    def add(self, lifecycle_controller):
        try:
            query = """
                INSERT INTO %s
                (
                    releaseid,
                    version,
                    firmware_bin,
                    firmware_exe,
                    hwstatus_check,
                    notes
                )
                VALUES (
                    (SELECT id FROM ibox_sw_release WHERE release = %s),
                    %s, %s, %s, %s, %s
                )
                RETURNING id
            """

            params = (
                AsIs(self.table),
                lifecycle_controller['ibox_release'],
                lifecycle_controller['lifecont_version'],
                lifecycle_controller['firmware_bin'],
                lifecycle_controller['firmware_exe'],
                lifecycle_controller['hwstatus_check'],
                lifecycle_controller['notes'])
            db.cursor.execute(query, params)
            db.conn.commit()
            fetch = db.cursor.fetchone()
            logger.debug("FETCH: {}".format(fetch))
            return fetch['id']
        except psycopg2.IntegrityError as e:
            logger.debug("!!!!ERROR: %s", e)
            db.conn.rollback()
            return self.fetch_id_by_release(lifecycle_controller['lifecycle_controller_version'])
        except Exception:
            logger.exception("!!!!ERROR")
            db.conn.rollback()
            raise
    # ____________________________

    def fetch_id_by_release(self, release):
        query = """
            SELECT id
            FROM %s
            WHERE releasid = ( SELECT id FROM ibox_sw_release WHERE release = %s)
        """
        params = (AsIs(self.table), release)

        logger.debug("QUERY: {}".format(db.cursor.mogrify(query, params)))
        db.cursor.execute(query, params)
        fetch = db.cursor.fetchone()
        return fetch['id']
    # ____________________________

    def fetch_by_id(self, rowid):
        query = """
            SELECT
                lc.id,
                ib.release,
                nm.model as node_model,
                lc.version,
                lc.firmware_bin,
                lc.firmware_exe,
                lc.hwstatus_check,
                lc.notes
            FROM node_lifecycle_controller lc, ibox_sw_release ib, node_model nm
            WHERE lc.id = %s
            AND lc.releaseid = ib.id
            AND ib.nodemodelid = nm.id
        """
        params = (rowid,)

        logger.debug("QUERY: {}".format(db.cursor.mogrify(query, params)))
        db.cursor.execute(query, params)
        fetch = db.cursor.fetchone()
        if not fetch:
            return {}
        return dict(fetch)
    # ____________________________

    def fetch_page(self, params):

        sort_field = params.get('sort_field', 'release')
        sort_order = params.get('sort_order', 'desc')
        limit = params.get('limit', None)
        offset = params.get('offset', 0)

        query = """
            SELECT
               lc.id,
               ib.release,
               lc.version,
               nm.model as node_model,
               lc.firmware_bin,
               lc.firmware_exe,
               lc.hwstatus_check,
               lc.notes
            FROM
                node_lifecycle_controller lc,
                node_model nm,
                ibox_sw_release ib
            WHERE lc.releaseid = ib.id
            AND ib.nodemodelid = nm.id

            ORDER BY %s %s
            LIMIT %s
            OFFSET %s
        """
        params = (AsIs(sort_field), AsIs(sort_order), AsIs(limit), AsIs(offset))

        db.cursor.execute(query, params)
        fetch = db.cursor.fetchall()
        logger.debug("Fetch page type: {}".format(type(fetch)))
        return fetch
    # ____________________________

    def update(self, lifecontid, lifecont):

        try:
            query = """
                UPDATE %s
                SET
                    version = %s,
                    firmware_bin = %s,
                    firmware_exe = %s,
                    hwstatus_check = %s,
                    notes = %s
                WHERE id = %s
            """

            params = (
                AsIs(self.table),
                lifecont['lifecont_version'],
                lifecont['firmware_bin'],
                lifecont['firmware_exe'],
                lifecont['hwstatus_check'],
                lifecont['notes'],
                lifecontid
            )

            db.cursor.execute(query, params)
            db.conn.commit()
        except Exception:
            db.conn.rollback()
            raise
    # ____________________________
