import psycopg2
from psycopg2.extensions import AsIs
from config import logger
from models import db, Base
# =============================


class Node(Base):

    def __init__(self):
        self.table = 'node_model'
    # ____________________________

    def add(self, node):
        try:
            query = """
                INSERT INTO %s
                ( model, vendorid, notes )
                VALUES (
                    %s,
                    (SELECT id FROM vendor WHERE name = %s),
                    %s
                )
                RETURNING id
            """

            params = (AsIs(self.table), node['node_model'], node['vendor_select'], node['notes'])
            db.cursor.execute(query, params)
            db.conn.commit()
            fetch = db.cursor.fetchone()
            logger.debug("FETCH: {}".format(fetch))
            return fetch['id']
        except psycopg2.IntegrityError as e:
            logger.debug("!!!!ERROR: %s", e)
            db.conn.rollback()
            return self.fetch_id_by_model(node['model'])
        except Exception:
            db.conn.rollback()
            raise
    # ____________________________

    def fetch_by_id(self, modelid):
        query = """
            SELECT
               n.id,
               n.model,
               v.display_name as vendor,
               n.notes
            FROM node_model n, vendor v
            WHERE n.id = %s
            AND n.vendorid = v.id
        """
        params = (modelid, )

        logger.debug("QUERY: {}".format(db.cursor.mogrify(query, params)))
        db.cursor.execute(query, params)
        fetch = db.cursor.fetchone()
        if not fetch:
            return {}
        return dict(fetch)
    # ____________________________

    def fetch_id_by_model(self, model):
        query = """
            SELECT id
            FROM %s
            WHERE model = %s
        """
        params = (AsIs(self.table), model)

        logger.debug("QUERY: {}".format(db.cursor.mogrify(query, params)))
        db.cursor.execute(query, params)
        fetch = db.cursor.fetchone()
        return fetch['id']
    # ____________________________

    def fetch_page(self, params):

        sort_field = params.get('sort_field', 'model')
        sort_order = params.get('sort_order', 'asc')
        limit = params.get('limit', None)
        offset = params.get('offset', 0)

        query = """
            SELECT
               n.id,
               n.model,
               v.display_name as vendor,
               n.notes
            FROM node_model n, vendor v
            WHERE n.vendorid = v.id
            ORDER BY n.%s %s
            LIMIT %s
            OFFSET %s
        """
        params = (AsIs(sort_field), AsIs(sort_order), AsIs(limit), AsIs(offset))

        db.cursor.execute(query, params)
        fetch = db.cursor.fetchall()
        logger.debug("Fetch page type: {}".format(type(fetch)))
        return fetch
    # ____________________________

    def update(self, modelid, model):

        try:
            query = """
                UPDATE %s
                SET
                    model = %s,
                    notes = %s
                WHERE id = %s
            """

            params = (
                AsIs(self.table),
                model['node_model'],
                model['notes'],
                modelid
            )

            db.cursor.execute(query, params)
            db.conn.commit()
        except Exception:
            db.conn.rollback()
            raise
    # ____________________________
