import psycopg2
from psycopg2.extensions import AsIs
from config import logger
from models import db, Base
# =============================


class Perc(Base):

    def __init__(self):
        self.table = 'node_perc'
    # ____________________________

    def add(self, perc):
        percid = self.insert_perc(perc)
        revisions = perc['supported_revisions']
        if revisions is None:
            revisions = []
        self.insert_perc_revisions(revisions)
        self.insert_perc_revision_map(percid, revisions)
    # ____________________________

    def delete_from_revision_map(self, percid):
        query = """
            DELETE FROM node_perc_revision_map
            WHERE percid = %s
        """
        params = (percid,)
        logger.debug("QUERY: {}".format(db.cursor.mogrify(query, params)))
        db.cursor.execute(query, params)
        db.conn.commit()

    # ____________________________

    def delete_by_id(self, percid):
        self.delete_from_revision_map(percid)
        super().delete_by_id(percid)
    # ____________________________

    def insert_perc(self, perc):
        try:
            query = """
                INSERT INTO %s
                (
                    releaseid,
                    model,
                    version,
                    revision_mix_allowed,
                    firmware_bin,
                    firmware_exe,
                    hwstatus_check,
                    notes
                )
                VALUES (
                    (SELECT id FROM ibox_sw_release WHERE release = %s),
                    %s, %s, %s, %s, %s, %s, %s
                )
                RETURNING id
            """

            params = (
                AsIs(self.table),
                perc['ibox_release'],
                perc['perc_model'],
                perc['perc_version'],
                perc['revision_mix_allowed'],
                perc['firmware_bin'],
                perc['firmware_exe'],
                perc['hwstatus_check'],
                perc['notes'])
            db.cursor.execute(query, params)
            db.conn.commit()
            fetch = db.cursor.fetchone()
            logger.debug("FETCH: {}".format(fetch))
            return fetch['id']
        except psycopg2.IntegrityError as e:
            logger.debug("!!!!ERROR: %s", e)
            db.conn.rollback()
            return self.fetch_id_by_version(perc['perc_version'])
        except Exception:
            db.conn.rollback()
            raise
    # ____________________________

    def insert_perc_revisions(self, revisions):
            query = """
                INSERT INTO node_perc_revision
                (official_revision)
                VALUES
                (%s)
            """

            for revision in revisions:
                try:
                    params = (revision,)
                    db.cursor.execute(query, params)
                    db.conn.commit()
                except psycopg2.IntegrityError as e:
                    logger.debug("!!!!ERROR: %s", e)
                    db.conn.rollback()
                except Exception:
                    db.conn.rollback()
                    raise
    # ____________________________

    def insert_perc_revision_map(self, percid, revisions):
        query = """
            INSERT INTO node_perc_revision_map
            (percid, revisionid)
            VALUES
            (
                %s,
                (SELECT id FROM node_perc_revision WHERE official_revision = %s)
            )
        """

        for revision in revisions:
            try:
                params = (percid, revision)
                db.cursor.execute(query, params)
                db.conn.commit()
            except psycopg2.IntegrityError as e:
                logger.debug("!!!!ERROR: %s", e)
                db.conn.rollback()
            except Exception:
                db.conn.rollback()
                raise
    # ____________________________

    def fetch_by_id(self, percid):

        query = """
            SELECT
               p.id,
               ib.release,
               p.version,
               p.revision_mix_allowed,
               nm.model as node_model,
               p.model,
               p.firmware_bin,
               p.firmware_exe,
               p.hwstatus_check,
               p.notes
            FROM
                node_perc p,
                node_model nm,
                ibox_sw_release ib
            WHERE p.releaseid = ib.id
            AND ib.nodemodelid = nm.id
            AND p.id = %s

        """
        params = (percid,)

        db.cursor.execute(query, params)
        fetch = db.cursor.fetchone()
        if not fetch:
            return {}
        return dict(fetch)
    # ____________________________

    def fetch_page(self, params):
        percs = self.fetch_percs(params)
        for perc in percs:
            perc['supported_revisions'] = ' '.join([r['text'] for r in self.fetch_supported_revisions(perc['id'])])
            logger.debug("Supported revisions for percid %s: %r", perc['id'], perc['supported_revisions'])

        return percs
    # ____________________________

    def fetch_percs(self, params):

        sort_field = params.get('sort_field', 'release')
        sort_order = params.get('sort_order', 'desc')
        limit = params.get('limit', None)
        offset = params.get('offset', 0)

        query = """
            SELECT
               p.id,
               ib.release,
               p.version,
               nm.model as node_model,
               p.model,
               p.firmware_bin,
               p.firmware_exe,
               p.revision_mix_allowed,
               p.hwstatus_check,
               p.notes
            FROM
                node_perc p,
                node_model nm,
                ibox_sw_release ib
            WHERE p.releaseid = ib.id
            AND ib.nodemodelid = nm.id

            ORDER BY %s %s
            LIMIT %s
            OFFSET %s
        """
        params = (AsIs(sort_field), AsIs(sort_order), AsIs(limit), AsIs(offset))

        db.cursor.execute(query, params)
        fetch = db.cursor.fetchall()
        logger.debug("Fetch page type: {}".format(type(fetch)))
        return [dict(p) for p in fetch]
    # ____________________________

    def fetch_supported_revisions(self, percid):

        query = """
            SELECT
                pr.id,
                pr.official_revision AS text
            FROM
                node_perc p,
                node_perc_revision pr,
                node_perc_revision_map prm
            WHERE p.id = %s
            AND prm.percid = p.id
            AND prm.revisionid = pr.id
            ORDER BY text
        """
        params = (percid,)

        db.cursor.execute(query, params)
        fetch = db.cursor.fetchall()
        return [dict(f) for f in fetch]
    # ____________________________

    def fetch_revisions(self):

        query = """
            SELECT
               id,
               official_revision AS text
            FROM
                node_perc_revision
            ORDER BY text
        """
        params = ()

        db.cursor.execute(query, params)
        fetch = db.cursor.fetchall()
        return [dict(r) for r in fetch]
    # ____________________________

    def reset_perc_model_revision_map(self, percid):
        query = """
            DELETE FROM node_perc_revision_map
            WHERE percid = %s
        """

        params = (percid, )
        db.cursor.execute(query, params)
        db.conn.commit()
    # ____________________________

    def update(self, percid, perc):
        self.update_perc(percid, perc)
        revisions = perc['supported_revisions']
        logger.debug("Revisions: %r", revisions)
        if len(revisions):
            self.insert_perc_revisions(revisions)
            self.update_revision_map(percid, revisions)

    # ____________________________

    def update_perc(self, percid, perc):

        try:
            query = """
                UPDATE %s
                SET
                    version = %s,
                    model = %s,
                    revision_mix_allowed = %s,
                    firmware_bin = %s,
                    firmware_exe = %s,
                    hwstatus_check = %s,
                    notes = %s
                WHERE id = %s
            """

            params = (
                AsIs(self.table),
                perc['perc_version'],
                perc['perc_model'],
                perc['revision_mix_allowed'],
                perc['firmware_bin'],
                perc['firmware_exe'],
                perc['hwstatus_check'],
                perc['notes'],
                percid
            )

            db.cursor.execute(query, params)
            db.conn.commit()
        except Exception:
            db.conn.rollback()
            raise
    # ____________________________

    def update_revision_map(self, percid, revisions):
        self.reset_perc_model_revision_map(percid)
        self.insert_perc_revision_map(percid, revisions)
