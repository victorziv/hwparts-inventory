import psycopg2
from psycopg2.extensions import AsIs
from config import logger
from models import db, Base
# =============================


class Frontbp(Base):

    def __init__(self):
        self.table = 'node_frontbp'
    # ____________________________

    def add(self, frontbp):
        try:
            query = """
                INSERT INTO %s
                (
                    releaseid,
                    pn,
                    mixpn,
                    revision,
                    version,
                    firmware_bin,
                    firmware_exe,
                    hwstatus_check,
                    notes
                )
                VALUES (
                    (SELECT id FROM ibox_sw_release WHERE release = %s),
                    %s, %s, %s, %s, %s, %s, %s, %s
                )
                RETURNING id
            """

            params = (
                AsIs(self.table),
                frontbp['ibox_release'],
                frontbp['frontbp_pn'],
                frontbp['frontbp_mixpn'],
                frontbp['frontbp_revision'],
                frontbp['frontbp_version'],
                frontbp['firmware_bin'],
                frontbp['firmware_exe'],
                frontbp['hwstatus_check'],
                frontbp['notes'])
            db.cursor.execute(query, params)
            db.conn.commit()
            fetch = db.cursor.fetchone()
            logger.debug("FETCH: {}".format(fetch))
            return fetch['id']
        except psycopg2.IntegrityError as e:
            logger.debug("!!!!ERROR: %s", e)
            db.conn.rollback()
            return self.fetch_id_by_version(frontbp['frontbp_version'])
        except Exception:
            db.conn.rollback()
            raise
    # ____________________________

    def fetch_by_id(self, frontbpid):
        query = """
            SELECT
               fb.id,
               fb.pn,
               fb.revision,
               fb.mixpn,
               ib.release,
               fb.version,
               nm.model as node_model,
               fb.firmware_bin,
               fb.firmware_exe,
               fb.hwstatus_check,
               fb.notes
            FROM
                node_frontbp fb,
                node_model nm,
                ibox_sw_release ib
            WHERE fb.releaseid = ib.id
            AND ib.nodemodelid = nm.id
            AND fb.id = %s

        """
        params = (frontbpid,)

        db.cursor.execute(query, params)
        fetch = db.cursor.fetchone()
        if not fetch:
            return {}
        return dict(fetch)

    # ____________________________

    def fetch_page(self, params):

        sort_field = params['sort_field']
        sort_order = params['sort_order']
        limit = params.get('limit', None)
        offset = params.get('offset', 0)

        query = """
            SELECT
               fb.id,
               fb.pn,
               fb.revision,
               fb.mixpn,
               ib.release,
               fb.version,
               nm.model as node_model,
               fb.firmware_bin,
               fb.firmware_exe,
               fb.hwstatus_check,
               fb.notes
            FROM
                node_frontbp fb,
                node_model nm,
                ibox_sw_release ib
            WHERE fb.releaseid = ib.id
            AND ib.nodemodelid = nm.id

            ORDER BY %s %s
            LIMIT %s
            OFFSET %s
        """
        params = (AsIs(sort_field), AsIs(sort_order), AsIs(limit), AsIs(offset))

        db.cursor.execute(query, params)
        fetch = db.cursor.fetchall()
        logger.debug("Fetch page type: {}".format(type(fetch)))
        return fetch
    # ____________________________

    def update(self, frontbpid, frontbp):

        try:
            query = """
                UPDATE %s
                SET
                    pn = %s,
                    mixpn = %s,
                    version = %s,
                    revision = %s,
                    firmware_bin = %s,
                    firmware_exe = %s,
                    hwstatus_check = %s,
                    notes = %s
                WHERE id = %s
            """
            params = (
                AsIs(self.table),
                frontbp['frontbp_pn'],
                frontbp['frontbp_mixpn'],
                frontbp['frontbp_version'],
                frontbp['frontbp_revision'],
                frontbp['firmware_bin'],
                frontbp['firmware_exe'],
                frontbp['hwstatus_check'],
                frontbp['notes'],
                frontbpid
            )

            db.cursor.execute(query, params)
            db.conn.commit()
        except Exception:
            db.conn.rollback()
            raise
    # ____________________________
