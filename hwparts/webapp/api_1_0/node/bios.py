from flask import jsonify, request, make_response
from config import logger
from .. import api
from models import Bios
from webapp.lib import reporthelper
biosmod = Bios()
# ====================================================

"""
It is a common practice to define
URLs that represent collections of resources with a trailing slash, as this gives them a
"folder" representation.
"""
# ====================================================


@api.route('/node/bios', methods=['POST'])
def add_node_bios():
    bios_to_add = request.get_json()['bios']
    logger.debug("BIOS to add: %r", bios_to_add)

    try:
        biosid = biosmod.add(bios_to_add)
        return jsonify({'biosid': biosid})

    except Exception as e:
        logger.exception("!ERROR")
        return make_response(jsonify({'message': str(e)}), 500)
# ______________________________________


@api.route('/node/bios/<int:biosid>', methods=['PUT'])
def node_bios_update(biosid):
    try:
        bios = request.get_json()['bios']
        logger.debug("BIOS to update by ID: %r", biosid)
        logger.debug("BIOS to update: %r", bios)

        biosmod.update(biosid, bios)
        return jsonify({'biosid': biosid})

    except Exception as e:
        logger.exception("!ERROR")
        return make_response(jsonify({'message': str(e)}), 500)
# ______________________________________


@api.route('/node/bios/', methods=['GET'])
def get_bios_page():
    logger.debug("Get nodes args: {}".format(request.args))

    current_page = int(request.args['page'])
    rows_limit = int(request.args['rows'])
    bios_total = biosmod.fetch_total()
    logger.debug("BIOS total: %s", bios_total)
    total_pages, offset = reporthelper.evaluate_page_data(bios_total, current_page, rows_limit)
    logger.debug("Total pages: %s", total_pages)
    logger.debug("Offset: %s", offset)

    args = {
        'offset': offset,
        'limit': rows_limit,
        'sort_field': request.args['sidx'],
        'sort_order': request.args['sord']
    }

    bios_page = biosmod.fetch_page(args)
    logger.debug("BIOS page: %r", bios_page)

    response_data = {
        "totalpages": total_pages,
        "totalrecords": bios_total,
        "currpage": current_page,
        "bios": [dict(b) for b in bios_page]
    }

    return jsonify(response_data)
# ______________________________________


@api.route('/node/bios/<int:biosid>')
def get_node_bios_by_id(biosid):
    bios = biosmod.fetch_by_id(biosid)
    logger.debug("Fetched BIOS: %r", bios)
    return jsonify({'bios': bios})
# ______________________________________
