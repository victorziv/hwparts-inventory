import sys
from flask import request, jsonify, make_response
from config import logger
from .. import api
from webapp.lib import reporthelper
from models import (
    Perc, Bios, Idrac, Frontbp, Rearbp, PSU
)
# from webapp.lib import reporthelper
percmod = Perc()
biosmod = Bios()
idracmod = Idrac()
frontbpmod = Frontbp()
rearbpmod = Rearbp()
psumod = PSU()

# ====================================================


@api.route('/node/<component>/', methods=['GET'])
def get_component_page(component):
    logger.debug("Get nodes args: {}".format(request.args))

    current_page = int(request.args['page'])
    rows_limit = int(request.args['rows'])
    mod = getattr(sys.modules[__name__], '%smod' % component)

    total = mod.fetch_total()
    logger.debug("Total: %s", total)
    total_pages, offset = reporthelper.evaluate_page_data(total, current_page, rows_limit)
    logger.debug("Total pages: %s", total_pages)
    logger.debug("Offset: %s", offset)

    args = {
        'offset': offset,
        'limit': rows_limit,
        'sort_field': request.args['sidx'],
        'sort_order': request.args['sord']
    }

    page = mod.fetch_page(args)
    logger.debug("%s page: %r", component, page)

    response_data = {
        "totalpages": total_pages,
        "totalrecords": total,
        "currpage": current_page,
        "component": [dict(c) for c in page]
    }

    return jsonify(response_data)
# ______________________________________


@api.route('/node/<component>/<int:componentid>')
def get_node_component_by_id(component, componentid):
    mod = getattr(sys.modules[__name__], '%smod' % component)
    data = mod.fetch_by_id(componentid)

    if not data:
        return make_response(
            jsonify({'message': "No %s found with ID %s" % (component, componentid)}), 404)

    return jsonify({'component': data})
# ______________________________________


@api.route('/node/<component>', methods=['POST'])
def node_component_add(component):
    component_to_add = request.get_json()['component']
    logger.debug("Component %s to add: %r", component, component_to_add)

    try:
        mod = getattr(sys.modules[__name__], '%smod' % component)
        componentid = mod.add(component_to_add)
        return jsonify({'componentid': componentid}), 201

    except Exception as e:
        logger.exception("!ERROR")
        return make_response(jsonify({'message': str(e)}), 500)
# ______________________________________


@api.route('/node/<component>/<int:componentid>', methods=['DELETE'])
def node_component_delete(component, componentid):
    try:
        logger.debug("Component %s to delete by ID: %r", component, componentid)

        mod = getattr(sys.modules[__name__], '%smod' % component)
        mod.delete_by_id(componentid)
        return jsonify({'componentid': componentid})

    except Exception as e:
        logger.exception("!ERROR")
        return make_response(jsonify({'message': str(e)}), 500)
# ______________________________________


@api.route('/node/<component>/<int:componentid>', methods=['PUT'])
def node_component_update(component, componentid):
    try:
        component_params = request.get_json()['component']
        logger.debug("Component to update by ID: %r", componentid)
        logger.debug("Component to update: %r", component_params)

        mod = getattr(sys.modules[__name__], '%smod' % component)
        mod.update(componentid, component_params)
        return jsonify({'componentid': componentid})

    except Exception as e:
        logger.exception("!ERROR")
        return make_response(jsonify({'message': str(e)}), 500)
# ______________________________________
