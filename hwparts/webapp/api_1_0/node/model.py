from flask import jsonify, request, make_response
from config import logger
from .. import api
from models import Node
from webapp.lib import reporthelper
modelmod = Node()
# ====================================================


@api.route('/node/model/', methods=['POST'])
def add_node_model():
    model_to_add = request.get_json()['model']
    logger.debug("Node model to add: %r", model_to_add)

    try:
        modelid = modelmod.add(model_to_add)
        return jsonify({'modelid': modelid})

    except Exception as e:
        logger.exception("!ERROR")
        return make_response(jsonify({'message': str(e)}), 500)
# ______________________________________


@api.route('/node/model/<int:modelid>', methods=['PUT'])
def node_model_update(modelid):
    try:
        model = request.get_json()['model']
        logger.debug("BIOS to update by ID: %r", modelid)
        logger.debug("BIOS to update: %r", model)

        modelmod.update(modelid, model)
        return jsonify({'modelid': modelid})

    except Exception as e:
        logger.exception("!ERROR")
        return make_response(jsonify({'message': str(e)}), 500)
# ______________________________________


@api.route('/node/model/<int:modelid>', methods=['DELETE'])
def node_model_delete(modelid):
    try:
        logger.debug("Model to delete by ID: %r", modelid)

        modelmod.delete_by_id(modelid)
        return jsonify({'modelid': modelid})

    except Exception as e:
        logger.exception("!ERROR")
        return make_response(jsonify({'message': str(e)}), 500)
# ______________________________________


@api.route('/node/model/', methods=['GET'])
def get_model_page():
    logger.debug("Get nodes args: {}".format(request.args))

    current_page = int(request.args['page'])
    rows_limit = int(request.args['rows'])
    total = modelmod.fetch_total()
    logger.debug("Models total: %s", total)
    total_pages, offset = reporthelper.evaluate_page_data(total, current_page, rows_limit)
    logger.debug("Total pages: %s", total_pages)
    logger.debug("Offset: %s", offset)

    args = {
        'offset': offset,
        'limit': rows_limit
    }
    models = modelmod.fetch_page(args)
    logger.debug("Node models page: %r", models)

    response_data = {
        "totalpages": total_pages,
        "totalrecords": total,
        "currpage": current_page,
        "model": [dict(m) for m in models]
    }

    return jsonify(response_data)
# ______________________________________


@api.route('/node/model/<int:modelid>')
def get_model_by_id(modelid):
    node_model = modelmod.fetch_by_id(modelid)
    return jsonify({'model': node_model})
# ______________________________________
