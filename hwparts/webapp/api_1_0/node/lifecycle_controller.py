from flask import jsonify, request, make_response
from config import logger
from .. import api
from models import LifecycleController
from webapp.lib import reporthelper
lifecontmod = LifecycleController()
# ====================================================

"""
It is a common practice to define
URLs that represent collections of resources with a trailing slash, as this gives them a
"folder" representation.
"""
# ====================================================


@api.route('/node/lifecont', methods=['POST'])
def node_lifecont_create():
    lifecont_to_add = request.get_json()['lifecycle_controller']
    logger.debug("Lifecycle controller to add: %r", lifecont_to_add)

    try:
        lifecontid = lifecontmod.add(lifecont_to_add)
        return jsonify({'lifecontid': lifecontid})

    except Exception as e:
        logger.exception("!ERROR")
        return make_response(jsonify({'message': str(e)}), 500)
# ______________________________________


@api.route('/node/lifecont/<int:lifecontid>', methods=['PUT'])
def node_lifecont_update(lifecontid):
    try:
        lifecont = request.get_json()['lifecont']
        logger.debug("LC to update by ID: %r", lifecontid)
        logger.debug("LC to update: %r", lifecont)

        lifecontmod.update(lifecontid, lifecont)
        return jsonify({'lifecontid': lifecontid})

    except Exception as e:
        logger.exception("!ERROR")
        return make_response(jsonify({'message': str(e)}), 500)
# ______________________________________


@api.route('/node/lifecont/', methods=['GET'])
def get_lifecont_page():
    logger.debug("Get nodes args: {}".format(request.args))

    current_page = int(request.args['page'])
    rows_limit = int(request.args['rows'])
    lifecycle_controller_total = lifecontmod.fetch_total()
    logger.debug("LC total: %s", lifecycle_controller_total)
    total_pages, offset = reporthelper.evaluate_page_data(lifecycle_controller_total, current_page, rows_limit)
    logger.debug("Total pages: %s", total_pages)
    logger.debug("Offset: %s", offset)

    args = {
        'offset': offset,
        'limit': rows_limit,
        'sort_field': request.args['sidx'],
        'sort_order': request.args['sord']
    }

    page = lifecontmod.fetch_page(args)
    logger.debug("Lifecycle Controller page: %r", page)

    response_data = {
        "totalpages": total_pages,
        "totalrecords": lifecycle_controller_total,
        "currpage": current_page,
        "lifecont": [dict(lc) for lc in page]
    }

    return jsonify(response_data)
# ______________________________________


@api.route('/node/lifecont/<int:lifecontid>')
def get_by_id(lifecontid):
    lifecont = lifecontmod.fetch_by_id(lifecontid)
    return jsonify({'lifecont': lifecont})
# ______________________________________


@api.route('/node/lifecont/<int:lifecontid>', methods=['DELETE'])
def node_lifecont_delete(lifecontid):
    try:
        logger.debug("Lifecycle Controller to delete by ID: %r", lifecontid)

        lifecontmod.delete_by_id(lifecontid)
        return jsonify({'lifecontid': lifecontid})

    except Exception as e:
        logger.exception("!ERROR")
        return make_response(jsonify({'message': str(e)}), 500)
# ______________________________________
