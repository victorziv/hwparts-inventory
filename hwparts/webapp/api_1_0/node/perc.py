from flask import jsonify
from .. import api
from config import logger
from models import Perc
percmod = Perc()
# ====================================================


@api.route('/node/perc/revisions/')
def get_perc_revisions():
    revisions = percmod.fetch_revisions()
    return jsonify({'results': revisions})
# ______________________________________


@api.route('/node/perc/revisions/supported/<percid>')
def get_perc_supported_revisions(percid):
    revisions = percmod.fetch_supported_revisions(percid)
    logger.debug("Supported revisions: %r", revisions)

    return jsonify({'results': revisions})
# ______________________________________
