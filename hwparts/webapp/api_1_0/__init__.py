from flask import Blueprint
api = Blueprint('api', __name__)
from . import vendor, ibox  # noqa
from .node import (  # noqa
    model,
    bios,
    lifecycle_controller,
    perc,
    component
)
