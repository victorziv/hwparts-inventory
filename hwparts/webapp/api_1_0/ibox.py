from flask import jsonify, request
from config import logger
from . import api
from models import Ibox
iboxmod = Ibox()
# ====================================================

"""
It is a common practice to define
URLs that represent collections of resources with a trailing slash, as this gives them a
"folder" representation.
"""
# ====================================================


@api.route('/ibox/release/', methods=['GET'])
def get_ibox_releases():
    try:
        releases = iboxmod.fetch_releases()
        logger.debug("Releases: %r", releases)
        return jsonify(releases)
    except Exception:
        logger.exception('!ERROR')
        raise
# ______________________________________


@api.route('/ibox/release/node/model', methods=['GET'])
def get_node_model_by_release():
    try:
        logger.debug("ARGS: %r", request.args)
        release = request.args['release']
        node_model = iboxmod.fetch_node_model_by_release(release)
        logger.debug("Node model: %r", node_model)
        return jsonify({'model': node_model})
    except Exception:
        logger.exception('!ERROR')
        raise
# ______________________________________
