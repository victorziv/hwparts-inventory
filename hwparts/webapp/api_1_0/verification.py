from flask import jsonify, request
from config import logger
from . import api
from models import VerificationSession
# ===============================


def calculate_session_state(states):
    if all([s == 'finished' for s in states]):
        return "finished"

    if 'running' in states:
        return 'running'
    elif 'onhold' in states:
        return 'onhold'
    elif 'aborted' in states:
        return 'aborted'

    return 'pending'


@api.route('/verification/session/step/state', methods=['PUT'])
def update_session_step_status():
    params = request.get_json()
    logger.debug("Got params: %r", params)
    stepid = params['stepid']
    state = params['state']
    logger.debug("Step ID: %r", stepid)
    logger.debug("Step state: %r", state)
    session = VerificationSession()
    session.update_step_state(stepid=stepid, state=state)
    return jsonify({'status': True})
# ______________________________________


@api.route('/verification/session/state', methods=['PUT'])
def update_session_status():
    params = request.get_json()
    logger.debug("Got params: %r", params)
    sessionid = params['sessionid']
    logger.debug("Session ID: %r", sessionid)
    sessionmod = VerificationSession()
    step_states = sessionmod.fetch_session_step_states(sessionid)
    logger.debug("Fetched step states for sessionid %s: %r", sessionid, step_states)
    state = calculate_session_state(step_states)
    sessionmod.update_session_state(sessionid, state)
    return jsonify({'state': state})
# ______________________________________
