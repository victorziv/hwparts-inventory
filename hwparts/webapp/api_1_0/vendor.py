from flask import jsonify, request
from config import logger
from . import api
from models import Vendor, Ware
from webapp.lib import reporthelper
vendormod = Vendor()
waremod = Ware()
# ====================================================

"""
It is a common practice to define
URLs that represent collections of resources with a trailing slash, as this gives them a
"folder" representation.
"""
# ====================================================


@api.route('/vendors/', methods=['POST'])
def add_vendor():
    vendor_to_add = request.get_json()['vendor']
    logger.debug("Vendor to add: %r", vendor_to_add)

    try:
        vendorid = vendormod.add(vendor_to_add)
        return jsonify({'vendorid': vendorid})

    except Exception:
        logger.exception("!ERROR")
        raise
# ______________________________________


@api.route('/vendors/', methods=['GET'])
def get_vendors():
    logger.debug("Get vendors args: {}".format(request.args))

    current_page = int(request.args['page'])
    rows_limit = int(request.args['rows'])
    vendors_total = vendormod.fetch_total()
    logger.debug("Vendors total: %s", vendors_total)
    total_pages, offset = reporthelper.evaluate_page_data(vendors_total, current_page, rows_limit)
    logger.debug("Total pages: %s", total_pages)
    logger.debug("Offset: %s", offset)

    args = {
        'offset': offset,
        'limit': rows_limit
    }
    vendors = vendormod.fetch_page(args)
    logger.debug("Vendors: %r".format(vendors))

    response_data = {
        "totalpages": total_pages,
        "totalrecords": vendors_total,
        "currpage": current_page,
        "vendors": [dict(v) for v in vendors]
    }

    return jsonify(response_data)
# ______________________________________


@api.route('/vendor/names', methods=['GET'])
def get_vendor_names():
    args = request.args
    logger.debug("ARGS: %r", args)
    vendor_names = vendormod.fetch_names(args['ware'])
    logger.debug("Vendor names: %r", vendor_names)
    return jsonify(vendor_names)
# ______________________________________


@api.route('/ware/', methods=['GET'])
def get_wares():
    wares = waremod.fetch_names()
    logger.debug("Wares: %r", wares)
    return jsonify(wares)
# ______________________________________


@api.route('/vendors/<int:vendorid>')
def get_vendor_by_id(caseid):
    vendor = vendormod.fetch_by_id(caseid)
    return jsonify({'vendor': vendor})
# ______________________________________


@api.route('/vendors/<name>')
def get_vendor_by_name(name):
    vendor = vendormod.fetch_by_name(name)
    return jsonify({'vendor': vendor})
