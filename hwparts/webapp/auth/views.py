from flask import redirect, url_for, flash
from flask_login import login_user, logout_user, login_required, current_user
from config import logger
from . import auth
from .oauth import OAuthSignIn
from models import User
# __________________________________________


# @auth.before_app_request
# def before_request():
#     if current_user.is_authenticated:
#         current_user.update_last_seen()
# __________________________________________


@auth.route('/authorize/<provider>')
def oauth_authorize(provider):
    logger.debug("PROVIDER: %r", provider)
    logger.debug("Current user: %r", current_user)
    if not current_user.is_anonymous:
        redirect(url_for('main.dashboard'))

    oauth = OAuthSignIn.get_provider(provider)
    return oauth.authorize()
# __________________________________________


@auth.route('/callback/<provider>')
def oauth_callback(provider):

    if not current_user.is_anonymous:
        return redirect(url_for('main.home'))

    oauth = OAuthSignIn.get_provider(provider)
    profile = oauth.callback()

    if 'hd' not in profile or profile['hd'] != 'infinidat.com':
        logger.error("Not Infinidat user: %s", profile['email'])
        flash('Only Infinidat users are allowed')
        return redirect(url_for('main.home'))

    if profile['social_id'] is None:
        logger.error("Authentication failed")
        flash('Authentication failed.')
        return redirect(url_for('main.inventory'))

    u = User()
    user = u.get_by_social_id(profile['social_id'])
    logger.debug('User found in DB: %r', user)
    if not user:
        user = u.save(profile)

    login_user(user, True)
    return redirect(url_for('main.home'))
# __________________________________________


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    flash("You have been logged out!")
    return redirect(url_for('main.home'))
# __________________________________________
