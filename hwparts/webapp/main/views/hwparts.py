from flask import render_template
from .. import main
# ______________________________


@main.route('/home', methods=['GET'])
def home():
    return render_template("main/home/home.html")
# ______________________________


@main.route('/', methods=['GET'])
@main.route('/hwparts/home', methods=['GET'])
def hwparts_home():
    return render_template("main/home/hwparts_home.html")
# ______________________________


@main.route('/documentation', methods=['GET'])
def documentation():
    return render_template("main/documentation/documentation.html")
# ______________________________


params_vendor = {
    'caption': 'HW Vendor',
    'component': 'vendor',
}


@main.route('/view/vendor', methods=['GET'])
def vendor():
    return render_template(
        "main/vendor/vendor.html",
        params=params_vendor
    )
# ______________________________


@main.route('/view/vendor/form/add', methods=['GET'])
def form_vendor_add():
    params_vendor.update({'operation': 'add'})
    return render_template(
        "main/vendor/form_vendor_add.html",
        params=params_vendor
    )
# ______________________________


@main.route('/view/vendor/form/edit/<int:vendorid>', methods=['GET'])
def form_vendor_edit(vendorid):
    params_vendor.update({
        'operation': 'edit',
        'selectedid': vendorid
    })
    return render_template(
        "main/vendor/form_vendor_edit.html",
        params=params_vendor
    )
#
