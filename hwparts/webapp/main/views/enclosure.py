from flask import render_template
from .. import main
# =====================================

params_encmaster = {
    'category': 'enclosure',
    'caption': 'Enclosure Master',
    'component': 'encmaster'
}


@main.route('/view/enclosure/encmaster', methods=['GET'])
def enclosure_encmaster():
    return render_template(
        "main/enclosure/encmaster/encmaster.html",
        params=params_encmaster
    )
# =====================================


params_encpsu = {
    'category': 'enclosure',
    'caption': 'Enclosure PSU',
    'component': 'encpsu'
}


@main.route('/view/enclosure/encpsu', methods=['GET'])
def enclosure_encpsu():
    return render_template(
        "main/enclosure/encpsu/encpsu.html",
        params=params_encpsu
    )
# =====================================


params_enciom = {
    'category': 'enclosure',
    'caption': 'Enclosure IOM',
    'component': 'enciom'
}


@main.route('/view/enclosure/enciom', methods=['GET'])
def enclosure_enciom():
    return render_template(
        "main/enclosure/enciom/enciom.html",
        params=params_enciom
    )
