from flask import render_template
from .. import main
# =====================================

params_drivemaster = {
    'category': 'drive',
    'caption': 'Drive Master',
    'component': 'drivemaster'
}


@main.route('/view/drive/drivemaster', methods=['GET'])
def drive_drivemaster():
    return render_template(
        "main/drive/drivemaster/drivemaster.html",
        params=params_drivemaster
    )
# =====================================


params_inquiry = {
    'category': 'drive',
    'caption': 'Drive Inquiry',
    'component': 'inquiry'
}


@main.route('/view/drive/inquiry', methods=['GET'])
def drive_inquiry():
    return render_template(
        "main/drive/inquiry/inquiry.html",
        params=params_inquiry
    )
# =====================================


params_modepages = {
    'category': 'drive',
    'caption': 'Drive Mode Pages',
    'component': 'modepages'
}


@main.route('/view/drive/modepages', methods=['GET'])
def drive_modepages():
    return render_template(
        "main/drive/modepages/modepages.html",
        params=params_modepages
    )
# =====================================


params_mixingkey = {
    'category': 'drive',
    'caption': 'Drive Mixing Key',
    'component': 'mixingkey'
}


@main.route('/view/drive/mixingkey', methods=['GET'])
def drive_mixingkey():
    return render_template(
        "main/drive/mixingkey/mixingkey.html",
        params=params_mixingkey
    )
# =====================================


params_hddrc = {
    'category': 'drive',
    'caption': 'HDD Rejection Criteria',
    'component': 'hddrc'
}


@main.route('/view/drive/hddrc', methods=['GET'])
def drive_hddrc():
    return render_template(
        "main/drive/hddrc/hddrc.html",
        params=params_hddrc
    )
# =====================================


params_ssdrc = {
    'category': 'drive',
    'caption': 'SSD Rejection Criteria',
    'component': 'ssdrc'
}


@main.route('/view/drive/ssdrc', methods=['GET'])
def drive_ssdrc():
    return render_template(
        "main/drive/ssdrc/ssdrc.html",
        params=params_ssdrc
    )
