from flask import render_template
from .. import main
# =====================================

params_fcadapter = {
    'category': 'fbchannel',
    'caption': 'FC Adapter',
    'component': 'fcadapter'
}


@main.route('/view/fbchannel/fcadapter', methods=['GET'])
def fbchannel_fcadapter():
    return render_template(
        "main/fbchannel/fcadapter/fcadapter.html",
        params=params_fcadapter
    )

# =====================================


params_fcgbic = {
    'category': 'fbchannel',
    'caption': 'FC GBIC',
    'component': 'fcgbic'
}


@main.route('/view/fbchannel/fcgbic', methods=['GET'])
def fbchannel_fcgbic():
    return render_template(
        "main/fbchannel/fcgbic/fcgbic.html",
        params=params_fcgbic
    )

# =====================================
