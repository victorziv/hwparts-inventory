from flask import render_template
from .. import main
# =====================================

params_pdu = {
    'category': 'electricity',
    'caption': 'PDU',
    'component': 'pdu'
}


@main.route('/view/electricity/pdu', methods=['GET'])
def electricity_pdu():
    return render_template(
        "main/electricity/pdu/pdu.html",
        params=params_pdu
    )
# =====================================
