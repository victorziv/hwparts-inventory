from flask import render_template
from .. import main
# =====================================

params_ethcard = {
    'category': 'controller',
    'caption': 'Ethernet Card',
    'component': 'ethcard'
}


@main.route('/view/controller/ethcard', methods=['GET'])
def controller_ethcard():
    return render_template(
        "main/controller/ethcard/ethcard.html",
        params=params_ethcard
    )

# =====================================


params_ibcard = {
    'category': 'controller',
    'caption': 'InfiniBand Card',
    'component': 'ibcard'
}


@main.route('/view/controller/ibcard', methods=['GET'])
def controller_ibcard():
    return render_template(
        "main/controller/ibcard/ibcard.html",
        params=params_ibcard
    )

# =====================================


params_sascard = {
    'category': 'controller',
    'caption': 'SAS Card',
    'component': 'sascard'
}


@main.route('/view/controller/sascard', methods=['GET'])
def controller_sascard():
    return render_template(
        "main/controller/sascard/sascard.html",
        params=params_sascard
    )

# =====================================
