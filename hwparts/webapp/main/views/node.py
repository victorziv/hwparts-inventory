from flask import render_template
from config import logger
from .. import main
# =====================================


@main.route('/view/node/model', methods=['GET'])
def node_model():
    return render_template(
        "main/node/model/model.html",
        params={
            'caption': 'Model',
            'component': 'model'
        }
    )
# ______________________________


@main.route('/view/node/model/form/add', methods=['GET'])
def form_node_model_add():
    return render_template(
        "main/node/model/form_model_add.html",
        params={
            'caption': 'Model',
            'component': 'model',
            'operation': 'add'
        }
    )
# ______________________________


@main.route('/view/node/model/form/edit/<int:modelid>', methods=['GET'])
def form_node_model_edit(modelid):
    logger.debug("Get node model with ID: %r", modelid)
    return render_template(
        "main/node/model/form_model_edit.html",
        params={
            'component': 'model',
            'operation': 'edit',
            'selectedid': modelid
        }
    )
# =====================================


@main.route('/view/node/bios', methods=['GET'])
def node_bios():
    return render_template(
        "main/node/bios/bios.html",
        params={
            'caption': 'BIOS',
            'component': 'bios'
        }
    )
# ______________________________


@main.route('/view/node/bios/form/add', methods=['GET'])
def form_bios_add():
    return render_template(
        "main/node/bios/form_bios_add.html",
        params={
            'caption': 'BIOS',
            'component': 'bios',
            'operation': 'add'
        }
    )
# ______________________________


@main.route('/view/node/bios/form/edit/<int:biosid>', methods=['GET'])
def form_bios_edit(biosid):
    logger.debug("Get BIOS with ID: %r", biosid)
    return render_template(
        "main/node/bios/form_bios_edit.html",
        params={
            'caption': 'BIOS',
            'component': 'bios',
            'operation': 'edit',
            'selected_biosid': biosid
        }
    )
# =====================================


params_idrac = {
    'caption': 'iDRAC',
    'component': 'idrac'
}


@main.route('/view/node/idrac', methods=['GET'])
def node_idrac():
    return render_template(
        "main/node/idrac/idrac.html",
        params=params_idrac
    )
# ____________________________________


@main.route('/view/node/idrac/form/add', methods=['GET'])
def form_idrac_add():
    params_idrac.update({'operation': 'add'})
    return render_template(
        "main/node/idrac/form_idrac_add.html",
        params=params_idrac
    )
# ____________________________________


@main.route('/view/node/idrac/form/edit/<int:idracid>', methods=['GET'])
def form_idrac_edit(idracid):
    params_idrac.update({'operation': 'edit', 'selectedid': idracid})
    return render_template(
        "main/node/idrac/form_idrac_edit.html",
        params=params_idrac
    )
# =====================================


@main.route('/view/node/lifecont', methods=['GET'])
def lifecont():
    return render_template(
        "main/node/lifecont/lifecont.html",
        params={
            'caption': 'LC',
            'component': 'lifecont'
        }
    )
# ______________________________


@main.route('/view/node/lifecont/form/add', methods=['GET'])
def form_lifecont_add():
    return render_template(
        "main/node/lifecont/form_lifecont_add.html",
        params={
            'component': 'lifecont',
            'operation': 'add'
        }
    )
# ______________________________


@main.route('/view/node/lifecont/form/edit/<int:lifecontid>', methods=['GET'])
def form_lifecont_edit(lifecontid):
    return render_template(
        "main/node/lifecont/form_lifecont_edit.html",
        params={
            'component': 'lifecont',
            'operation': 'edit',
            'selected_lifecontid': lifecontid
        }
    )
# =====================================


params_perc = {
    'caption': 'PERC',
    'component': 'perc'
}


@main.route('/view/node/perc', methods=['GET'])
def node_perc():
    return render_template(
        "main/node/perc/perc.html",
        params=params_perc
    )
# ______________________________


@main.route('/view/node/perc/form/add', methods=['GET'])
def form_perc_add():
    params_perc.update({'operation': 'add'})
    return render_template(
        "main/node/perc/form_perc_add.html",
        params=params_perc
    )
# ______________________________


@main.route('/view/node/perc/form/edit/<int:percid>', methods=['GET'])
def form_perc_edit(percid):
    params_perc.update({
        'operation': 'edit',
        'selectedid': percid
    })

    return render_template(
        "main/node/perc/form_perc_edit.html",
        params=params_perc
    )
# =====================================


params_frontbp = {
    'caption': 'Front BP',
    'component': 'frontbp'
}


@main.route('/view/node/frontbp', methods=['GET'])
def node_frontbp():
    return render_template(
        "main/node/frontbp/frontbp.html",
        params=params_frontbp
    )
# ______________________________


@main.route('/view/node/frontbp/form/add', methods=['GET'])
def form_frontbp_add():
    params_frontbp.update({'operation': 'add'})
    return render_template(
        "main/node/frontbp/form_frontbp_add.html",
        params=params_frontbp
    )
# ______________________________


@main.route('/view/node/frontbp/form/edit/<int:frontbpid>', methods=['GET'])
def form_frontbp_edit(frontbpid):
    params_frontbp.update({
        'operation': 'edit',
        'selectedid': frontbpid
    })

    return render_template(
        "main/node/frontbp/form_frontbp_edit.html",
        params=params_frontbp
    )
# =====================================


params_rearbp = {
    'caption': 'Rear BP',
    'component': 'rearbp'
}


@main.route('/view/node/rearbp', methods=['GET'])
def node_rearbp():
    return render_template(
        "main/node/rearbp/rearbp.html",
        params=params_rearbp
    )
# ______________________________


@main.route('/view/node/rearbp/form/add', methods=['GET'])
def form_rearbp_add():
    params_rearbp.update({'operation': 'add'})
    return render_template(
        "main/node/rearbp/form_rearbp_add.html",
        params=params_rearbp
    )
# ______________________________


@main.route('/view/node/rearbp/form/edit/<int:rearbpid>', methods=['GET'])
def form_rearbp_edit(rearbpid):
    params_rearbp.update({
        'operation': 'edit',
        'selectedid': rearbpid
    })

    return render_template(
        "main/node/rearbp/form_rearbp_edit.html",
        params=params_rearbp
    )
# =====================================


params_psu = {
    'caption': 'PSU',
    'component': 'psu'
}


@main.route('/view/node/psu', methods=['GET'])
def node_psu():
    return render_template(
        "main/node/psu/psu.html",
        params=params_psu
    )
# ______________________________


@main.route('/view/node/psu/form/add', methods=['GET'])
def form_psu_add():
    params_psu.update({'operation': 'add'})
    return render_template(
        "main/node/psu/form_psu_add.html",
        params=params_psu
    )
# ______________________________


@main.route('/view/node/psu/form/edit/<int:psuid>', methods=['GET'])
def form_psu_edit(psuid):
    params_psu.update({
        'operation': 'edit',
        'selectedid': psuid
    })
    return render_template(
        "main/node/psu/form_psu_edit.html",
        params=params_psu
    )
# =====================================
