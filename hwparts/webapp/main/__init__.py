from flask import Blueprint
main = Blueprint('main', __name__)
from .views import hwparts  # noqa
from .views import node  # noqa
from .views import controller  # noqa
from .views import fiber_channel  # noqa
from .views import enclosure  # noqa
from .views import drive  # noqa
from .views import electricity  # noqa
