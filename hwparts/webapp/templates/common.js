// ======= Form Functions =============

function backToParentPage(component) {
    var url = '/view/node/' + component;
    window.location.href = url;
}
//________________________________________

function initButtons() {
    jQuery("#btn_node_{{ params.component }}_add").on('click', showFormAdd);
    jQuery("#btn_node_{{ params.component }}_edit").on('click', showFormEdit);
    jQuery("#btn_node_{{ params.component }}_delete").on('click', showDeleteConfirmation);
}
//________________________________________

function initGrid() {
    initGridResize(
        "#node_{{ params.component }}_grid_container",
        "#node_{{ params.component }}_grid",
        "#node_{{ params.component }}_grid_pager"
    );
}
//________________________________________

// ======= /Form Functions =============

// ======= ADD Functions =============
function collectFormAddFields() {
    var component = {};
    var component_inputs = jQuery('form#{{params.component}}_add_form .{{params.component}}_field');

    component_inputs.each(function() {
        component[this.name] = jQuery(this).val();
    });

    if (jQuery("form#{{params.component}}_add_form #hwstatus_check").is(':checked')) {
        component.hwstatus_check = true;
    } else {
        component.hwstatus_check = false;
    }

    return component;
}
//________________________________________

function initFormAdd() {

    var component = "{{ params.component }}";

    jQuery("#btn_cancel_{{params.component}}_add").on('click', function() {
        resetFormAdd();
        backToParentPage(component);
    });

    jQuery("form#{{params.component}}_add_form #btn_submit_{{params.component}}_add").on('click', onSubmitAdd);

    jQuery('form#{{params.component}}_add_form').on('keydown', function(evt) {
        if (evt.which === 13) {
            evt.preventDefault();
            onSubmitAdd();
            return false;
        }
    });

    jQuery('form#{{params.component}}_add_form select#ibox_release').on('change', function() { onIboxReleaseChange('{{params.component}}_add_form')});

}
//________________________________________

function showFormAdd() {
    var formAddURL = '/view/node/{{params.component}}/form/add';
    window.location.href = formAddURL;
}
//________________________________________

function onShowFormAdd() {
    populateIboxReleaseList(formadd);
    jQuery('form#{{params.component}}_add_form input#{{params.component}}_version').focus();
}
//________________________________________

// ======= /ADD Functions =============
// ======= EDIT Functions =============

function collectFormEditFields() {
    var component = {};
    var component_inputs = jQuery('form#{{ params.component }}_edit_form .{{ params.component }}_field_editable'); 

    component_inputs.each(function() { 
        component[this.name] = jQuery(this).val(); 
    }); 

    if (jQuery("form#{{ params.component }}_edit_form #hwstatus_check").is(':checked')) {
        component.hwstatus_check = true;
    } else {
        component.hwstatus_check = false;
    }

    return component;
}
//________________________________________

function initFormEdit() {

    jQuery("#btn_submit_{{ params.component }}_edit").on('click', onSubmitEdit);
    jQuery("#btn_cancel_{{ params.component }}_edit").on('click', function() { backToParentPage(component)});

    jQuery("form#{{ params.component }}_edit_form").on('keydown', function(evt) {
        if (evt.which === 13) {
            evt.preventDefault();
            onSubmitEdit();
            return false;
        }
    });
}
//________________________________________

function onShowFormEdit() {
    jQuery('form#{{ params.component}}_edit_form input#{{ params.component}}_version').focus();
    var getComponentUrl = "{{ config.API_URL_PREFIX }}/node/{{ params.component}}/{{ params.selectedid }}";

    var aj = jQuery.ajax({
        type: "GET",
        url: getComponentUrl,
        dataType: "json",
        async: true,

        beforeSend: function() {},
    });

    aj.done(function( data, textStatus, obj ) {
        console.log(data);
        populateFormEdit(data);
    });

    aj.always(function() {});

    aj.fail(function( xtr, textStatus, errorThrown ) {
        var msg = JSON.parse(xtr.responseText).message;
        notifyError(msg);
    });

}
//________________________________________

function showFormEdit() {
    var rowid = jQuery("#node_{{ params.component }}_grid").jqGrid ('getGridParam', 'selrow');
    if (! rowid) {
        notifyWarning("No {{ params.caption }} was selected for edit");
        return false;
    }
    var url = '/view/node/{{ params.component }}/form/edit/' + rowid;
    window.location.href = url;
}
// ======= /EDIT Functions =============

// ======= DELETE Functions =============

function onSubmitDelete(componentid) {

    var version = jQuery("#node_{{ params.component }}_grid").jqGrid('getRowData', componentid).version;

    var aj = jQuery.ajax({
        type: "DELETE",
        url: "{{ config.API_URL_PREFIX }}/node/{{ params.component }}/" + componentid,
        contentType: "application/json",
        dataType: "json",
        async: true,

        beforeSend: function() {},
    });

    aj.done(function( data, textStatus, obj ) {
        var msg = "Node {{ params.caption }} " + version + " has been deleted!";
        notifySuccess(msg, function() { backToParentPage(component)});
    });

    aj.always(function() {});

    aj.fail(function( xtr, textStatus, errorThrown ) {
        var msg = JSON.parse(xtr.responseText).message;
        notifyError(msg);
    });
}

// _________________________________________

function showDeleteConfirmation() {
    var rowid = jQuery("#node_{{ params.component }}_grid").jqGrid ('getGridParam', 'selrow');
    if (! rowid) {
        notifyWarning("No {{ params.caption }} was selected for deletion");
        return false;
    }

    var version = jQuery("#node_{{ params.component }}_grid").jqGrid('getRowData', rowid).version;

    confirmAction("Deleting {{ params.caption }} <B>"+version+"</B>", onSubmitDelete, rowid);
}

// ======= /DELETE Functions =============

function confirmAction(msg, action_callback, callback_param ) {
    var dlg = new BootstrapDialog(
        {
            type: BootstrapDialog.TYPE_WARNING,
            size: BootstrapDialog.SIZE_MEDIUM,
            title: '<H3>Are you sure?</H3>',
            draggable: true,
            message: '<H4>'+msg+'</H4>',
            closable: true,
            buttons : [
                {
                   label: 'Go',
                   action: function(dialogRef){
                       action_callback(callback_param);
                       dialogRef.close();
                   }
                },
                {
                   label: 'Cancel',
                   action: function(dialogRef){
                       dialogRef.close();
                   }
                },
            ]
        }
    );

    dlg.open();
}
// __________________________________

function fwBinUrlFormatter(cellvalue, options, rowObject) {
    if ( ! cellvalue ) {
        return '';
    }
    var href = '<a href="'+cellvalue+'" target=_blank><i class="fa fa-link"></i> BIN'; 
    return href;
}
//________________________________________

function fwExeUrlFormatter(cellvalue, options, rowObject) {
    if ( ! cellvalue ) {
        return '';
    }
    var href = '<a href="'+cellvalue+'" target=_blank><i class="fa fa-link"></i> EXE'; 
    return href;
}
//________________________________________

function fwRomUrlFormatter(cellvalue, options, rowObject) {
    if ( ! cellvalue ) {
        return '';
    }
    var href = '<a href="'+cellvalue+'" target=_blank><i class="fa fa-link"></i> ROM'; 
    return href;
}
//________________________________________


function initGridResize(containerid, gridid, pagerid) {
    var container = jQuery(containerid);
    var grid = jQuery(gridid);
    var pager = jQuery(pagerid);

    grid.jqGrid('setGridWidth', container.width(), true);

    jQuery(window).resize(function() {
        var width = container.width();
        grid.setGridWidth(width, true);
        pager.setGridWidth(width, true);
    });
}
//________________________________________

function initInventorySelect() {
    jQuery('.select-hwparts-inventory').select2({
        tags: true
    });
}
//________________________________________



function localDateFormatter(cellvalue, options, rowObject) {
    if (  cellvalue ) {
        return moment(cellvalue).format('YYYY-MM-DD')
    }

    return ''
}
// ______________________________

function localDateTimeFormatter(cellvalue, options, rowObject) {
    if (  cellvalue ) {
        return moment(cellvalue).format('YYYY-MM-DD HH:mm')
    }

    return ''
}
// ______________________________


var myStack = {"dir1":"down", "dir2":"right", "push":"top"};

function notifyError(msg, callback) {

    if( typeof (PNotify) === 'undefined'){ return; }

    new PNotify({
        title: 'Error!',
        text: msg,
        type: 'error',
        delay: 5000,
        width: '500px',
        addclass: "stack-custom",
        stack: myStack,
        icon: 'fa fa-lg fa-exclamation-triangle',
        after_close: function(msg, timer_hide) {
           callback(); 
        }
    });
}
// __________________________________

function notifyWarning(msg, callback) {
    if( typeof (PNotify) === 'undefined'){ return; }

    new PNotify({
        title: 'Sorry!',
        text: msg,
        delay: 4000,
        icon: 'fa fa-lg fa-exclamation-circle',
        addclass: "stack-custom",
        stack: myStack,
        width: '500px',
        after_close: function(msg, timer_hide) {
            if ( typeof callback === 'undefined' ) {
                return false;
            } else {
               callback(msg); 
            }
        }
    });
}

// __________________________________

function notifySuccess(msg, callback) {

    if( typeof (PNotify) === 'undefined'){ return; }

    new PNotify({
        title: 'OK!',
        text: msg,
        type: 'success',
        delay: 2000,
        icon: 'fa fa-lg fa-check',
        width: '500px',
        addclass: "stack-custom",
        stack: myStack,
        after_close: function(msg, timer_hide) {
           callback(); 
        }
    });
}
// __________________________________

function onIboxReleaseChange(formid) {
    var release = jQuery("form#"+formid+" select#ibox_release").val();
    console.log("Release in select: " + release);
    var aj = jQuery.ajax({
        type: "GET",
        url: "{{ config.API_URL_PREFIX }}/ibox/release/node/model",
        dataType: "json",
        data: { 'release' : release},
        async: true,

        beforeSend: function() {},
    });

    aj.done(function( data, textStatus, obj ) {
        jQuery('form#'+formid+' input#node_model').val(data.model);
    });

    aj.always(function() {});

    aj.fail(function( xtr, textStatus, errorThrown ) {
        var msg = JSON.parse(xtr.responseText).message;
        notifyError(msg);
    });

}
//________________________________________

function populateIboxReleaseList(formid) {
    var aj = jQuery.ajax({
        type: "GET",
        url: "{{ config.API_URL_PREFIX }}/ibox/release/",
        dataType: "json",
        async: true,

        beforeSend: function() {},
    });

    aj.done(function( data, textStatus, obj ) {

            var ibox_release_select = jQuery('form#' + formid + ' select#ibox_release');
            var releases = []
            for (i=0;i<data.length;i++) {
                releases.push('<option value="'+data[i] + '">'+data[i] + '</option>');
            }
            ibox_release_select.empty();
            ibox_release_select.append(releases.join(''));
            jQuery("form#" + formid + " select#ibox_release").change();
            return false; 
    });

    aj.always(function() {});

    aj.fail(function( xtr, textStatus, errorThrown ) {
        var msg = JSON.parse(xtr.responseText).message;
        notifyError(msg);
    });
}

//________________________________________


function startWait(msg) {

}
// __________________________________

function stopWait() {
}
// __________________________________
