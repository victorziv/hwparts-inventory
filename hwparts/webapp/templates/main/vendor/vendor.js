"use strict";
{% include "common.js" %}
// ______________________________

var component = "{{ params.component }}";
var formadd = "{{ params.component}}_add_form";

var vendor_grid = jQuery("#node_{{ params.component }}_grid");

vendor_grid.jqGrid({
    {% include "main/node/grid-options.js" %}
    {% include "main/vendor/vendor_colmodel.js" %}
});
//________________________________________

{% include "main/node/buttons-grid.html" %}

// ============ FUNCTIONS ==================

function initForm() {
    initFormAdd();
    initFormEdit();
    initButtons();
}
//________________________________________

function validateForm() {

    var pn_input = jQuery("form#{{ params.component }}_{{ params.operation }}_form input#{{ params.component }}_pn");
    var pn = pn_input.val();
    if (! pn) {
        jQuery("#{{ params.component }}_pn_group").addClass("has-error");
        pn_input.focus();
        pn_input.attr("placeholder", "{{ params.caption }} P/N is required");
        return false;
    }

    var version_input = jQuery("form#{{ params.component }}_{{ params.operation }}_form input#{{ params.component }}_version");
    var version = version_input.val();
    if (! version) {
        jQuery("#{{ params.component }}_version_group").addClass("has-error");
        version_input.focus();
        version_input.attr("placeholder", "Version is required");
        return false;
    }

    return true;
}
//________________________________________

// ============ ADD FUNCTIONS ==================

function onSubmitAdd() {

    if ( ! validateForm() ) {
        return false;
    }

    var vendor = collectFormAddFields();
    var aj = jQuery.ajax({
        type: "POST",
        url: "{{ config.API_URL_PREFIX }}/{{params.component}}",
        data: JSON.stringify({ 'component' : vendor}),
        contentType: "application/json",
        dataType: "json",
        async: true,

        beforeSend: function() {},
    });

    aj.done(function( data, textStatus, obj ) {
        var msg = "{{ params.caption }} " + vendor.name + " has been added successfully!";
        notifySuccess(msg, function() { backToParentPage(component), resetFormAdd()});
    });

    aj.always();

    aj.fail(function( xtr, textStatus, errorThrown ) {
        var msg = JSON.parse(xtr.responseText).message;
        notifyError(msg);
    });

}
//________________________________________

function resetFormAdd() {
    jQuery("#{{ params.component }}_name_group").removeClass("has-error");
    var vendor_input = jQuery("form#{{params.component}}_add_form input#{{params.component}}_name");
    vnendor_input.val('');
    vendor_input.attr("placeholder", "");

    jQuery('form#{{params.component}}_add_form select#ibox_release').val('Select a release');
}
//________________________________________

// ============= /ADD FUNCTIONS
// ==== EDIT Functions ==================


function onSubmitEdit() {

    if ( ! validateForm() ) {
        return false;
    }

    var vendor = collectFormEditFields();
    console.log("Collected edit fields");
    console.log(vendor);

    var aj = jQuery.ajax({
        type: "PUT",
        url: "{{ config.API_URL_PREFIX }}/{{ params.component }}/"+vendor.vendorid,
        data: JSON.stringify({ 'component' : vendor}),
        contentType: "application/json",
        dataType: "json",
        async: true,

        beforeSend: function() {},
    });

    aj.done(function( data, textStatus, obj ) {
        var msg = "Node {{ params.caption }} " + vendor.name + " has been updated!";
        notifySuccess(msg, function() { backToParentPage(component)});

    });

    aj.always();

    aj.fail(function( xtr, textStatus, errorThrown ) {
        var msg = JSON.parse(xtr.responseText).message;
        notifyError(msg);
    });
}
//________________________________________

function populateFormEdit(data) {

    var vendor = data.component;
    console.log("Fetched vendor");
    console.log(vendor);
    jQuery("form#{{ params.component }}_edit_form input#{{ params.component }}id").val(psu.id);
    jQuery("form#{{ params.component }}_edit_form input#{{ params.component }}_name").val(psu.name);
}
//________________________________________

// ==== /EDIT Functions ==================

initGrid();
initForm();
