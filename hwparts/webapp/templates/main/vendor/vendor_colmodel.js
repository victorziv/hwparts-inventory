colModel: [
    {
        label: 'VendorID',
        name: 'id',
        index: 'id',
        align: 'center',
        sortable: false,
        hidden: true,
        search: false
    },
    {
        label: 'normal_name',
        name: 'name',
        index: 'name',
        align: 'center',
        sortable: false,
        search: false,
        hidden: true
    },
    {
        label: 'Name',
        name: 'display_name',
        index: 'display_name',
        align: 'center',
        sortable: true,
        search: true
    },
    {
        label: 'Ware',
        name: 'ware',
        index: 'ware',
        align: 'center',
        sortable: true,
        search: true
    },
    {
        label: 'Headquarters',
        name: 'hq',
        index: 'hq',
        align: 'center',
        sortable: true,
        search: true
    },
    {
        label: 'Notes',
        name: 'notes',
        index: 'notes',
        align: 'center',
        sortable: false,
        search: false
    }
],
