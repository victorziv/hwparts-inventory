"use strict";
{% include "common.js" %}
// ______________________________

var model_grid = jQuery("#node_model_grid");
var grid_pager = "#node_model_grid_pager";

model_grid.jqGrid({

    mtype: "GET",
    cache : false,
    async: true,
    datatype: "json",

    url: "{{ config.API_URL_PREFIX }}/node/model/",

    jsonReader : { 
        root: "model",
        page: "currpage",
        total: "totalpages",
        records: "totalrecords",
        repeatitems: false,
        id: "id"
    },

    {% include "main/node/model/model_colmodel.js" %}

    rowNum:10,
    rowList:[10,20,30,40,50,100],
    pager: grid_pager,
    viewrecords: true,
    height: '100%',
    autowidth: true,
    shrinkToFit: true,
    gridview: true,
    autoResize: true,

    guiStyle: "bootstrap",
    iconSet: "fontAwesome",
    caption: "Node Models",
    rownumbers: false,
    sortname: "release",
    sortorder: "desc"
});
//________________________________________

model_grid.jqGrid('navGrid',grid_pager,

    {edit:false,add:false,del:false,search:false, view:false, refresh:true },

        {}, {}, {}, {}, {}

).navSeparatorAdd(grid_pager);
//________________________________________


var options = {
        caption: "",
        buttonicon: "fa fa-trash",
        title: "Delete Model",
        position: 'first',
        onClickButton: showDeleteConfirmation
} 

model_grid.jqGrid('navGrid', grid_pager).jqGrid('navButtonAdd',grid_pager, options);

var options = {
        caption: "",
        buttonicon: "fa fa-edit",
        title: "Edit Model",
        position: 'first',
        onClickButton: showFormEdit
} 

model_grid.jqGrid('navGrid', grid_pager).jqGrid('navButtonAdd',grid_pager, options);

var options = {
        caption: "",
        buttonicon: "fa fa-plus",
        title: "Add Model",
        position: 'first',
        onClickButton: showFormAdd
} 

model_grid.jqGrid('navGrid', grid_pager).jqGrid('navButtonAdd',grid_pager, options);

// ===== Functions =================

function backToParentPage() {
    var url = '/view/node/model';
    window.location.href = url;
}
//________________________________________

function initButtons() {
    jQuery("#btn_node_model_add").on('click', showFormAdd);
    jQuery("#btn_node_model_edit").on('click', showFormEdit);
    jQuery("#btn_node_model_delete").on('click', showDeleteConfirmation);
}
//________________________________________

function initManagement() {
    initFormAdd();
    initFormEdit();
    initButtons();
}

// ==== /ADD Functions ==================

function initFormAdd() {

    jQuery("#btn_cancel_model_add").on('click', function() {
        resetAddForm();
        backToParentPage();
    });

    jQuery("#btn_submit_model_add").on('click', onSubmitAdd);
    jQuery('form#node_model_add_form').on('keydown', function(evt) {
        if (evt.which === 13) {
            evt.preventDefault();
            onSubmitAdd();
            return false;
        }
    });
}
//________________________________________

function collectFormFieldsAdd() {
    var model = {};
    var model_inputs = jQuery('form#node_model_add_form .model_field'); 

    model_inputs.each(function() { 
        model[this.name] = jQuery(this).val(); 
    }); 

    return model;
}
//________________________________________

function onSubmitAdd() {

    var node_model_input = jQuery("#node_model");
    var node_model = node_model_input.val();
    if (! node_model) {
        jQuery("#node_model_group").addClass("has-error");
        node_model_input.focus();
        node_model_input.attr("placeholder", "Node model is required");
        return false;
    }

    var model = collectFormFieldsAdd();

    var aj = jQuery.ajax({
        type: "POST",
        url: "{{ config.API_URL_PREFIX }}/node/model/",
        data: JSON.stringify({ 'model' : model}),
        contentType: "application/json",
        dataType: "json",
        async: true,

        beforeSend: function() {},
    });

    aj.done(function( data, textStatus, obj ) {
        msg = "Node model " + node_model + " has been added successfully!";
        notifySuccess(msg, function() {backToParentPage(); resetAddForm()}); 

    });

    aj.always();

    aj.fail(function( xtr, textStatus, errorThrown ) {
        var msg = JSON.parse(xtr.responseText).message;
        notifyError(msg);
    });
}
//________________________________________

function resetAddForm() {

    jQuery("#node_model_group").removeClass("has-error");
    var node_model_input = jQuery("form#node_model_add_form input#node_model");
    node_model_input.val('');
    node_model_input.attr("placeholder", "");

    jQuery('form#node_model_add_form select#vendor_select').val('Select a vendor');
    jQuery('form#node_model_add_form #notes').val('');
}
//________________________________________

function onShowFormAdd() {
    populateVendorsList();
    jQuery('form#node_model_add_form input#node_model').focus();
}
//________________________________________


function populateVendorsList() {
    var aj = jQuery.ajax({
        type: "GET",
        url: "{{ config.API_URL_PREFIX }}/vendor/names",
        dataType: "json",
        data: {'ware' : 'server'},
        async: true,

        beforeSend: function() {},
    });

    aj.done(function( data, textStatus, obj ) {

            var vendor_select = jQuery('form#node_model_add_form select#vendor_select');
            var vendors = []
            for (i=0;i<data.length;i++) {
                vendors.push('<option value="'+data[i].name+'">'+data[i].display_name+'</option>');
            }
            vendor_select.empty();
            vendor_select.append(vendors.join(''));
    });

    aj.always(function() {});

    aj.fail(function( xtr, textStatus, errorThrown ) {
        var msg = JSON.parse(xtr.responseText).message;
        notifyError(msg);
    });
}

//________________________________________

function showFormAdd() {
    var url = '/view/node/model/form/add';
    window.location.href = url;
}

// ==== /ADD Functions ==================

// ==== EDIT Functions ==================

function collectFormEditFields() {
    var model = {};
    var model_inputs = jQuery('form#model_edit_form .model_field_editable'); 

    model_inputs.each(function() { 
        model[this.name] = jQuery(this).val(); 
    }); 

    if (jQuery("form#model_edit_form #hwstatus_check").is(':checked')) {
        model.hwstatus_check = true;    
    } else {
        model.hwstatus_check = false;    
    }

    return model;
}
//________________________________________

function initFormEdit() {

    jQuery("#btn_submit_model_edit").on('click', onSubmitEdit);
    jQuery("#btn_cancel_model_edit").on('click', backToParentPage);

    jQuery("form#model_edit_form").on('keydown', function(evt) {
        if (evt.which === 13) {
            evt.preventDefault();
            onSubmitEdit();
            return false;
        }
    });
}
//________________________________________


function onShowFormEdit() {
    jQuery('form#node_model_edit_form input#model_version').focus();
    var getURL = "{{ config.API_URL_PREFIX }}/node/model/{{ params.selectedid }}";

    var aj = jQuery.ajax({
        type: "GET",
        url: getURL,
        dataType: "json",
        async: true,

        beforeSend: function() {},
    });

    aj.done(function( data, textStatus, obj ) {
        populateFormEdit(data);
    });

    aj.always(function() {});

    aj.fail(function( xtr, textStatus, errorThrown ) {
        var msg = JSON.parse(xtr.responseText).message;
        notifyError(msg);
    });
}
//________________________________________

function onSubmitEdit() {

    var node_model_input = jQuery("#node_model");
    var node_model = node_model_input.val();
    if (! node_model) {
        jQuery("#node_model_group").addClass("has-error");
        node_model_input.focus();
        node_model_input.attr("placeholder", "Node model is required");
        return false;
    }

    var model = collectFormEditFields();

    var aj = jQuery.ajax({
        type: "PUT",
        url: "{{ config.API_URL_PREFIX }}/node/model/"+model.modelid,
        data: JSON.stringify({ 'model' : model}),
        contentType: "application/json",
        dataType: "json",
        async: true,

        beforeSend: function() {},
    });

    aj.done(function( data, textStatus, obj ) {
        var msg = "Node model " + model.node_model + " has been updated!";
        notifySuccess(msg, backToParentPage);

    });

    aj.always();

    aj.fail(function( xtr, textStatus, errorThrown ) {
        var msg = JSON.parse(xtr.responseText).message;
        notifyError(msg);
    });
}
//________________________________________

function populateFormEdit(data) {

    var model = data.model;
    console.log(model);
    jQuery("form#model_edit_form input#modelid").val(model.id);
    jQuery("form#model_edit_form input#node_model").val(model.model);
    jQuery("form#model_edit_form input#vendor").val(model.vendor);
    jQuery("form#model_edit_form textarea#notes").val(model.notes);
}

//________________________________________

function showFormEdit() {
    var rowid = model_grid.jqGrid ('getGridParam', 'selrow');
    if (! rowid) {
        notifyWarning("No {{ params.caption }} was selected for edit");
        return false;
    }
    var url = '/view/node/model/form/edit/' + rowid;
    window.location.href = url;
}
// ==== /EDIT Functions ==================

// ==== DELETE Functions ==================

function showDeleteConfirmation() {
    var rowid = model_grid.jqGrid ('getGridParam', 'selrow');
    if (! rowid) {
        notifyWarning("No {{ params.caption }} was selected for deletion");
        return false;
    }

    var model = model_grid.jqGrid('getRowData', rowid).model;
    confirmAction("Deleting node model <B>" + model +"</B>", onSubmitDelete, rowid);
}
//________________________________________

function onSubmitDelete(modelid) {

    var model = model_grid.jqGrid('getRowData', modelid).model;

    var aj = jQuery.ajax({
        type: "DELETE",
        url: "{{ config.API_URL_PREFIX }}/node/model/" + modelid,
        contentType: "application/json",
        dataType: "json",
        async: true,

        beforeSend: function() {},
    });

    aj.done(function( data, textStatus, obj ) {
        var msg = "Node model " + model + " has been deleted!";
        notifySuccess(msg, backToParentPage);

    });

    aj.always();

    aj.fail(function( xtr, textStatus, errorThrown ) {
        var msg = JSON.parse(xtr.responseText).message;
        notifyError(msg);
    });
}
// ==== /DELETE Functions ==================

initGridResize("#model_grid_container", "#model_grid", grid_pager);
initManagement();
