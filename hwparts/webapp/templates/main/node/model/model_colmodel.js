colModel: [
    {
        label: 'NodeID',
        name: 'id',
        index: 'id',
        align: 'center',
        sortable: false,
        hidden: true,
        search: false
    },
    {
        label: 'Model',
        name: 'model',
        index: 'model',
        align: 'center',
        sortable: true,
        search: true
    },
    {
        label: 'Vendor',
        name: 'vendor',
        index: 'vendor',
        align: 'center',
        sortable: true,
        search: true
    },
    {
        label: 'Notes',
        name: 'notes',
        index: 'notes',
        align: 'left',
        sortable: false,
        search: false
    }
],
