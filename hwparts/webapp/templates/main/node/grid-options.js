    mtype: "GET",
    cache : false,
    async: true,
    datatype: "json",

    url: "{{ config.API_URL_PREFIX }}/node/{{ params.component}}/",

    jsonReader : { 
        root: "component",
        page: "currpage",
        total: "totalpages",
        records: "totalrecords",
        repeatitems: false,
        id: "id"
    },
    rowNum:10,
    rowList:[10,20,30,40,50,100],
    pager: "#node_{{params.component}}_grid_pager",

    viewrecords: true,
    height: '100%',
    autowidth: true,
    shrinkToFit: true,
    gridview: true,
    autoResize: true,

    guiStyle: "bootstrap",
    iconSet: "fontAwesome",
    caption: "Node {{ params.caption }}",
    rownumbers: false,
    sortname: "release",
    sortorder: "desc",
