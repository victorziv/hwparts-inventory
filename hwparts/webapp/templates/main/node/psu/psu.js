"use strict";
{% include "common.js" %}
// ______________________________

var component = "{{ params.component }}";
var formadd = "{{ params.component}}_add_form";

var node_psu_grid = jQuery("#node_{{ params.component }}_grid");

node_psu_grid.jqGrid({
    {% include "main/node/grid-options.js" %}
    {% include "main/node/psu/psu_colmodel.js" %}
});
//________________________________________

{% include "main/node/buttons-grid.html" %}

// ============ FUNCTIONS ==================

function initForm() {
    initFormAdd();
    initFormEdit();
    initButtons();
}
//________________________________________

function validateForm() {

    var pn_input = jQuery("form#{{ params.component }}_{{ params.operation }}_form input#{{ params.component }}_pn");
    var pn = pn_input.val();
    if (! pn) {
        jQuery("#{{ params.component }}_pn_group").addClass("has-error");
        pn_input.focus();
        pn_input.attr("placeholder", "{{ params.caption }} P/N is required");
        return false;
    }

    var version_input = jQuery("form#{{ params.component }}_{{ params.operation }}_form input#{{ params.component }}_version");
    var version = version_input.val();
    if (! version) {
        jQuery("#{{ params.component }}_version_group").addClass("has-error");
        version_input.focus();
        version_input.attr("placeholder", "Version is required");
        return false;
    }

    return true;
}
//________________________________________

// ============ ADD FUNCTIONS ==================

function onSubmitAdd() {

    if ( ! validateForm() ) {
        return false;
    }

    var psu = collectFormAddFields();
    var aj = jQuery.ajax({
        type: "POST",
        url: "{{ config.API_URL_PREFIX }}/node/{{params.component}}",
        data: JSON.stringify({ 'component' : psu}),
        contentType: "application/json",
        dataType: "json",
        async: true,

        beforeSend: function() {},
    });

    aj.done(function( data, textStatus, obj ) {
        var msg = "{{ params.caption }} " + psu.psu_version + " has been added successfully!";
        notifySuccess(msg, function() { backToParentPage(component), resetFormAdd()});
    });

    aj.always();

    aj.fail(function( xtr, textStatus, errorThrown ) {
        var msg = JSON.parse(xtr.responseText).message;
        notifyError(msg);
    });

}
//________________________________________

function resetFormAdd() {
    jQuery("#{{ params.component }}_version_group").removeClass("has-error");
    var psu_version_input = jQuery("form#{{params.component}}_add_form input#{{params.component}}_version");
    psu_version_input.val('');
    psu_version_input.attr("placeholder", "");

    jQuery('form#{{params.component}}_add_form select#ibox_release').val('Select a release');
}
//________________________________________

// ============= /ADD FUNCTIONS
// ==== EDIT Functions ==================


function onSubmitEdit() {

    if ( ! validateForm() ) {
        return false;
    }

    var psu = collectFormEditFields();
    console.log("Collected edit fields");
    console.log(psu);

    var aj = jQuery.ajax({
        type: "PUT",
        url: "{{ config.API_URL_PREFIX }}/node/{{ params.component }}/"+psu.psuid,
        data: JSON.stringify({ 'component' : psu}),
        contentType: "application/json",
        dataType: "json",
        async: true,

        beforeSend: function() {},
    });

    aj.done(function( data, textStatus, obj ) {
        var msg = "Node {{ params.caption }} " + psu.psu_version + " has been updated!";
        notifySuccess(msg, function() { backToParentPage(component)});

    });

    aj.always();

    aj.fail(function( xtr, textStatus, errorThrown ) {
        var msg = JSON.parse(xtr.responseText).message;
        notifyError(msg);
    });
}
//________________________________________

function populateFormEdit(data) {

    var psu = data.component;
    console.log("Fetched psu");
    console.log(psu);
    jQuery("form#{{ params.component }}_edit_form input#{{ params.component }}id").val(psu.id);
    jQuery("form#{{ params.component }}_edit_form input#{{ params.component }}_power").val(psu.power);
    jQuery("form#{{ params.component }}_edit_form input#{{ params.component }}_pn").val(psu.pn);
    jQuery("form#{{ params.component }}_edit_form input#{{ params.component }}_version").val(psu.version);
    jQuery("form#{{ params.component }}_edit_form input#ibox_release").val(psu.release);
    jQuery("form#{{ params.component }}_edit_form input#node_model").val(psu.node_model);
    jQuery("form#{{ params.component }}_edit_form input#firmware_bin").val(psu.firmware_bin);
    jQuery("form#{{ params.component }}_edit_form input#firmware_exe").val(psu.firmware_exe);
    jQuery("form#{{ params.component }}_edit_form textarea#notes").val(psu.notes);

    if (psu.hwstatus_check) {
        jQuery("form#{{ params.component }}_edit_form #hwstatus_check").iCheck('check');
    } else {
        jQuery("form#{{ params.component }}_edit_form #hwstatus_check").iCheck('uncheck');
    }
    jQuery('form#{{params.component}}_add_form input#{{params.component}}_pn').focus();
}
//________________________________________

// ==== /EDIT Functions ==================

initGrid();
initForm();
