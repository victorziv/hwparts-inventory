"use strict";
{% include "common.js" %}
// ______________________________

var node_lifecont_grid = jQuery("#node_lifecont_grid");

node_lifecont_grid.jqGrid({

    mtype: "GET",
    cache : false,
    async: true,
    datatype: "json",

    url: "{{ config.API_URL_PREFIX }}/node/lifecont/",

    jsonReader : { 
        root: "lifecont",
        page: "currpage",
        total: "totalpages",
        records: "totalrecords",
        repeatitems: false,
        id: "id"
    },

    {% include "main/node/lifecont/lifecont_colmodel.js" %}

    rowNum:10,
    rowList:[10,20,30,40,50,100],
    pager: '#node_lifecont_grid_pager',
    viewrecords: true,
    height: '100%',
    autowidth: true,
    shrinkToFit: true,
    gridview: true,
    autoResize: true,

    guiStyle: "bootstrap",
    iconSet: "fontAwesome",
    caption: "Node Lifecycle Controller",
    rownumbers: false,
    sortname: "release",
    sortorder: "desc"
});
//________________________________________

node_lifecont_grid.jqGrid('navGrid','#node_lifecont_grid_pager',

    {edit:false,add:false,del:false,search:false, view:false, refresh:true },

        {}, {}, {}, {}, {}

).navSeparatorAdd("#node_lifecont_grid_pager");
//________________________________________


var options = {
        caption: "",
        buttonicon: "fa fa-trash",
        title: "Delete Lifecycle Controller",
        position: 'first',
        onClickButton: showDeleteConfirmation
} 

node_lifecont_grid.jqGrid('navGrid', '#node_lifecont_grid_pager').jqGrid('navButtonAdd',"#node_lifecont_grid_pager", options);

var options = {
        caption: "",
        buttonicon: "fa fa-edit",
        title: "Edit Lifecycle Controller",
        position: 'first',
        onClickButton: showFormEdit
} 

node_lifecont_grid.jqGrid('navGrid', '#node_lifecont_grid_pager').jqGrid('navButtonAdd',"#node_lifecont_grid_pager", options);

var options = {
        caption: "",
        buttonicon: "fa fa-plus",
        title: "Add Lifecycle Controller",
        position: 'first',
        onClickButton: showFormAdd
} 

node_lifecont_grid.jqGrid('navGrid', '#node_lifecont_grid_pager').jqGrid('navButtonAdd',"#node_lifecont_grid_pager", options);
//________________________________________


// ==== ADD functions ====

function initFormAdd() {

    jQuery("#btn_cancel_lifecont_add").on('click', backToParentPage);
    jQuery("#btn_submit_lifecont_add").on('click', onSubmitAdd);
    jQuery("form#lifecont_add_form").on('keydown', function(evt) {
        if (evt.which === 13) {
            evt.preventDefault();
            onSubmitAdd();
            return false;
        }
    });

    jQuery('#lifecont_add_form select#ibox_release').on( 'change', function() { onIboxReleaseChange('lifecont_add_form')});
}
//________________________________________

function collectFormAddFields() {
    var lifecont = {};
    var lifecont_inputs = jQuery('form#lifecont_add_form .lifecont_field'); 

    lifecont_inputs.each(function() { 
        lifecont[this.name] = jQuery(this).val(); 
    }); 

    if (jQuery("form#lifecont_add_form #hwstatus_check").is(':checked')) {
        lifecont.hwstatus_check = true;    
    } else {
        lifecont.hwstatus_check = false;    
    }

    return lifecont;
}
//________________________________________

function onSubmitAdd(grid) {

    if ( ! validateForm() ) {
        return false;
    }

    var lifecont = collectFormAddFields();
    console.log("Collect lifecont add inputs");
    console.log(lifecont);

    var aj = jQuery.ajax({
        type: "POST",
        url: "{{ config.API_URL_PREFIX }}/node/lifecont",
        data: JSON.stringify({ 'lifecycle_controller' : lifecont}),
        contentType: "application/json",
        dataType: "json",
        async: true,

        beforeSend: function() {},
    });

    aj.done(function( data, textStatus, obj ) {
        var msg = "Lifecycle Controller " + lifecont.lifecont_version + " has been added successfully!";
        notifySuccess(msg, function() { resetFormAdd(); backToParentPage()});
    });

    aj.always(function() {});

    aj.fail(function( xtr, textStatus, errorThrown ) {
        var msg = JSON.parse(xtr.responseText).message;
        notifyError(msg);
    });
}
//________________________________________

function resetFormAdd() {
    var lifecont = {};
    var lifecont_inputs = jQuery('form#lifecont_add_form .lifecont_field'); 
    lifecont_inputs.each(function() { 
        lifecont[this.name] = jQuery(this).val(''); 
    }); 

    jQuery("#lifecont_version_group").removeClass("has-error");
    var lifecont_version_input = jQuery("form#lifecont_add_form input#lifecont_version");
    lifecont_version_input.val('');
    lifecont_version_input.attr("placeholder", "");

    jQuery('form#lifecont_add_form select#ibox_release').val('Select a release');
}
//________________________________________


function onShowFormAdd() {
    populateIboxReleaseList();
    jQuery('form#lifecont_add_form input#lifecont_version').focus();
}
//________________________________________

function populateIboxReleaseList() {

    var aj = jQuery.ajax({
        type: "GET",
        url: "{{ config.API_URL_PREFIX }}/ibox/release/",
        dataType: "json",
        async: true,

        beforeSend: function() {},
    });

    aj.done(function( data, textStatus, obj ) {

            var ibox_release_select = jQuery('form#lifecont_add_form select#ibox_release');
            var releases = []
            for (i=0;i<data.length;i++) {
                releases.push('<option value="'+data[i] + '">'+data[i] + '</option>');
            }
            ibox_release_select.empty();
            ibox_release_select.append(releases.join(''));
            jQuery("#lifecont_add_form select#ibox_release").change();
            return false; 
    });

    aj.always(function() {});

    aj.fail(function( xtr, textStatus, errorThrown ) {
        var msg = JSON.parse(xtr.responseText).message;
        notifyError(msg);
    });
}

//________________________________________

function showFormAdd() {
    var url = '/view/node/lifecont/form/add';
    window.location.href = url;
}

// ==== /ADD functions ====

// ==== EDIT functions ====

var selected_rowid = null;

// ====================================

function collectFormEditFields() {
    var lifecont = {};
    var lifecont_inputs = jQuery('form#lifecont_edit_form .lifecont_field_editable'); 

    lifecont_inputs.each(function() { 
        lifecont[this.name] = jQuery(this).val(); 
    }); 

    if (jQuery("form#lifecont_edit_form #hwstatus_check").is(':checked')) {
        lifecont.hwstatus_check = true;    
    } else {
        lifecont.hwstatus_check = false;    
    }

    return lifecont;
}
//________________________________________

function initFormEdit() {

    jQuery("#btn_submit_lifecont_edit").on('click', onSubmitEdit);
    jQuery("#btn_cancel_lifecont_edit").on('click', backToParentPage);

    jQuery("form#lifecont_edit_form").on('keydown', function(evt) {
        if (evt.which === 13) {
            evt.preventDefault();
            onSubmitEdit();
            return false;
        }
    });
}
//________________________________________


function onShowFormEdit() {
    jQuery('form#lifecont_edit_form input#lifecont_version').focus();
    selected_rowid = "{{ params.selected_lifecontid }}";
    console.log("Selected ID: " + selected_rowid);

    var aj = jQuery.ajax({
        type: "GET",
        url: "{{ config.API_URL_PREFIX }}/node/lifecont/" + selected_rowid,
        dataType: "json",
        async: true,

        beforeSend: function() {},
    });

    aj.done(function( data, textStatus, obj ) {
        populateFormEdit(data);
    });

    aj.always(function() {});

    aj.fail(function( xtr, textStatus, errorThrown ) {
        var msg = JSON.parse(xtr.responseText).message;
        notifyError(msg);
    });
}
//________________________________________

function onSubmitEdit() {

    if ( ! validateForm() ) {
        return false;
    }

    var lifecont = collectFormEditFields();
    console.log("Collected edit fields");
    console.log(lifecont);

    var aj = jQuery.ajax({
        type: "PUT",
        url: "{{ config.API_URL_PREFIX }}/node/lifecont/"+lifecont.lifecontid,
        data: JSON.stringify({ 'lifecont' : lifecont}),
        contentType: "application/json",
        dataType: "json",
        async: true,

        beforeSend: function() {},
    });

    aj.done(function( data, textStatus, obj ) {
        var msg = "iDRAC " + lifecont.lifecont_version + " has been updated!";
        notifySuccess(msg, backToParentPage);

    });

    aj.always(resetFormAdd);

    aj.fail(function( xtr, textStatus, errorThrown ) {
        var msg = JSON.parse(xtr.responseText).message;
        notifyError(msg);
    });
}
//________________________________________

function populateFormEdit(data) {

    var lifecont = data.lifecont;
    console.log("Got LC");
    console.log(lifecont);
    jQuery("form#lifecont_edit_form input#lifecontid").val(lifecont.id);
    jQuery("form#lifecont_edit_form input#lifecont_version").val(lifecont.version);
    jQuery("form#lifecont_edit_form input#lifecont_build").val(lifecont.build);
    jQuery("form#lifecont_edit_form input#ibox_release").val(lifecont.release);
    jQuery("form#lifecont_edit_form input#node_model").val(lifecont.node_model);
    jQuery("form#lifecont_edit_form input#firmware_bin").val(lifecont.firmware_bin);
    jQuery("form#lifecont_edit_form input#firmware_exe").val(lifecont.firmware_exe);
    jQuery("form#lifecont_edit_form textarea#notes").val(lifecont.notes);

    if (lifecont.hwstatus_check) {
        jQuery("form#lifecont_edit_form #hwstatus_check").iCheck('check');
    } else {
        jQuery("form#lifecont_edit_form #hwstatus_check").iCheck('uncheck');
    }

}

//________________________________________

function showFormEdit() {
    var lifecontid = node_lifecont_grid.jqGrid ('getGridParam', 'selrow');
    if (! lifecontid) {
        notifyWarning("Select a Lifecycle Controller to edit");
        return false;
    }
    var url = '/view/node/lifecont/form/edit/' + lifecontid;
    window.location.href = url;
}
// ==== /EDIT functions ====

// ==== DELETE functions ====

function showDeleteConfirmation() {

    var lifecontid = node_lifecont_grid.jqGrid ('getGridParam', 'selrow');
    if (! lifecontid) {
        notifyWarning("No Lifecycle Controller was selected for deletion");
        return false;
    }

    var version = node_lifecont_grid.jqGrid('getRowData', lifecontid).version;

    confirmAction("Deleting Lifecycle Controller <B>"+version+"</B>", onSubmitDelete, lifecontid);
}
//________________________________________

function onSubmitDelete(lifecontid) {

    var version = node_lifecont_grid.jqGrid('getRowData', lifecontid).version;

    var aj = jQuery.ajax({
        type: "DELETE",
        url: "{{ config.API_URL_PREFIX }}/node/lifecont/" + lifecontid,
        contentType: "application/json",
        dataType: "json",
        async: true,

        beforeSend: function() {},
    });

    aj.done(function( data, textStatus, obj ) {
        var msg = "Lifecycle Controller " + version + " has been deleted!";
        notifySuccess(msg, backToParentPage);

    });

    aj.always(resetFormAdd);

    aj.fail(function( xtr, textStatus, errorThrown ) {
        var msg = JSON.parse(xtr.responseText).message;
        notifyError(msg);
    });
}
// ==== /DELETE functions ====
function backToParentPage() {
    var url = '/view/node/lifecont'
    window.location.href = url;
    node_lifecont_grid.trigger("reloadGrid");
}
//________________________________________

function initButtons() {
    jQuery("#btn_node_lifecont_add").on('click', showFormAdd);
    jQuery("#btn_node_lifecont_edit").on('click', showFormEdit);
    jQuery("#btn_node_lifecont_delete").on('click', showDeleteConfirmation);
}
//________________________________________

function initLifecont() {
    initFormAdd();
    initFormEdit();
    initButtons();
}
//________________________________________

function validateForm() {
    var lifecont_version_input = jQuery("#lifecont_version");
    var lifecont_version = lifecont_version_input.val();
    if (! lifecont_version) {
        jQuery("#lifecont_version_group").addClass("has-error");
        lifecont_version_input.focus();
        lifecont_version_input.attr("placeholder", "iDRAC version is required");
        return false;
    }

    return true;

}
//________________________________________

initGridResize("#node_lifecont_grid_container", "#node_lifecont_grid", "#node_lifecont_grid_pager");
initLifecont();
