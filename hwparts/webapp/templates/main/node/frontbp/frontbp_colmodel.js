colModel: [
    {
        label: 'Front BP ID',
        name: 'id',
        index: 'id',
        align: 'center',
        sortable: false,
        hidden: true,
        search: false
    },
    {
        label: 'Front BP P/N',
        name: 'pn',
        index: 'pn',
        align: 'center',
        sortable: true,
        search: true
    },
    {
        label: 'Revision',
        name: 'revision',
        index: 'revision',
        align: 'center',
        sortable: true,
        search: true
    },
    {
        label: 'Mix Allowed P/N',
        name: 'mixpn',
        index: 'mixpn',
        align: 'center',
        sortable: true,
        search: true
    },
    {
        label: 'Version',
        name: 'version',
        index: 'version',
        align: 'center',
        sortable: true,
        search: true
    },
    {
        label: 'Release',
        name: 'release',
        index: 'release',
        align: 'center',
        sortable: true,
        search: true
    },
    {
        label: 'Node Model',
        name: 'node_model',
        index: 'node_model',
        align: 'center',
        sortable: true,
        search: true
    },
    {
        label: 'FW BIN',
        name: 'firmware_bin',
        index: 'firmware_bin',
        align: 'center',
        sortable: false,
        search: false,
        formatter: fwBinUrlFormatter 
    },
    {
        label: 'FW EXE',
        name: 'firmware_exe',
        index: 'firmware_exe',
        align: 'center',
        sortable: false,
        search: false,
        formatter: fwExeUrlFormatter 
    },
    {
        label: 'HWStatus Check',
        name: 'hwstatus_check',
        index: 'hwstatus_check',
        align: 'center',
        sortable: true,
        search: false,
        formatter: function(cellvalue, options,rowObject) {
            if ( cellvalue === null || typeof cellvalue === 'undefined' ) {
                return "Unknown";
            } else if ( cellvalue ) {
                return 'Yes';
            } else  if ( ! cellvalue ) {
                return "No";
            }
        }
    },
    {
        label: 'Notes',
        name: 'notes',
        index: 'notes',
        align: 'left',
        sortable: false,
        search: false,
        cellattr: function (rowId, tv, rawObject, cm, rdata) { 
            return 'style="white-space: normal;"';
        }

    }
],
