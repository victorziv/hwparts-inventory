"use strict";
{% include "common.js" %}
// ______________________________

var component = "{{ params.component }}";
var formadd = "{{ params.component}}_add_form";

var node_frontbp_grid = jQuery("#node_{{ params.component }}_grid");

node_frontbp_grid.jqGrid({
    {% include "main/node/grid-options.js" %}
    {% include "main/node/frontbp/frontbp_colmodel.js" %}
});
//________________________________________

{% include "main/node/buttons-grid.html" %}

// ============ FUNCTIONS ==================

function initForm() {
    initFormAdd();
    initFormEdit();
    initButtons();
}
//________________________________________

function validateForm() {

    var pn_input = jQuery("form#{{ params.component }}_{{ params.operation }}_form input#{{ params.component }}_pn");
    var pn = pn_input.val();
    if (! pn) {
        jQuery("#{{ params.component }}_pn_group").addClass("has-error");
        pn_input.focus();
        pn_input.attr("placeholder", "{{ params.caption }} P/N is required");
        return false;
    }

    var version_input = jQuery("form#{{ params.component }}_{{ params.operation }}_form input#{{ params.component }}_version");
    var version = version_input.val();
    if (! version) {
        jQuery("#{{ params.component }}_version_group").addClass("has-error");
        version_input.focus();
        version_input.attr("placeholder", "Version is required");
        return false;
    }

    return true;
}
//________________________________________

// ============ ADD FUNCTIONS ==================

function onSubmitAdd() {

    if ( ! validateForm() ) {
        return false;
    }

    var frontbp = collectFormAddFields();
    var aj = jQuery.ajax({
        type: "POST",
        url: "{{ config.API_URL_PREFIX }}/node/{{params.component}}",
        data: JSON.stringify({ 'component' : frontbp}),
        contentType: "application/json",
        dataType: "json",
        async: true,

        beforeSend: function() {},
    });

    aj.done(function( data, textStatus, obj ) {
        msg = "{{ params.caption }} " + frontbp.frontbp_pn + " has been added successfully!";
        notifySuccess(msg, function() { backToParentPage(component), resetFormAdd()});
    });

    aj.always();

    aj.fail(function( xtr, textStatus, errorThrown ) {
        var msg = JSON.parse(xtr.responseText).message;
        notifyError(msg);
    });

}
//________________________________________

function resetFormAdd() {
    jQuery("#{{ params.component }}_version_group").removeClass("has-error");
    var frontbp_version_input = jQuery("form#{{params.component}}_add_form input#{{params.component}}_version");
    frontbp_version_input.val('');
    frontbp_version_input.attr("placeholder", "");

    jQuery('form#modal_node_{{params.component}}_add_form select#ibox_release').val('Select a release');
}
//________________________________________

// ============= /ADD FUNCTIONS
// ==== EDIT Functions ==================


function onSubmitEdit() {

    if ( ! validateForm() ) {
        return false;
    }

    var frontbp = collectFormEditFields();
    console.log("Collected edit fields");
    console.log(frontbp);

    var aj = jQuery.ajax({
        type: "PUT",
        url: "{{ config.API_URL_PREFIX }}/node/{{ params.component }}/"+frontbp.frontbpid,
        data: JSON.stringify({ 'component' : frontbp}),
        contentType: "application/json",
        dataType: "json",
        async: true,

        beforeSend: function() {},
    });

    aj.done(function( data, textStatus, obj ) {
        var msg = "Node {{ params.caption }} " + frontbp.frontbp_version + " has been updated!";
        notifySuccess(msg, function() { backToParentPage(component)});

    });

    aj.always();

    aj.fail(function( xtr, textStatus, errorThrown ) {
        var msg = JSON.parse(xtr.responseText).message;
        notifyError(msg);
    });
}
//________________________________________

function populateFormEdit(data) {

    var frontbp = data.component;
    console.log("Fetched frontbp");
    console.log(frontbp);
    jQuery("form#frontbp_edit_form input#frontbpid").val(frontbp.id);
    jQuery("form#frontbp_edit_form input#frontbp_pn").val(frontbp.pn);
    jQuery("form#frontbp_edit_form input#frontbp_mixpn").val(frontbp.mixpn);
    jQuery("form#frontbp_edit_form input#frontbp_version").val(frontbp.version);
    jQuery("form#frontbp_edit_form input#ibox_release").val(frontbp.release);
    jQuery("form#frontbp_edit_form input#node_model").val(frontbp.node_model);
    jQuery("form#frontbp_edit_form input#firmware_bin").val(frontbp.firmware_bin);
    jQuery("form#frontbp_edit_form input#firmware_exe").val(frontbp.firmware_exe);
    jQuery("form#frontbp_edit_form textarea#notes").val(frontbp.notes);

    if (frontbp.hwstatus_check) {
        jQuery("form#frontbp_edit_form #hwstatus_check").iCheck('check');
    } else {
        jQuery("form#frontbp_edit_form #hwstatus_check").iCheck('uncheck');
    }
}
//________________________________________

// ==== /EDIT Functions ==================

initGrid();
initForm();
