"use strict";
{% include "common.js" %}
// ______________________________

var component = "{{ params.component }}";
var formadd = "{{ params.component}}_{{ params.operation }}_form";

var node_perc_grid = jQuery("#node_{{ params.component }}_grid");

node_perc_grid.jqGrid({
    {% include "main/node/grid-options.js" %}
    {% include "main/node/perc/perc_colmodel.js" %}
});
//________________________________________

{% include "main/node/buttons-grid.html" %}

// ============ FUNCTIONS ==================

function initForm() {
    initFormAdd();
    initFormEdit();
    initButtons();
}
//________________________________________

function initFormRevisions() {
    jQuery("form#{{params.component}}_{{ params.operation }}_form select#perc_revision").select2({
        tags: true,
        tokenSeparators: [',', ' '],
        ajax: {
            url: "{{ config.API_URL_PREFIX }}/node/{{params.component}}/revisions/",
        }
    });
}
//________________________________________

function initFormEditRevisions() {
    initFormRevisions();
    getSupportedRevisionsPerPERCModel();
}
//________________________________________

function getSupportedRevisionsPerPERCModel() {
    var aj = jQuery.ajax({
        type: "GET",
        url: "{{ config.API_URL_PREFIX }}/node/{{params.component}}/revisions/supported/{{params.selectedid}}",
        contentType: "application/json",
        dataType: "json",
        async: true,

        beforeSend: function() {},
    });

    aj.done(function( data, textStatus, obj ) {
        populateRevisionSelect(data.results);
    });

    aj.always();

    aj.fail(function( xtr, textStatus, errorThrown ) {
        var msg = JSON.parse(xtr.responseText).message;
        notifyError(msg);
    });
}
// ___________________________________

function populateRevisionSelect(revisions) {
    var sel  = jQuery("form#{{params.component}}_edit_form select#perc_revision");
    for(i=0;i<revisions.length;i++) { 
        var data = revisions[i];
        var op = new Option(data.text, data.id, true, true);
        sel.append(op);
    }
    sel.trigger('change');
}
// ___________________________________

function validateForm() {

    var version_input = jQuery("#{{params.component}}_version");
    var version = version_input.val();
    if (! version) {
        jQuery("#{{ params.component }}_version_group").addClass("has-error");
        version_input.focus();
        version_input.attr("placeholder", "Version is required");
        return false;
    }

    var model_input = jQuery("#{{params.component}}_model");
    var model = model_input.val();
    if (! model) {
        jQuery("#{{ params.component }}_model_group").addClass("has-error");
        model_input.focus();
        model_input.attr("placeholder", "{{ params.caption }} model is required");
        return false;
    }

    return true;
}

// ============ ADD FUNCTIONS ==================

function collectSupportedRevisions() {
    var selected_revisions = [];
    var revs = jQuery('form#{{params.component}}_{{ params.operation }}_form #perc_revision').select2('data');
    for (i=0;i<revs.length;i++) {
        selected_revisions.push(revs[i].text);
    }

    return selected_revisions;
}
//________________________________________

function onSubmitAdd() {

    if ( ! validateForm() ) {
        return false;
    }

    var perc = collectFormAddFields();
    perc.supported_revisions = collectSupportedRevisions();
    perc.revision_mix_allowed = figureRevisionMixAllowed(perc);

    var aj = jQuery.ajax({
        type: "POST",
        url: "{{ config.API_URL_PREFIX }}/node/{{params.component}}",
        data: JSON.stringify({ 'component' : perc}),
        contentType: "application/json",
        dataType: "json",
        async: true,

        beforeSend: function() {},
    });

    aj.done(function( data, textStatus, obj ) {
        msg = "{{ params.caption }} " + perc.perc_model + " has been added successfully!";
        notifySuccess(msg, function() { backToParentPage(component), resetFormAdd()});
    });

    aj.always();

    aj.fail(function( xtr, textStatus, errorThrown ) {
        var msg = JSON.parse(xtr.responseText).message;
        notifyError(msg);
    });

}
//________________________________________

function resetFormAdd() {
    jQuery("#{{ params.component }}_version_group").removeClass("has-error");
    var perc_version_input = jQuery("form#{{params.component}}_{{ params.operation }}_form input#{{params.component}}_version");
    perc_version_input.val('');
    perc_version_input.attr("placeholder", "");

    jQuery("#{{ params.component }}_model_group").removeClass("has-error");
    var perc_version_input = jQuery("form#{{params.component}}_{{ params.operation }}_form input#{{params.component}}_model");
    perc_version_input.val('');
    perc_version_input.attr("placeholder", "");

    jQuery('form#modal_node_{{params.component}}_add_form select#ibox_release').val('Select a release');
}
//________________________________________

// ============= /ADD FUNCTIONS
// ==== EDIT Functions ==================

function figureRevisionMixAllowed(perc) {

    console.log("In figure revsion mix allowed");
    console.log(perc);

    if (typeof perc.supported_revisions === 'undefined') {
        return null;
    }

    var revision_mix_allowed_check = jQuery("form#{{params.component}}_{{ params.operation }}_form #revision_mix_allowed").is(':checked');
    if (perc.supported_revisions.length > 0 && revision_mix_allowed_check) {
        return true;
    } else {
        return false;
    }
}
//________________________________________

function onSubmitEdit() {

    if ( ! validateForm() ) {
        return false;
    }

    var perc = collectFormEditFields();
    perc.supported_revisions = collectSupportedRevisions();
    perc.revision_mix_allowed = figureRevisionMixAllowed(perc);
    console.log("On submit edit");
    console.log(perc);

    var aj = jQuery.ajax({
        type: "PUT",
        url: "{{ config.API_URL_PREFIX }}/node/{{ params.component }}/"+perc.percid,
        data: JSON.stringify({ 'component' : perc}),
        contentType: "application/json",
        dataType: "json",
        async: true,

        beforeSend: function() {},
    });

    aj.done(function( data, textStatus, obj ) {
        var msg = "Node {{ params.caption }} " + perc.perc_version + " has been updated!";
        notifySuccess(msg, function() { backToParentPage(component)});

    });

    aj.always();

    aj.fail(function( xtr, textStatus, errorThrown ) {
        var msg = JSON.parse(xtr.responseText).message;
        notifyError(msg);
    });
}
//________________________________________

function populateFormEdit(data) {

    var perc = data.component;
    jQuery("form#perc_edit_form input#percid").val(perc.id);
    jQuery("form#perc_edit_form input#perc_version").val(perc.version);
    jQuery("form#perc_edit_form input#perc_model").val(perc.model);
    jQuery("form#perc_edit_form input#ibox_release").val(perc.release);
    jQuery("form#perc_edit_form input#node_model").val(perc.node_model);
    jQuery("form#perc_edit_form input#firmware_bin").val(perc.firmware_bin);
    jQuery("form#perc_edit_form input#firmware_exe").val(perc.firmware_exe);
    jQuery("form#perc_edit_form textarea#notes").val(perc.notes);

    if (perc.hwstatus_check) {
        jQuery("form#{{ params.component }}_{{ params.operation }}_form #hwstatus_check").iCheck('check');
    } else {
        jQuery("form#{{ params.component }}_{{ params.operation }}_form #hwstatus_check").iCheck('uncheck');
    }

    console.log("mix allowed: " + perc.revision_mix_allowed)
    if (perc.revision_mix_allowed) {
        jQuery("form#{{ params.component }}_{{ params.operation }}_form #revision_mix_allowed").iCheck('check');
    } else {
        jQuery("form#{{ params.component }}_{{ params.operation }}_form #revision_mix_allowed").iCheck('uncheck');
    }
}
//________________________________________

// ==== /EDIT Functions ==================

initGrid();
initForm();
