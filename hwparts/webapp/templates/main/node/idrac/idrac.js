"use strict";
{% include "common.js" %}
// ______________________________

var component = "{{ params.component }}";
var formadd = "{{ params.component}}_add_form";

var node_idrac_grid = jQuery("#node_{{ params.component }}_grid");

node_idrac_grid.jqGrid({
    {% include "main/node/grid-options.js" %}
    {% include "main/node/idrac/idrac_colmodel.js" %}
});
//________________________________________

{% include "main/node/buttons-grid.html" %}

// ============ FUNCTIONS ==================

function initForm() {
    initFormAdd();
    initFormEdit();
    initButtons();
}
//________________________________________

function validateForm() {
    var idrac_version_input = jQuery("#{{ params.component }}_version");
    var idrac_version = idrac_version_input.val();
    if (! idrac_version) {
        jQuery("#{{ params.component }}_version_group").addClass("has-error");
        idrac_version_input.focus();
        idrac_version_input.attr("placeholder", "Version is required");
        return false;
    }

    return true;

}
//________________________________________


// ==== ADD Functions ==================

function onSubmitAdd() {

    if ( ! validateForm() ) {
        return false;
    }

    var idrac = collectFormAddFields();

    var aj = jQuery.ajax({
        type: "POST",
        url: "{{ config.API_URL_PREFIX }}/node/{{ params.component }}",
        data: JSON.stringify({ 'component' : idrac}),
        contentType: "application/json",
        dataType: "json",
        async: true,

        beforeSend: function() {},
    });

    aj.done(function( data, textStatus, obj ) {
        var msg = "{{ params.caption }} " + idrac.idrac_version + " has been added successfully!";
        notifySuccess(msg, function() { backToParentPage(component), resetFormAdd()});
    
    });

    aj.always();

    aj.fail(function( xtr, textStatus, errorThrown ) {
        var msg = JSON.parse(xtr.responseText).message;
        notifyError(msg);
    });
}
//________________________________________

function resetFormAdd() {
    var idrac = {};
    var idrac_inputs = jQuery('form#{{ params.component }}_add_form .{{ params.component }}_field'); 
    idrac_inputs.each(function() { 
        idrac[this.name] = jQuery(this).val(''); 
    }); 

    jQuery("#{{ params.component }}_version_group").removeClass("has-error");
    var idrac_version_input = jQuery("form#{{ params.component }}_add_form input#{{ params.component }}_version");
    idrac_version_input.val('');
    idrac_version_input.attr("placeholder", "");

    jQuery('form#{{ params.component }}_add_form select#ibox_release').val('Select a release');
}
//________________________________________

// ==== /ADD Functions ==================

// ==== EDIT Functions ==================

function onSubmitEdit() {

    if ( ! validateForm() ) {
        return false;
    }

    var idrac = collectFormEditFields();
    console.log("Collected edit fields");
    console.log(idrac);

    var aj = jQuery.ajax({
        type: "PUT",
        url: "{{ config.API_URL_PREFIX }}/node/{{ params.component }}/"+idrac.idracid,
        data: JSON.stringify({ 'component' : idrac}),
        contentType: "application/json",
        dataType: "json",
        async: true,

        beforeSend: function() {},
    });

    aj.done(function( data, textStatus, obj ) {
        var msg = "Node {{ params.caption }} " + idrac.idrac_version + " has been updated!";
        notifySuccess(msg, function() { backToParentPage(component)});

    });

    aj.always();

    aj.fail(function( xtr, textStatus, errorThrown ) {
        var msg = JSON.parse(xtr.responseText).message;
        notifyError(msg);
    });
}
//________________________________________

function populateFormEdit(data) {

    var idrac = data.component;
    console.log("Got {{ params.caption }}");
    console.log(idrac);
    jQuery("form#{{ params.component }}_edit_form input#{{ params.component }}id").val(idrac.id);
    jQuery("form#{{ params.component }}_edit_form input#{{ params.component }}_version").val(idrac.version);
    jQuery("form#{{ params.component }}_edit_form input#{{ params.component }}_build").val(idrac.build);
    jQuery("form#{{ params.component }}_edit_form input#ibox_release").val(idrac.release);
    jQuery("form#{{ params.component }}_edit_form input#node_model").val(idrac.node_model);
    jQuery("form#{{ params.component }}_edit_form input#firmware_bin").val(idrac.firmware_bin);
    jQuery("form#{{ params.component }}_edit_form input#firmware_exe").val(idrac.firmware_exe);
    jQuery("form#{{ params.component }}_edit_form textarea#notes").val(idrac.notes);

    if (idrac.hwstatus_check) {
        jQuery("form#{{ params.component }}_edit_form #hwstatus_check").iCheck('check');
    } else {
        jQuery("form#{{ params.component }}_edit_form #hwstatus_check").iCheck('uncheck');
    }

}
// ==== /EDIT Functions ==================

initGrid();
initForm();
