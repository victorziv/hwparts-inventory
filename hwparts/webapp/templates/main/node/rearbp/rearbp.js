"use strict";
{% include "common.js" %}
// ______________________________

var component = "{{ params.component }}";
var formadd = "{{ params.component}}_add_form";

var node_rearbp_grid = jQuery("#node_{{ params.component }}_grid");

node_rearbp_grid.jqGrid({
    {% include "main/node/grid-options.js" %}
    {% include "main/node/rearbp/rearbp_colmodel.js" %}
});
//________________________________________

{% include "main/node/buttons-grid.html" %}

// ============ FUNCTIONS ==================

function initForm() {
    initFormAdd();
    initFormEdit();
    initButtons();
}
//________________________________________

function validateForm() {

    var pn_input = jQuery("form#{{ params.component }}_{{ params.operation }}_form input#{{ params.component }}_pn");
    var pn = pn_input.val();
    if (! pn) {
        jQuery("#{{ params.component }}_pn_group").addClass("has-error");
        pn_input.focus();
        pn_input.attr("placeholder", "{{ params.caption }} P/N is required");
        return false;
    }

    var version_input = jQuery("form#{{ params.component }}_{{ params.operation }}_form input#{{ params.component }}_version");
    var version = version_input.val();
    if (! version) {
        jQuery("#{{ params.component }}_version_group").addClass("has-error");
        version_input.focus();
        version_input.attr("placeholder", "Version is required");
        return false;
    }

    return true;
}
//________________________________________

// ============ ADD FUNCTIONS ==================

function onSubmitAdd() {

    if ( ! validateForm() ) {
        return false;
    }

    var rearbp = collectFormAddFields();
    var aj = jQuery.ajax({
        type: "POST",
        url: "{{ config.API_URL_PREFIX }}/node/{{params.component}}",
        data: JSON.stringify({ 'component' : rearbp}),
        contentType: "application/json",
        dataType: "json",
        async: true,

        beforeSend: function() {},
    });

    aj.done(function( data, textStatus, obj ) {
        msg = "{{ params.caption }} " + rearbp.rearbp_pn + " has been added successfully!";
        notifySuccess(msg, function() { backToParentPage(component), resetFormAdd()});
    });

    aj.always();

    aj.fail(function( xtr, textStatus, errorThrown ) {
        var msg = JSON.parse(xtr.responseText).message;
        notifyError(msg);
    });

}
//________________________________________

function resetFormAdd() {
    jQuery("#{{ params.component }}_version_group").removeClass("has-error");
    var rearbp_version_input = jQuery("form#{{params.component}}_add_form input#{{params.component}}_version");
    rearbp_version_input.val('');
    rearbp_version_input.attr("placeholder", "");

    jQuery('form#modal_node_{{params.component}}_add_form select#ibox_release').val('Select a release');
}
//________________________________________

// ============= /ADD FUNCTIONS
// ==== EDIT Functions ==================


function onSubmitEdit() {

    if ( ! validateForm() ) {
        return false;
    }

    var rearbp = collectFormEditFields();
    console.log("Collected edit fields");
    console.log(rearbp);

    var aj = jQuery.ajax({
        type: "PUT",
        url: "{{ config.API_URL_PREFIX }}/node/{{ params.component }}/"+rearbp.rearbpid,
        data: JSON.stringify({ 'component' : rearbp}),
        contentType: "application/json",
        dataType: "json",
        async: true,

        beforeSend: function() {},
    });

    aj.done(function( data, textStatus, obj ) {
        var msg = "Node {{ params.caption }} " + rearbp.rearbp_version + " has been updated!";
        notifySuccess(msg, function() { backToParentPage(component)});

    });

    aj.always();

    aj.fail(function( xtr, textStatus, errorThrown ) {
        var msg = JSON.parse(xtr.responseText).message;
        notifyError(msg);
    });
}
//________________________________________

function populateFormEdit(data) {

    var rearbp = data.component;
    console.log("Fetched rearbp");
    console.log(rearbp);
    jQuery("form#rearbp_edit_form input#rearbpid").val(rearbp.id);
    jQuery("form#rearbp_edit_form input#rearbp_pn").val(rearbp.pn);
    jQuery("form#rearbp_edit_form input#rearbp_mixpn").val(rearbp.mixpn);
    jQuery("form#rearbp_edit_form input#rearbp_version").val(rearbp.version);
    jQuery("form#rearbp_edit_form input#ibox_release").val(rearbp.release);
    jQuery("form#rearbp_edit_form input#node_model").val(rearbp.node_model);
    jQuery("form#rearbp_edit_form input#firmware_bin").val(rearbp.firmware_bin);
    jQuery("form#rearbp_edit_form input#firmware_exe").val(rearbp.firmware_exe);
    jQuery("form#rearbp_edit_form textarea#notes").val(rearbp.notes);

    if (rearbp.hwstatus_check) {
        jQuery("form#rearbp_edit_form #hwstatus_check").iCheck('check');
    } else {
        jQuery("form#rearbp_edit_form #hwstatus_check").iCheck('uncheck');
    }
    jQuery('form#{{params.component}}_add_form input#{{params.component}}_pn').focus();
}
//________________________________________

// ==== /EDIT Functions ==================

initGrid();
initForm();
