"use strict";
{% include "common.js" %}
// ______________________________

var component = "{{ params.component }}";
var node_bios_grid = jQuery("#node_{{ params.component }}_grid");

node_bios_grid.jqGrid({

    mtype: "GET",
    cache : false,
    async: true,
    datatype: "json",

    url: "{{ config.API_URL_PREFIX }}/node/bios/",

    jsonReader : { 
        root: "bios",
        page: "currpage",
        total: "totalpages",
        records: "totalrecords",
        repeatitems: false,
        id: "id"
    },

    {% include "main/node/bios/bios_colmodel.js" %}

    rowNum:10,
    rowList:[10,20,30,40,50,100],
    pager: '#node_bios_grid_pager',
    viewrecords: true,
    height: '100%',
    autowidth: true,
    shrinkToFit: true,
    gridview: true,
    autoResize: true,

    guiStyle: "bootstrap",
    iconSet: "fontAwesome",
    caption: "Node BIOS",
    rownumbers: false,
    sortname: "release",
    sortorder: "desc"
});
//________________________________________

node_bios_grid.jqGrid('navGrid','#node_bios_grid_pager',

    {edit:false,add:false,del:false,search:false, view:false, refresh:true },

        {}, {}, {}, {}, {}

).navSeparatorAdd("#node_bios_grid_pager");
//________________________________________

{% include "main/node/buttons-grid.html" %}

// ==== FUNCTIONS =======================

function initButtons() {
    jQuery("#btn_node_bios_add").on('click', showFormAdd);
    jQuery("#btn_node_bios_edit").on('click', showFormEdit);
    jQuery("#btn_node_bios_delete").on('click', showDeleteConfirmation);
}

//________________________________________

function initBios() {
    initBiosAdd();
    initFormEdit();
    initButtons();
}
//________________________________________

function validateForm() {
    var bios_version_input = jQuery("#bios_version");
    var bios_version = bios_version_input.val();
    if (! bios_version) {
        jQuery("#bios_version_group").addClass("has-error");
        bios_version_input.focus();
        bios_version_input.attr("placeholder", "Version is required");
        return false;
    }

    return true;

}
//________________________________________

// ==== ADD functions ===

function initBiosAdd() {

    jQuery("#btn_cancel_bios_add").on('click', function() {
        resetFormAdd();
        backToParentPage(component);
    });

    jQuery("#btn_submit_bios_add").on('click', onSubmitAdd);
    jQuery('form#bios_add_form').on('keydown', function(evt) {
        if (evt.which === 13) {
            evt.preventDefault();
            onSubmitAdd();
            return false;
        }
    });

    jQuery('form#bios_add_form select#ibox_release').on('change', function() { onIboxReleaseChange('bios_add_form')});
}
//________________________________________

function collectBiosAddFormFields() {
    var bios = {};
    var bios_inputs = jQuery('form#bios_add_form .bios_field'); 

    bios_inputs.each(function() { 
        bios[this.name] = jQuery(this).val(); 
    }); 

    if (jQuery("form#bios_add_form #hwstatus_check").is(':checked')) {
        bios.hwstatus_check = true;    
    } else {
        bios.hwstatus_check = false;    
    }

    return bios;
}
//________________________________________

function onSubmitAdd(grid) {
    if ( ! validateForm() ) {
        return false;
    }

    var bios = collectBiosAddFormFields();

    var aj = jQuery.ajax({
        type: "POST",
        url: "{{ config.API_URL_PREFIX }}/node/bios",
        data: JSON.stringify({ 'bios' : bios}),
        contentType: "application/json",
        dataType: "json",
        async: true,

        beforeSend: function() {},
    });

    aj.done(function( data, textStatus, obj ) {
        msg = "{{ params.caption }} " + bios.bios_version + " has been added successfully!";
        notifySuccess(msg, function() { backToParentPage(component), resetFormAdd()});
    });

    aj.always();

    aj.fail(function( xtr, textStatus, errorThrown ) {
        var msg = JSON.parse(xtr.responseText).message;
        notifyError(msg);
    });
}
//________________________________________

function resetFormAdd() {
    jQuery("#bios_version_group").removeClass("has-error");
    var bios_version_input = jQuery("form#bios_add_form input#bios_version");
    bios_version_input.val('');
    bios_version_input.attr("placeholder", "");

    jQuery('form#modal_node_bios_add_form select#ibox_release').val('Select a release');
}
//________________________________________

function onShowFormAdd() {
    populateIboxReleaseList('bios_add_form');
    jQuery('form#bios_add_form input#bios_version').focus();
}
//________________________________________

function showFormAdd() {
    var formAddURL = '/view/node/bios/form/add';
    window.location.href = formAddURL;
}
// ==== /ADD Functions ==================

// ==== EDIT Functions ==================

function collectFormEditFields() {
    var bios = {};
    var bios_inputs = jQuery('form#bios_edit_form .bios_field_editable'); 

    bios_inputs.each(function() { 
        bios[this.name] = jQuery(this).val(); 
    }); 

    if (jQuery("form#bios_edit_form #hwstatus_check").is(':checked')) {
        bios.hwstatus_check = true;    
    } else {
        bios.hwstatus_check = false;    
    }

    return bios;
}
//________________________________________

function initFormEdit() {

    jQuery("#btn_submit_bios_edit").on('click', onSubmitEdit);
    jQuery("#btn_cancel_bios_edit").on('click', function() { backToParentPage(component)});

    jQuery("form#bios_edit_form").on('keydown', function(evt) {
        if (evt.which === 13) {
            evt.preventDefault();
            onSubmitEdit();
            return false;
        }
    });
}
//________________________________________


function onShowFormEdit() {
    jQuery('form#bios_edit_form input#bios_version').focus();
    var getBiosURL = "{{ config.API_URL_PREFIX }}/node/bios/";
    {% if params %}
        getBiosURL += "{{ params.selected_biosid }}";
    {% endif %}

    var aj = jQuery.ajax({
        type: "GET",
        url: getBiosURL,
        dataType: "json",
        async: true,

        beforeSend: function() {},
    });

    aj.done(function( data, textStatus, obj ) {
        populateFormEdit(data);
    });

    aj.always(function() {});

    aj.fail(function( xtr, textStatus, errorThrown ) {
        var msg = JSON.parse(xtr.responseText).message;
        notifyError(msg);
    });
}
//________________________________________

function onSubmitEdit() {

    if ( ! validateForm() ) {
        return false;
    }

    var bios = collectFormEditFields();
    console.log("Collected edit fields");
    console.log(bios);

    var aj = jQuery.ajax({
        type: "PUT",
        url: "{{ config.API_URL_PREFIX }}/node/bios/"+bios.biosid,
        data: JSON.stringify({ 'bios' : bios}),
        contentType: "application/json",
        dataType: "json",
        async: true,

        beforeSend: function() {},
    });

    aj.done(function( data, textStatus, obj ) {
        var msg = "Node BIOS " + bios.bios_version + " has been updated!";
        notifySuccess(msg, function() { backToParentPage(component)});

    });

    aj.always();

    aj.fail(function( xtr, textStatus, errorThrown ) {
        var msg = JSON.parse(xtr.responseText).message;
        notifyError(msg);
    });
}
//________________________________________

function populateFormEdit(data) {

    var bios = data.bios;
    console.log("Got BIOS");
    console.log(bios);
    jQuery("form#bios_edit_form input#biosid").val(bios.id);
    jQuery("form#bios_edit_form input#bios_version").val(bios.version);
    jQuery("form#bios_edit_form input#cpu_microarch").val(bios.cpu_microarch);
    jQuery("form#bios_edit_form input#ibox_release").val(bios.release);
    jQuery("form#bios_edit_form input#node_model").val(bios.node_model);
    jQuery("form#bios_edit_form input#firmware_bin").val(bios.firmware_bin);
    jQuery("form#bios_edit_form input#firmware_exe").val(bios.firmware_exe);
    jQuery("form#bios_edit_form textarea#notes").val(bios.notes);

    if (bios.hwstatus_check) {
        jQuery("form#bios_edit_form #hwstatus_check").iCheck('check');
    } else {
        jQuery("form#bios_edit_form #hwstatus_check").iCheck('uncheck');
    }

}

//________________________________________

function showFormEdit() {
    var rowid = node_bios_grid.jqGrid ('getGridParam', 'selrow');
    if (! rowid) {
        notifyWarning("No BIOS was selected for edit");
        return false;
    }
    var url = '/view/node/bios/form/edit/' + rowid;
    window.location.href = url;
}
// ==== /EDIT Functions ==================

// ==== DELETE Functions ==================

function onSubmitDelete(biosid) {

    var version = node_bios_grid.jqGrid('getRowData', biosid).version;

    var aj = jQuery.ajax({
        type: "DELETE",
        url: "{{ config.API_URL_PREFIX }}/node/bios/" + biosid,
        contentType: "application/json",
        dataType: "json",
        async: true,

        beforeSend: function() {},
    });

    aj.done(function( data, textStatus, obj ) {
        var msg = "Node {{ params.caption }} " + version + " has been deleted!";
        notifySuccess(msg, function() { backToParentPage(component)});

    });

    aj.always(function() {});

    aj.fail(function( xtr, textStatus, errorThrown ) {
        var msg = JSON.parse(xtr.responseText).message;
        notifyError(msg);
    });
}
// ==== /DELETE Functions ==================

initGridResize("#node_bios_grid_container", "#node_bios_grid", "#node_bios_grid_pager");
initBios();
