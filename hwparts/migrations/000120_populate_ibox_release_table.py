#!/usr/bin/env python


def populate_ibox_releases(conn):
    cursor = conn.cursor()

    releases = [
        ('1.4', 'R720'),
        ('1.5', 'R720'),
        ('2.0', 'R720'),
        ('2.0.2.1', 'R730XD'),
        ('2.2', 'R730XD'),
        ('3.0', 'R730XD')
    ]

    query = """
        INSERT INTO ibox_sw_release
            ( release, nodemodelid )
        VALUES
            (
                %s,
                (SELECT id FROM node_model WHERE model = %s)
            )
        """

    for release in releases:
        cursor.execute(query, release)

    conn.commit()
# _____________________________


def upgrade(conn, **kwargs):
    populate_ibox_releases(conn)
# _______________________________


def downgrade(conn):
    pass
