#!/usr/bin/env python


def create_table_fcadapter_gbic(conn):
    cursor = conn.cursor()

    query = """
        CREATE TABLE IF NOT EXISTS fcadapter_gbic(
            id serial PRIMARY KEY,
            model VARCHAR(32) UNIQUE,
            revision VARCHAR(32),
            port_speed VARCHAR(16),
            vendorid INTEGER REFERENCES vendor(id)
        );
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def drop_table_fcadapter_gbic(conn):
    cursor = conn.cursor()

    query = """
        DROP TABLE IF EXISTS fcadapter_gbic;
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def upgrade(conn, **kwargs):
    create_table_fcadapter_gbic(conn)
# _______________________________


def downgrade(conn):
    drop_table_fcadapter_gbic(conn)
