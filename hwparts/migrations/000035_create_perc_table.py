#!/usr/bin/env python


def create_table_node_perc(conn):
    cursor = conn.cursor()

    query = """
        CREATE TABLE IF NOT EXISTS node_perc (
            id serial PRIMARY KEY,
            releaseid INTEGER REFERENCES ibox_sw_release(id),
            model VARCHAR(32),
            version VARCHAR(32),
            firmware_bin VARCHAR(256),
            firmware_exe VARCHAR(256),
            hwstatus_check BOOLEAN,
            revision_mix_allowed BOOLEAN,
            notes TEXT,
            UNIQUE(releaseid, version)
        );
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def drop_table_node_perc(conn):
    cursor = conn.cursor()

    query = """
        DROP TABLE IF EXISTS node_perc;
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def upgrade(conn, **kwargs):
    create_table_node_perc(conn)
# _______________________________


def downgrade(conn):
    drop_table_node_perc(conn)
