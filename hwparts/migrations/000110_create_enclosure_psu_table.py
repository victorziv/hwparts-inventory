#!/usr/bin/env python


def create_table_enclosure_psu(conn):
    cursor = conn.cursor()

    query = """
        CREATE TABLE IF NOT EXISTS enclosure_psu (
            id serial PRIMARY KEY,
            model VARCHAR(32) UNIQUE,
            revision VARCHAR(32),
            vendorid INTEGER REFERENCES vendor(id),
            enclosure_modelid INTEGER REFERENCES enclosure(id)
        );
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def drop_table_enclosure_psu(conn):
    cursor = conn.cursor()

    query = """
        DROP TABLE IF EXISTS enclosure_psu;
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def upgrade(conn, **kwargs):
    create_table_enclosure_psu(conn)
# _______________________________


def downgrade(conn):
    drop_table_enclosure_psu(conn)
