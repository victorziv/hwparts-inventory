#!/usr/bin/env python


def create_table_node_bios(conn):
    cursor = conn.cursor()

    query = """
        CREATE TABLE IF NOT EXISTS node_bios (
            id serial PRIMARY KEY,
            releaseid INTEGER REFERENCES ibox_sw_release(id),
            version VARCHAR(32),
            firmware_bin VARCHAR(256),
            firmware_exe VARCHAR(256),
            hwstatus_check BOOLEAN,
            cpu_microarch VARCHAR(32),
            notes TEXT,
            UNIQUE(releaseid, version)

        );
    """
    params = ()

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def drop_table_node_bios(conn):
    cursor = conn.cursor()

    query = """
        DROP TABLE IF EXISTS node_bios;
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def upgrade(conn, **kwargs):
    create_table_node_bios(conn)
# _______________________________


def downgrade(conn):
    drop_table_node_bios(conn)
