#!/usr/bin/env python


def create_table_ware(conn):
    cursor = conn.cursor()

    query = """
        CREATE TABLE IF NOT EXISTS ware (
            id serial PRIMARY KEY,
            name VARCHAR(128) UNIQUE,
            display_name VARCHAR(128)
        );
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def populate_table_ware(conn):
    cursor = conn.cursor()

    wares = [
        ('server', 'Server'),
        ('ssd', 'SSD'),
        ('hdd', 'HDD'),
        ('enclosure', 'Enclosure'),
        ('fc_card', 'FC Card'),
        ('ib_card', 'IB Card')
    ]

    query = """
        INSERT INTO ware
            ( name, display_name )
        VALUES
            ( %s, %s)
        """

    for ware in wares:
        cursor.execute(query, ware)

    conn.commit()
# _____________________________


def create_table_vendor_ware_map(conn):
    cursor = conn.cursor()

    query = """
        CREATE TABLE IF NOT EXISTS vendor_ware_map (
            id serial PRIMARY KEY,
            vendorid INTEGER NOT NULL REFERENCES vendor(id),
            wareid INTEGER NOT NULL REFERENCES ware(id),
            UNIQUE(vendorid, wareid)
        );
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def init_vendor(conn):
    cursor = conn.cursor()

    query = """
        INSERT INTO vendor
            ( name, display_name )
        VALUES
            ( %s, %s)
        """

    params = ('dell', 'Dell Inc.')
    cursor.execute(query, params)

    conn.commit()
# _____________________________


def init_node_server_vendor(conn):
    cursor = conn.cursor()

    query = """
        INSERT INTO vendor_ware_map
            ( vendorid, wareid )
        VALUES (
            (SELECT id FROM vendor WHERE name = %s),
            (SELECT id FROM ware WHERE name = %s)
        );
        """

    params = ('dell', 'server')
    cursor.execute(query, params)

    conn.commit()
# _____________________________


def drop_table_vendor_ware_map(conn):
    cursor = conn.cursor()

    query = """
        DROP TABLE IF EXISTS vendor_ware_map;
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def drop_table_ware(conn):
    cursor = conn.cursor()

    query = """
        DROP TABLE IF EXISTS ware;
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def upgrade(conn, **kwargs):
    create_table_ware(conn)
    create_table_vendor_ware_map(conn)
    populate_table_ware(conn)
    init_vendor(conn)
    init_node_server_vendor(conn)
# _______________________________


def downgrade(conn):
    drop_table_vendor_ware_map(conn)
    drop_table_ware(conn)
