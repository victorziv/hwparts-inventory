def create_table_perc_revision_map(conn):
    cursor = conn.cursor()

    query = """
        CREATE TABLE IF NOT EXISTS node_perc_revision_map (
            percid INTEGER REFERENCES node_perc(id),
            revisionid INTEGER REFERENCES node_perc_revision(id),
            UNIQUE(percid, revisionid)
        );
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def drop_table_perc_revision_map(conn):
    cursor = conn.cursor()

    query = """
        DROP TABLE IF EXISTS node_perc_revision_map;
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def upgrade(conn, **kwargs):
    create_table_perc_revision_map(conn)
# _______________________________


def downgrade(conn):
    drop_table_perc_revision_map(conn)
