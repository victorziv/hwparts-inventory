#!/usr/bin/env python


def create_table_node_lifecycle_controller(conn):
    cursor = conn.cursor()

    query = """
        CREATE TABLE IF NOT EXISTS node_lifecycle_controller (
            id serial PRIMARY KEY,
            releaseid INTEGER REFERENCES ibox_sw_release(id),
            version VARCHAR(32),
            firmware_bin VARCHAR(256),
            firmware_exe VARCHAR(256),
            hwstatus_check BOOLEAN,
            notes TEXT,
            UNIQUE(releaseid, version)
        );
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def drop_table_node_lifecycle_controller(conn):
    cursor = conn.cursor()

    query = """
        DROP TABLE IF EXISTS node_lifecycle_controller;
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def upgrade(conn, **kwargs):
    create_table_node_lifecycle_controller(conn)
# _______________________________


def downgrade(conn):
    drop_table_node_lifecycle_controller(conn)
