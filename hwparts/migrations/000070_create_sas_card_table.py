#!/usr/bin/env python


def create_table_sascard(conn):
    cursor = conn.cursor()

    query = """
        CREATE TABLE IF NOT EXISTS sascard (
            id serial PRIMARY KEY,
            model VARCHAR(32) UNIQUE,
            revision VARCHAR(32),
            vendorid INTEGER REFERENCES vendor(id)
        );
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def drop_table_sascard(conn):
    cursor = conn.cursor()

    query = """
        DROP TABLE IF EXISTS sascard;
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def upgrade(conn, **kwargs):
    create_table_sascard(conn)
# _______________________________


def downgrade(conn):
    drop_table_sascard(conn)
