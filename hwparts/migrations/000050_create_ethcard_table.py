#!/usr/bin/env python


def create_table_ethcard(conn):
    cursor = conn.cursor()

    query = """
        CREATE TABLE IF NOT EXISTS ethernet_card (
            id serial PRIMARY KEY,
            model VARCHAR(32) UNIQUE,
            revision VARCHAR(32),
            vendorid INTEGER REFERENCES vendor(id)
        );
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def drop_table_ethcard(conn):
    cursor = conn.cursor()

    query = """
        DROP TABLE IF EXISTS ethernet_card;
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def upgrade(conn, **kwargs):
    create_table_ethcard(conn)
# _______________________________


def downgrade(conn):
    drop_table_ethcard(conn)
