#!/usr/bin/env python


def create_table_node_perc_revision(conn):
    cursor = conn.cursor()

    query = """
        CREATE TABLE IF NOT EXISTS node_perc_revision(
            id serial PRIMARY KEY,
            official_revision VARCHAR(32) UNIQUE,
            hwstatus_revision VARCHAR(32),
            notes TEXT
        );
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def comment_on_hwstatus_revision(conn):
    cursor = conn.cursor()

    query = """
        COMMENT ON COLUMN node_perc_revision.hwstatus_revision IS %s;
    """
    comment = "HWStatus revisions are deprecated; should be retired in a short while"

    params = (comment,)

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def insert_perc_revisions(conn):
    cursor = conn.cursor()

    revisions = [
        ('07H4CNA03', 'A03'),
        ('07H4CNA04', 'A04'),
        ('07H4CNA05', 'A05'),
        ('7H4CNA06', 'A06'),
        ('07H4CNA07', 'A07')
    ]

    query = """
        INSERT INTO node_perc_revision
            ( official_revision, hwstatus_revision)
        VALUES ( %s, %s);
    """

    for revision in revisions:
        cursor.execute(query, revision)
        conn.commit()
# _____________________________


def drop_table_node_perc_revision(conn):
    cursor = conn.cursor()

    query = """
        DROP TABLE IF EXISTS node_perc_revision;
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def upgrade(conn, **kwargs):
    create_table_node_perc_revision(conn)
    insert_perc_revisions(conn)
    comment_on_hwstatus_revision(conn)
# _______________________________


def downgrade(conn):
    drop_table_node_perc_revision(conn)
