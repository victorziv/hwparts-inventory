#!/usr/bin/env python


def create_table_ibox_sw_release(conn):
    cursor = conn.cursor()

    query = """
        CREATE TABLE IF NOT EXISTS ibox_sw_release (
            id serial PRIMARY KEY,
            release VARCHAR(32) UNIQUE,
            nodemodelid INTEGER REFERENCES node_model(id)
        );
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def drop_table_ibox_sw_release(conn):
    cursor = conn.cursor()

    query = """
        DROP TABLE IF EXISTS ibox_sw_release;
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def upgrade(conn, **kwargs):
    create_table_ibox_sw_release(conn)
# _______________________________


def downgrade(conn):
    drop_table_ibox_sw_release(conn)
