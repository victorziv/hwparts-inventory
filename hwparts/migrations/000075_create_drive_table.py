#!/usr/bin/env python


def create_table_drive(conn):
    cursor = conn.cursor()

    query = """
        CREATE TABLE IF NOT EXISTS drive (
            id serial PRIMARY KEY,
            model VARCHAR(32) UNIQUE,
            revision VARCHAR(32),
            vendorid INTEGER REFERENCES vendor(id),
            is_ssd BOOLEAN,
            protocol VARCHAR(16)
        );
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def drop_table_drive(conn):
    cursor = conn.cursor()

    query = """
        DROP TABLE IF EXISTS drive;
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def upgrade(conn, **kwargs):
    create_table_drive(conn)
# _______________________________


def downgrade(conn):
    drop_table_drive(conn)
