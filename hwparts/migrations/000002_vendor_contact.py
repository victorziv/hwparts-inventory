#!/usr/bin/env python


def create_table_contact(conn):
    cursor = conn.cursor()

    query = """
        CREATE TABLE IF NOT EXISTS contact (
            id serial PRIMARY KEY,
            name VARCHAR(128) NOT NULL,
            email VARCHAR(128) NOT NULL UNIQUE,
            phone_desk VARCHAR(32),
            phone_mobile VARCHAR(32),
            notes TEXT
        );
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def create_table_vendor(conn):
    cursor = conn.cursor()

    query = """
        CREATE TABLE IF NOT EXISTS vendor (
            id serial PRIMARY KEY,
            name VARCHAR(128) UNIQUE,
            display_name VARCHAR(128),
            hq VARCHAR(128),
            notes TEXT
        );
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def create_table_vendor_contact_map(conn):
    cursor = conn.cursor()

    query = """
        CREATE TABLE IF NOT EXISTS vendor_contact_map (
            id serial PRIMARY KEY,
            vendorid INTEGER NOT NULL REFERENCES vendor(id),
            contactid INTEGER REFERENCES contact(id),
            UNIQUE(vendorid, contactid)
        );
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def drop_table_vendor_contact_map(conn):
    cursor = conn.cursor()

    query = """
        DROP TABLE IF EXISTS vendor_contact_map;
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def drop_table_vendor(conn):
    cursor = conn.cursor()

    query = """
        DROP TABLE IF EXISTS vendor;
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def drop_table_contact(conn):
    cursor = conn.cursor()

    query = """
        DROP TABLE IF EXISTS contact;
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def upgrade(conn, **kwargs):
    create_table_contact(conn)
    create_table_vendor(conn)
    create_table_vendor_contact_map(conn)
# _______________________________


def downgrade(conn):
    drop_table_vendor_contact_map(conn)
    drop_table_contact(conn)
    drop_table_vendor(conn)
