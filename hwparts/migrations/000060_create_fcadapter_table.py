#!/usr/bin/env python


def create_table_fcadapter(conn):
    cursor = conn.cursor()

    query = """
        CREATE TABLE IF NOT EXISTS fcadapter (
            id serial PRIMARY KEY,
            model VARCHAR(32) UNIQUE,
            revision VARCHAR(32),
            vendorid INTEGER REFERENCES vendor(id),
            port_speed VARCHAR(16)
        );
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def drop_table_fcadapter(conn):
    cursor = conn.cursor()

    query = """
        DROP TABLE IF EXISTS fcadapter;
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def upgrade(conn, **kwargs):
    create_table_fcadapter(conn)
# _______________________________


def downgrade(conn):
    drop_table_fcadapter(conn)
