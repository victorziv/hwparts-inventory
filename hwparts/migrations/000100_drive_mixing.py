#!/usr/bin/env python


def create_table_drive_mixing(conn):
    cursor = conn.cursor()

    query = """
        CREATE TABLE IF NOT EXISTS drive_mixing (
            modelid INTEGER NOT NULL REFERENCES drive(id),
            alternative_modeleid INTEGER NOT NULL REFERENCES drive(id),
            PRIMARY KEY(modelid, alternative_modeleid)
        );
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def drop_table_drive_mixing(conn):
    cursor = conn.cursor()

    query = """
        DROP TABLE IF EXISTS drive_ssd_rejection_criteria;
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def upgrade(conn, **kwargs):
    create_table_drive_mixing(conn)
# _______________________________


def downgrade(conn):
    drop_table_drive_mixing(conn)
