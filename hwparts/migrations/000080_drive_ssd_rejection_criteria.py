#!/usr/bin/env python


def create_table_drive_ssd_rejection_criteria(conn):
    cursor = conn.cursor()

    query = """
        CREATE TABLE IF NOT EXISTS drive_ssd_rejection_criteria (
            modelid INTEGER REFERENCES drive(id),
            attr INTEGER,
            use_raw BOOLEAN,
            comparison VARCHAR(16),
            value INTEGER,
            PRIMARY KEY(modelid, attr)
        );
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def drop_table_drive_ssd_rejection_criteria(conn):
    cursor = conn.cursor()

    query = """
        DROP TABLE IF EXISTS drive_ssd_rejection_criteria;
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def upgrade(conn, **kwargs):
    create_table_drive_ssd_rejection_criteria(conn)
# _______________________________


def downgrade(conn):
    drop_table_drive_ssd_rejection_criteria(conn)
