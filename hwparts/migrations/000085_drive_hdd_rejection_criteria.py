#!/usr/bin/env python


def create_table_drive_hdd_rejection_criteria(conn):
    cursor = conn.cursor()

    query = """
        CREATE TABLE IF NOT EXISTS drive_hdd_rejection_criteria (
            modelid INTEGER REFERENCES drive(id),
            logpage INTEGER,
            counter_byte_offset INTEGER,
            counter_length INTEGER,
            comparison VARCHAR(16),
            value INTEGER,
            PRIMARY KEY(modelid, logpage, counter_byte_offset)
        );
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def drop_table_drive_hdd_rejection_criteria(conn):
    cursor = conn.cursor()

    query = """
        DROP TABLE IF EXISTS drive_hdd_rejection_criteria;
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def upgrade(conn, **kwargs):
    create_table_drive_hdd_rejection_criteria(conn)
# _______________________________


def downgrade(conn):
    drop_table_drive_hdd_rejection_criteria(conn)
