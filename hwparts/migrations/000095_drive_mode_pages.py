#!/usr/bin/env python


def create_table_drive_mode_pages(conn):
    cursor = conn.cursor()

    query = """
        CREATE TABLE IF NOT EXISTS drive_mode_pages (
            modelid INTEGER REFERENCES drive(id),
            page INTEGER,
            data VARCHAR(128),
            PRIMARY KEY(modelid, page)
        );
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def drop_table_drive_mode_pages(conn):
    cursor = conn.cursor()

    query = """
        DROP TABLE IF EXISTS drive_mode_pages;
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def upgrade(conn, **kwargs):
    create_table_drive_mode_pages(conn)
# _______________________________


def downgrade(conn):
    drop_table_drive_mode_pages(conn)
