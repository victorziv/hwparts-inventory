#!/usr/bin/env python


def create_table_front_backplane(conn):
    cursor = conn.cursor()

    query = """
        CREATE TABLE IF NOT EXISTS node_frontbp (
            id serial PRIMARY KEY,
            releaseid INTEGER REFERENCES ibox_sw_release(id),
            pn VARCHAR(32),
            mixpn VARCHAR(128),
            revision VARCHAR(16),
            version VARCHAR(32),
            firmware_bin VARCHAR(256),
            firmware_exe VARCHAR(256),
            hwstatus_check BOOLEAN,
            notes TEXT,
            UNIQUE(releaseid, version)
        );
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def drop_table_front_backplane(conn):
    cursor = conn.cursor()

    query = """
        DROP TABLE IF EXISTS node_frontbp;
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def upgrade(conn, **kwargs):
    create_table_front_backplane(conn)
# _______________________________


def downgrade(conn):
    drop_table_front_backplane(conn)
