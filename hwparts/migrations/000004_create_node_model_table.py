#!/usr/bin/env python


def create_table_node_model(conn):
    cursor = conn.cursor()

    query = """
        CREATE TABLE IF NOT EXISTS node_model (
            id serial PRIMARY KEY,
            model VARCHAR(32) UNIQUE,
            vendorid INTEGER REFERENCES vendor(id),
            notes TEXT
        );
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def populate_node_server_models(conn):
    cursor = conn.cursor()

    models = [
        ('R720', 'dell', 'Deprecated model'),
        ('R730XD', 'dell', 'Current qualified model'),
        ('R740', 'dell', 'Not qualified yet'),

    ]

    query = """
        INSERT INTO node_model
            ( model, vendorid, notes )
        VALUES (
            %s,
            (SELECT id FROM vendor WHERE name = %s),
            %s
        );
        """

    for model in models:
        cursor.execute(query, model)
    conn.commit()
# _____________________________


def drop_table_node_model(conn):
    cursor = conn.cursor()

    query = """
        DROP TABLE IF EXISTS node_model;
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def upgrade(conn, **kwargs):
    create_table_node_model(conn)
    populate_node_server_models(conn)
# _______________________________


def downgrade(conn):
    drop_table_node_model(conn)
