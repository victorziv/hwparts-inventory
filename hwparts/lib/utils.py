import re
SPACE_PAT = re.compile('\s+')
# ____________________________________


def normalize_display_name(name):
    return ' '.join([w.capitalize() for w in name.split()])
# ____________________________________


def normalize_name(name):
    return SPACE_PAT.sub('_', name.lower().strip())
# ____________________________________
