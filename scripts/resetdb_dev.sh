#!/bin/bash

PROJECT=hwparts
curdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ROOTDIR=$(dirname ${curdir})

python ${ROOTDIR}/${PROJECT}/manage.py dbreset --configkey=develop
python ${ROOTDIR}/${PROJECT}/manage.py dbupgrade --configkey=develop
