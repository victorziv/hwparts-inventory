#!/bin/bash

CURDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
APPROOT=$(dirname $CURDIR)
echo $APPROOT

RUNDIR="${APPROOT}/run"

SOCKFILE="${RUNDIR}/hwparts.sock"
running_user=`whoami`
USER=$running_user
GROUP=$running_user
NUM_WORKERS=2

echo "Starting ${NAME} as ${running_user}"

# Activate the virtual environment
source ${APPROOT}/venv/bin/activate
echo `python -V`

# Create the run directory if it doesn't exist
test -d ${RUNDIR} || mkdir -p ${RUNDIR}

$(which gunicorn) manage:app --name hwparts --workers ${NUM_WORKERS} --user ${USER} --bind=unix:${SOCKFILE}
